<?php

return [
    'paths' => [
        realpath(base_path('home/Resources/Views')),
    ],
    'compiled' => realpath(storage_path('framework/views')),
];