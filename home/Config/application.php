<?php

return [
    'package_name' => 'application',
    'package_title' => [
        'ru' => 'Модуль расширения функционала',
        'en' => 'Expansion Module functionality',
        'es' => 'Funcionalidad del módulo de expansión',
    ],
    'package_icon' => 'zmdi zmdi-apps',
    'version' => [
        'ver' => 0.9,
        'date' => '01.01.2016'
    ]
];