## STALKER CMS 2.0.5.2 Beta pre-RC1.0 на базе Laravel PHP Framework

[![Версия Laravel](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Лицензия Laravel](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

### Laravel использует Composer для управления зависимостями. Поэтому прежде чем ставить STALKER CMS вы должны установить Composer (https://getcomposer.org/).

### Требований к серверу:
1. PHP >= 5.5.9
2. OpenSSL PHP Extension
3. PDO PHP Extension
4. Mbstring PHP Extension
5. Tokenizer PHP Extension
6. Fileinfo PHP Extension
7. Mcrypt PHP Extension
8. Filter PHP Extension
9. Session PHP Extension
10. Phar PHP Extension
11. MySQL 5.5+
12. Apache 2.2+ с активным модулем mod_rewrite
13. Использование PHP в командной строке

###Рекомендуется установить и включить следующие разширения PHP:

1. cURL PHP Extension
2. Iconv PHP Extension
3. Json PHP Extension
4. SimpleXML PHP Extension
5. Zip PHP Extension

Корень сайта должен указывать на каталог public. Если каталог корня сайта имеет другое название - переименуйте каталог public.

Установка STALKER CMS происходит средствами мастера установки. 
Для этого нужно в браузере перейти по ссылке http://mysite.com/install и следовать дальнейшим инструкциям.
База данных автоматически не создается и должна быть создана до начала установки CMS.

#### Если понадобится деинсталировать STALKER CMS воспользуйтесь коммандой artisan: php artisan Uninstall

###Ссылки для ознакомления:
1. Официальная документация: https://laravel.com/docs/5.2
2. Документация API: https://laravel.com/api/5.2/index.html
3. Русская документация: http://laravel.su/docs/5.0/installation или https://laravel.ru/docs/v5/
4. Laravel Packagist - https://packagist.org/packages/laravel/framework
5. Laravel 5 Fundamentals: https://laracasts.com/series/laravel-5-fundamentals


###УСТАНОВКА STALKER CMS 2.0.5.2 Beta pre-RC1.0 от 25.10.2016 г.

Для установки CMS необходимо в командной строке выполнить:
composer --prefer-dist create-project grphm/stalker <ПУТЬ К КАТАЛОГУ ПРОЕКТА> dev-master