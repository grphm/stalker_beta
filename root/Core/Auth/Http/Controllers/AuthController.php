<?php

namespace STALKER_CMS\Core\Auth\Http\Controllers;

use Carbon\Carbon;
use STALKER_CMS\Core\System\Models\User;
use STALKER_CMS\Core\System\Models\Group;

use STALKER_CMS\Vendor\Models\Languages;
use STALKER_CMS\Vendor\Traits\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

/**
 * Контроллер авторизации
 * Class AuthController
 * @package STALKER_CMS\Core\Auth\Http\Controllers
 */
class AuthController extends ModuleController {

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * @var string
     */
    protected $redirectTo = 'admin';

    /**
     * AuthController constructor.
     */
    public function __construct() {

        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Страница авторизации
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm() {

        return view('core_auth_views::index');
    }

    /**
     * Метод авторизации
     * @return \Illuminate\Http\JsonResponse
     */
    public function login() {

        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, User::getAuthRules())):
            $throttles = $this->isUsingThrottlesLoginsTrait();
            if ($throttles && $this->hasTooManyLoginAttempts($request)):
                return $this->sendLockoutResponse($request);
            endif;
            $credentials = array_collapse([$request::only('login', 'password'), ['approve' => TRUE, 'active' => TRUE]]);
            if (\Auth::guard($this->getGuard())->attempt($credentials, $request::has('remember'))):
                return $this->authenticated($request, $throttles);
            endif;
            if ($throttles):
                $this->incrementLoginAttempts($request);
            endif;
            return \ResponseController::error(2101)->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Возвращает путь в странице после успешной авторизации
     * @param $request
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    private function setRedirectPath($request) {

        $redirectTo = ($request::has('redirect')) ? $request::input('redirect') : url(\Auth::user()->group->start_url);
        return $this->redirectTo = $redirectTo;
    }

    /**
     * @param $request
     * @param $throttles
     * @return \Illuminate\Http\JsonResponse
     */
    protected function authenticated($request, $throttles) {

        if ($throttles):
            $this->clearLoginAttempts($request);
        endif;
        \Auth::user()->last_login = Carbon::now();
        \Auth::user()->save();
        $this->setRedirectPath($request);
        return \ResponseController::success(200)
            ->set('responseText', '<nobr>' . trans('core_auth_lang::auth.welcome') . ' ' . \Auth::user()->name . '</nobr>')
            ->redirect($this->redirectPath())->json();
    }
}
