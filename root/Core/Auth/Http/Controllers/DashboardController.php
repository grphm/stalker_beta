<?php

namespace STALKER_CMS\Core\Auth\Http\Controllers;

/**
 * Контроллер Dashboard пользователя
 * Class DashboardController
 * @package STALKER_CMS\Core\Auth\Http\Controllers
 */
class DashboardController extends ModuleController {

    /**
     * DashboardController constructor.
     */
    public function __construct() {

        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $template = \Auth::user()->group->dashboard;
        $slug = \Auth::user()->group->slug;
        if (view()->exists("application_views::'. $slug . '.' . $template")):
            return view("core_system_views::'. $slug . '.' . $template");
        elseif (view()->exists("site_views::packages.core_system.dashboards.$template")):
            return view("site_views::packages.core_system.dashboards.$template");
        elseif (view()->exists("core_system_views::dashboards.$template")):
            return view("core_system_views::dashboards.$template");
        else:
            return view('core_system_views::dashboards.admin');
        endif;
    }
}
