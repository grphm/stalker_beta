<?php

namespace STALKER_CMS\Vendor\Http\Controllers;

use Illuminate\Foundation\Auth\ResetsPasswords;

/**
 * Class PasswordController
 * @package STALKER_CMS\Vendor\Http\Controllers
 */
class PasswordController extends ModuleController {

    use ResetsPasswords;

    /**
     * PasswordController constructor.
     */
    public function __construct() {

        $this->middleware('guest');
    }
}
