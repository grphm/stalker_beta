<?php

return [
    'page_title' => 'Authorization',
    'page_description' => '',
    'form' => [
        'title' => 'Enter the site',
        'login_field' => 'Login',
        'password' => 'Password'
    ]
];
