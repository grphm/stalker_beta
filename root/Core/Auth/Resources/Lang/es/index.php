<?php

return [
    'page_title' => 'Autorización',
    'page_description' => '',
    'form' => [
        'title' => 'Entre en el sitio',
        'login_field' => "Iniciar sesión",
        'password' => 'Contraseña'
    ]
];
