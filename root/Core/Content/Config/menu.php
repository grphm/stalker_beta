<?php

return [
    'package' => 'core_content',
    'title' => ['ru' => 'Контент', 'en' => 'Content', 'es' => 'Contenido'],
    'route' => '__#',
    'icon' => 'zmdi zmdi-view-quilt',
    'menu_child' => [
        'pages' => [
            'title' => ['ru' => 'Страницы', 'en' => 'Pages', 'es' => 'Páginas'],
            'route' => 'core.content.pages.index',
            'icon' => 'zmdi zmdi-view-web'
        ],
        'menu' => [
            'title' => ['ru' => 'Конструктор меню', 'en' => 'Menu designer', 'es' => 'Diseñador de menús'],
            'route' => 'core.content.menu.index',
            'icon' => 'zmdi zmdi-menu'
        ],
        'templates' => [
            'title' => ['ru' => 'Шаблоны', 'en' => 'Templates', 'es' => 'Plantillas'],
            'route' => 'core.content.templates.index',
            'icon' => 'zmdi zmdi-layers'
        ],
        'languages' => [
            'title' => ['ru' => 'Локализация', 'en' => 'Localization', 'es' => 'localización'],
            'route' => 'core.content.languages.index',
            'icon' => 'zmdi zmdi-translate'
        ],
    ]
];