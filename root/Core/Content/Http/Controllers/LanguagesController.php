<?php
namespace STALKER_CMS\Core\Content\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

/**
 * Контроллер страница
 * Class LanguagesController
 * @package STALKER_CMS\Core\Content\Http\Controllers
 */
class LanguagesController extends ModuleController implements CrudInterface {

    /**
     * LanguagesController constructor.
     */
    public function __construct() {

        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_content', 'languages');
        $language_file = '';
        $directory_lang = base_path('home/Resources/Lang/' . \App::getLocale());
        $languages_files = new Collection(\File::glob($directory_lang . '/*.php', GLOB_NOSORT));
        if ($languages_files->count()):
            $languages_variables = [];
            if (\Request::has('file') && \File::exists($directory_lang . '/' . \Request::get('file') . '.php')):
                $languages_content = \File::get($directory_lang . '/' . \Request::get('file') . '.php');
                $language_file = $directory_lang . '/' . \Request::get('file') . '.php';
            else:
                $languages_content = \File::get($languages_files->first());
                $language_file = $languages_files->first();
            endif;
            return view('core_content_views::languages.index', compact('languages_files', 'languages_content', 'language_file'));
        else:
            return redirect()->to(route('core.content.pages.index') . '?status=2615');
        endif;
    }

    /**
     *
     */
    public function create() {
        // TODO: Implement create() method.
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function store() {

        \PermissionsController::allowPermission('core_content', 'languages');
        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if (\ValidatorController::passes($request, ['file' => 'required', 'content' => 'required'])):
            $file_path = base_path('/home/Resources/Lang/' . \App::getLocale() . '/' . $request::input('file'));
            $file_path = \STALKER_CMS\Vendor\Helpers\double_slash($file_path);
            \File::put($file_path, $request::input('content'));
            \Artisan::call('CacheKiller');
            return \ResponseController::success(202)->redirect(route('core.content.languages.index') . '?file=' . basename($file_path, '.php'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     */
    public function edit($id) {
        // TODO: Implement edit() method.
    }

    /**
     * @param $id
     */
    public function update($id) {
        // TODO: Implement update() method.
    }

    /**
     * @param $id
     */
    public function destroy($id) {
        // TODO: Implement destroy() method.
    }
}