<?php
namespace STALKER_CMS\Core\Content\Http\Controllers;

use Illuminate\Support\Collection;
use STALKER_CMS\Core\Content\Models\Menu;
use STALKER_CMS\Core\Content\Models\MenuItem;
use STALKER_CMS\Core\Content\Models\Page;
use STALKER_CMS\Core\Content\Models\PageTemplate;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

use STALKER_CMS\Vendor\Helpers as Helpers;

/**
 * Контроллер элемента меню
 * Class MenuItemController
 * @package STALKER_CMS\Core\Content\Http\Controllers
 */
class MenuItemController extends ModuleController implements CrudInterface {

    /**
     * @var
     */
    protected $menu;
    /**
     * @var MenuItem
     */
    protected $model;

    /**
     * MenuItemController constructor.
     * @param MenuItem $item
     * @param Menu $menu
     */
    public function __construct(MenuItem $item, Menu $menu) {

        $this->menu = $menu->whereLocale(\App::getLocale())->findOrFail(\Request::segment(4));
        $this->model = $item;
        $this->middleware('auth');
        \PermissionsController::allowPermission('core_content', 'menu');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $items = \PublicMenu::make($this->menu->slug);
        return view('core_content_views::menu.items.index', [
            'menu' => $this->menu,
            'items' => $items,
            'list' => $this->makeMenuList(\PublicMenu::make($this->menu->slug))
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        return view('core_content_views::menu.items.create', ['menu' => $this->menu]);
    }

    /**
     * @param null $menu_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($menu_id = NULL) {

        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('core.content.menu.items_index', $menu_id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param null $menu_id
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($menu_id = NULL, $id = NULL) {

        return view('core_content_views::menu.items.edit', [
            'menu' => $this->menu,
            'item' => $this->model->findOrFail($id)
        ]);
    }

    /**
     * @param null $menu_id
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($menu_id = NULL, $id = NULL) {

        \PermissionsController::allowPermission('core_content', 'menu');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('core.content.menu.items_index', $menu_id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param null $menu_id
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($menu_id = NULL, $id = NULL) {

        $request = \RequestController::isAJAX()->init();
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('core.content.menu.items_index', $menu_id))->json();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function sortable() {

        \PermissionsController::allowPermission('core_content', 'menu');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, ['elements' => 'required'])):
            $this->sortMenuItems(json_decode($request::input('elements'), TRUE), 0, 1);
            return \ResponseController::success(202)->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /*****************************************************************************************/
    /**
     * @param $elements
     * @param $parent_id
     * @param $index
     */
    private function sortMenuItems($elements, $parent_id, $index) {

        foreach ($elements as $element):
            if (isset($element['children'])):
                MenuItem::where('id', $element['item'])->update(['order' => $index, 'parent_id' => $parent_id]);
                $this->sortMenuItems($element['children'][0], $element['item'], $index + 1);
            else:
                MenuItem::where('id', $element['item'])->update(['order' => $index, 'parent_id' => $parent_id]);
            endif;
            $index++;
        endforeach;
    }

    /**
     * @param array $items
     * @return string
     */
    private function makeMenuList(array $items) {

        $tree = '';
        $types = [
            'page_link' => \Lang::get('core_content_lang::menu_items.insert.form.page_link_type'),
            'external_link' => \Lang::get('core_content_lang::menu_items.insert.form.external_link'),
            'anchor_link' => \Lang::get('core_content_lang::menu_items.insert.form.anchor_link'),
            'file_link' => \Lang::get('core_content_lang::menu_items.insert.form.file_link')
        ];
        $tree .= '<ul class="p-l-20">';
        foreach ($items as $item) :
            $tree .= '<li class="js-item-container lv-item media p-0 p-l-20 p-t-5 p-b-5" data-item="' . $item['id'] . '">';
            $tree .= view('core_content_views::menu.items.item', ['element' => $item, 'types' => $types, 'menu' => $this->menu])->render();
            if (isset($item['sub_menu'])):
                $tree .= $this->makeMenuList($item['sub_menu']);
            else:
                $tree .= '<ul class="p-l-20"></ul>';
            endif;
            $tree .= '</li>';
        endforeach;
        $tree .= '</ul>';
        return $tree;
    }
}