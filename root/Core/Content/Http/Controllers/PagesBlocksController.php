<?php
namespace STALKER_CMS\Core\Content\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Core\Content\Models\Page;
use STALKER_CMS\Core\Content\Models\PageBlock;
use STALKER_CMS\Core\Content\Models\PageTemplate;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Helpers as Helpers;

/**
 * Контроллер информационных блоков страницы
 * Class PagesBlocksController
 * @package STALKER_CMS\Core\Content\Http\Controllers
 */
class PagesBlocksController extends ModuleController {

    /**
     * @var mixed
     */
    protected $page;
    /**
     * @var PageBlock
     */
    protected $model;

    /**
     * PagesBlocksController constructor.
     * @param PageBlock $page_block
     * @param Page $page
     */
    public function __construct(PageBlock $page_block, Page $page) {

        $this->model = $page_block;
        $this->page = $page->whereFind(['id' => \Request::segment(4), 'locale' => \App::getLocale()]);
        \PermissionsController::allowPermission('core_content', 'edit');
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        return view('core_content_views::blocks.index', [
            'page' => $this->page,
            'blocks' => $this->model->wherePageId($this->page->id)->orderBy('updated_at', 'DESC')->with('author')->get()
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        return view('core_content_views::blocks.create', [
            'templates' => new Collection(),
            'page' => $this->page
        ]);
    }

    /**
     * @param $page_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store($page_id) {

        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            $this->model->uniqueness(['page_id' => $page_id, 'slug' => $request::input('slug')]);
            $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('core.content.pages.blocks_index', $page_id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $page_id
     * @param $block_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($page_id, $block_id) {

        return view('core_content_views::blocks.edit', [
            'templates' => new Collection(),
            'page' => $this->page,
            'block' => $this->model->whereFind(['id' => $block_id, 'page_id' => $page_id])
        ]);
    }

    /**
     * @param $page_id
     * @param $block_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update($page_id, $block_id) {

        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if (\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->uniqueness(['page_id' => $page_id, 'slug' => $request::input('slug')], $block_id);
            $this->model->replace($block_id, $request);
            return \ResponseController::success(202)->redirect(route('core.content.pages.blocks_index', $page_id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $page_id
     * @param $block_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($page_id, $block_id) {

        $request = \RequestController::isAJAX()->init();
        $this->model->remove($block_id);
        return \ResponseController::success(1203)->redirect(route('core.content.pages.blocks_index', $page_id))->json();
    }
}