<?php
namespace STALKER_CMS\Core\Content\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Core\Content\Models\Page;
use STALKER_CMS\Core\Content\Models\PageTemplate;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Helpers as Helpers;

/**
 * Контроллер страницы
 * Class PagesController
 * @package STALKER_CMS\Core\Content\Http\Controllers
 */
class PagesController extends ModuleController implements CrudInterface {

    /**
     * @var Page
     */
    protected $model;

    /**
     * PagesController constructor.
     * @param Page $page
     */
    public function __construct(Page $page) {

        $this->model = $page;
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_content', 'pages');
        $request = \RequestController::init();
        $pages = $this->model->whereLocale(\App::getLocale())->with('author', 'template');
        if ($request::has('search')):
            $search = $request::input('search');
            $pages = $pages->where(function ($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%');
                $query->orWhere('seo_title', 'like', '%' . $search . '%');
            });
        endif;
        if ($request::has('sort_field') && $request::has('sort_direction')):
            foreach (explode(',', $request::input('sort_field')) as $index):
                $pages = $pages->orderBy($index, $request::input('sort_direction'));
            endforeach;
        endif;
        return view('core_content_views::pages.index', ['pages' => $pages->orderBy('updated_at', 'DESC')->get()]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create() {

        \PermissionsController::allowPermission('core_content', 'create');
        $templates = PageTemplate::whereLocale(\App::getLocale())->whereRequired(FALSE)->whereMenuType('page')->lists('title', 'id');
        if ($templates->count()):
            return view('core_content_views::pages.create', compact('templates'));
        else:
            return redirect()->route('core.content.templates.create')->with('status', 2612);
        endif;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store() {

        \PermissionsController::allowPermission('core_content', 'create');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            $this->model->forbiddenURLs($request::only(['slug', 'seo_url']));
            $this->model->uniqueness($request::only('slug'));
            $this->model->uniqueSeoURL($request::input('seo_url'));
            if (\PermissionsController::isPackageEnabled('core_open_graph') && \PermissionsController::allowPermission('core_content', 'open_graph', FALSE)):
                $open_graph = \OpenGraph::makeOpenGraphData($request);
                $request::merge(['open_graph' => $open_graph]);
            endif;
            $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('core.content.pages.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id) {

        \PermissionsController::allowPermission('core_content', 'edit');
        $templates = PageTemplate::whereLocale(\App::getLocale())->whereRequired(FALSE)->whereMenuType('page')->lists('title', 'id');
        if ($templates->count()):
            $page = $this->model->findOrFail($id);
            $blocks = new Collection();
            return view('core_content_views::pages.edit', compact('templates', 'page', 'blocks'));
        else:
            return redirect()->route('core.content.templates.create')->with('status', 2612);
        endif;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update($id) {

        \PermissionsController::allowPermission('core_content', 'edit');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->forbiddenURLs($request::only(['slug', 'seo_url']));
            $this->model->uniqueness($request::only('slug'), $id);
            $this->model->uniqueSeoURL($request::input('seo_url'), $id);
            if (\PermissionsController::isPackageEnabled('core_open_graph') && \PermissionsController::allowPermission('core_content', 'open_graph', FALSE)):
                $page = $this->model->findOrFail($id);
                $open_graph = \OpenGraph::remakeOpenGraphData($page, $request);
                $request::merge(['open_graph' => $open_graph]);
            endif;
            $this->clearCompiledViews($id, $request);
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('core.content.pages.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \PermissionsController::allowPermission('core_content', 'delete');
        $request = \RequestController::isAJAX()->init();
        if (\PermissionsController::isPackageEnabled('core_open_graph')):
            $page = $this->model->findOrFail($id);
            \OpenGraph::destroyOpenGraphData($page);
        endif;
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('core.content.pages.index'))->json();
    }

    /**
     * Удаление скомпилированных blade шаблонов
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clearCash() {

        \PermissionsController::allowPermission('core_content', 'pages');
        \Artisan::call('CacheKiller');
        return redirect()->to(route('core.content.pages.index') . '?status=200');
    }

    /**************************************************************************************************************/

    /**
     * Удаление скомпилированных blade шаблонов
     * @param $page_id
     * @param \Request $request
     */
    private function clearCompiledViews($page_id, \Request $request) {

        $page = $this->model->find($page_id);
        if ($page->title !== $request::input('title')):
            \Artisan::call('CacheKiller');
        endif;
        if ($request::has('publication') && !$page->publication):
            \Artisan::call('CacheKiller');
        endif;
        if (!$request::has('publication') && $page->publication):
            \Artisan::call('CacheKiller');
        endif;
    }
}