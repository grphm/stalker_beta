<?php
namespace STALKER_CMS\Core\Content\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\View;
use STALKER_CMS\Core\Content\Models\PageTemplate;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Helpers as Helpers;

/**
 * Контроллер шаблона
 * Class TemplatesController
 * @package STALKER_CMS\Core\Content\Http\Controllers
 */
class TemplatesController extends ModuleController implements CrudInterface {

    /**
     * @var PageTemplate
     */
    protected $model;

    /**
     * TemplatesController constructor.
     * @param PageTemplate $pageTemplate
     */
    public function __construct(PageTemplate $pageTemplate) {

        $this->model = $pageTemplate;
        $this->middleware('auth');
    }

    /**
     * @return View
     */
    public function index() {

        \PermissionsController::allowPermission('core_content', 'templates');
        $templates = [];
        foreach(config('core_content::config.menu_types') as $menu_type_slug => $menu_type):
            $templates[$menu_type_slug] = [
                'title' => Helpers\array_translate($menu_type),
                'path' => Helpers\array_translate(Helpers\settings(['core_content', 'pages', 'templates_dir'], 'note')),
                'files' => $this->model->whereLocale(\App::getLocale())->whereMenuType($menu_type_slug)->orderBy('updated_at', 'DESC')->get()
            ];
        endforeach;
        if(\Request::has('type') && isset($templates[\Request::get('type')])):
            $templates_type = array_first($templates, function($key, $value) {

                return $key >= \Request::get('type');
            });
            $templates = [
                \Request::get('type') => $templates_type
            ];
        endif;
        return view('core_content_views::templates.index', compact('templates'));
    }

    /**
     * @return View
     */
    public function create() {

        \PermissionsController::allowPermission('core_content', 'templates');
        $menu_types = [];
        foreach(config('core_content::config.menu_types') as $menu_type_slug => $menu_type):
            $menu_types[$menu_type_slug] = Helpers\array_translate($menu_type);
        endforeach;
        $template_content = '';
        if(view()->exists('core_content_views::templates.sketch')):
            $template_content = \File::get(view('core_content_views::templates.sketch')->getPath());
        endif;
        return view('core_content_views::templates.create', compact('menu_types', 'template_content'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function store() {

        \PermissionsController::allowPermission('core_content', 'templates');
        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            $prefix = \App::getLocale() == Helpers\settings(['core_system', 'settings', 'base_locale']) ? '' : \App::getLocale().'/';
            $template_directory = Helpers\double_slash(base_path('home/Resources/Views/'.Helpers\settings(['core_content', 'pages', 'templates_dir']).'/'.$prefix));
            if(\File::exists($template_directory) === FALSE):
                \File::makeDirectory($template_directory);
            endif;
            $view_path = $template_directory.$request::input('path').'.blade.php';
            if(\File::exists($view_path) === FALSE):
                $request::merge(['path' => $prefix.$request::input('path').'.blade.php', 'required' => FALSE, 'locale' => \App::getLocale()]);
                $this->model->insert($request);
                \File::put($view_path, $request::input('content'));
                \Artisan::call('CacheKiller');
                return \ResponseController::success(1600)->redirect(route('core.content.templates.index'))->json();
            else:
                return \ResponseController::error(2610)->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return View
     */
    public function edit($id) {

        \PermissionsController::allowPermission('core_content', 'templates');
        $template_content = '';
        $template = $this->model->findOrFail($id);
        $view_path = base_path('/home/Resources/Views/'.Helpers\settings(['core_content', 'pages', 'templates_dir']).'/'.$template->path);
        if(\File::exists(realpath($view_path))):
            $template_content = \File::get(realpath($view_path));
        endif;
        return view('core_content_views::templates.edit', compact('template_content', 'template'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id) {

        \PermissionsController::allowPermission('core_content', 'templates');
        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $template = $this->model->findOrFail($id);
            $view_path = base_path('/home/Resources/Views/'.Helpers\settings(['core_content', 'pages', 'templates_dir']).'/'.$template->path);
            $view_path = Helpers\double_slash($view_path);
            \File::put($view_path, $request::input('content'));
            $this->model->replace($id, $request);
            \Artisan::call('CacheKiller');
            return \ResponseController::success(202)->redirect(route('core.content.templates.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \PermissionsController::allowPermission('core_content', 'templates');
        $request = \RequestController::isAJAX()->init();
        try {
            $template = $this->model->findOrFail($id);
            $view_path = base_path('/home/Resources/Views/'.Helpers\settings(['core_content', 'pages', 'templates_dir']).'/'.$template->path);
            if(\File::exists(realpath($view_path))):
                \File::delete(realpath($view_path));
            endif;
            $this->model->remove($id);
            return \ResponseController::success(1203)->redirect(route('core.content.templates.index'))->json();
        } catch(Exception $e) {
            return \ResponseController::success(2503)->json();
        }
    }

    /**
     * Позволяет редактировать шаблон dashboard
     */
    public function editDashboardTemplate() {

        \PermissionsController::allowPermission('core_content', 'templates');
        \RequestController::isAJAX()->init();
        $html = '';
        $status_code = 204;
        $dashboard_template = \Auth::user()->group->dashboard;
        if(view()->exists("site_views::packages.core_system.dashboards/".$dashboard_template)):
            $html = \File::get(base_path('home/Resources/Views/packages/core_system/dashboards/'.$dashboard_template.'.blade.php'));
            $status_code = 200;
        else:
            if(!\File::isDirectory(base_path('home/Resources/Views/packages/core_system/dashboards'))):
                \File::makeDirectory(base_path('home/Resources/Views/packages/core_system/dashboards'), 0755, TRUE, TRUE);
            endif;
            if(\File::copy(base_path('root/Core/System/Resources/Views/dashboards/admin.blade.php'), base_path('home/Resources/Views/packages/core_system/dashboards/'.$dashboard_template.'.blade.php'))):
                $html = \File::get(base_path('home/Resources/Views/packages/core_system/dashboards/'.$dashboard_template.'.blade.php'));
                $status_code = 201;
            else:
                return \ResponseController::error(2611)->json();
            endif;
        endif;
        ob_start();
        ?>
        <div class="row">
            <div class="col-sm-12">
                <button type="submit" id="dashboard-template-save" class="btn btn-primary btn-sm m-t-25 waves-effect waves-effect">
                    <i class="fa fa-save"></i>
                    <span class="btn-text"><?= Helpers\array_translate(['ru' => 'Сохранить', 'en' => 'Save', 'es' => 'Salvar']); ?></span>
                </button>
            </div>
        </div>
        <?php
        $button = ob_get_clean();
        return \ResponseController::success($status_code)->set('html', $html)->set('button', $button)->json();
    }

    public function updateDashboardTemplate() {

        \PermissionsController::allowPermission('core_content', 'templates');
        $request = \RequestController::isAJAX()->trim_spaces()->get();
        if(\ValidatorController::passes($request, ['content' => 'required'])):
            $dashboard_template = \Auth::user()->group->dashboard;
            $template = base_path('home/Resources/Views/packages/core_system/dashboards/'.$dashboard_template.'.blade.php');
            if(\File::exists($template)):
                \File::put($template, $request::input('content'));
                return \ResponseController::success(202)->redirect(route('dashboard'))->json();
            else:
                return \ResponseController::error(2611)->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }
}