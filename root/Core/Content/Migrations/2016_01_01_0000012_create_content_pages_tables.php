<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentPagesTables extends Migration {

    public function up() {

        Schema::create('content_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 50)->nullable()->index();
            $table->string('locale', 10)->nullable()->index();
            $table->smallInteger('template_id', FALSE, TRUE)->nullable()->index();
            $table->boolean('publication')->default(1)->nullable()->index();
            $table->boolean('start_page')->default(0)->nullable()->index();
            $table->string('title', 100)->nullable();
            $table->string('seo_title', 255)->nullable();
            $table->text('seo_description')->nullable();
            $table->string('seo_h1', 255)->nullable();
            $table->string('seo_url', 255)->nullable();
            $table->text('open_graph')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable()->index();
            $table->timestamps();
        });

        Schema::create('content_pages_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 50)->nullable()->index();
            $table->integer('page_id', FALSE, TRUE)->nullable()->index();
            $table->string('title', 100)->nullable();
            $table->smallInteger('template_id', FALSE, TRUE)->nullable()->index();
            $table->text('content')->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable()->index();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('content_pages');
        Schema::dropIfExists('pages_content_blocks');
    }
}

