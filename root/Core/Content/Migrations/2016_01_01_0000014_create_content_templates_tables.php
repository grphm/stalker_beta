<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentTemplatesTables extends Migration {

    public function up() {

        Schema::create('content_pages_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('menu_type', 20)->nullable()->index();
            $table->string('locale', 10)->nullable()->index();
            $table->string('title', 100)->nullable();
            $table->string('path', 255)->nullable();
            $table->boolean('required')->default(FALSE)->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable()->index();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('content_pages_templates');
    }
}

