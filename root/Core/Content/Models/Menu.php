<?php
namespace STALKER_CMS\Core\Content\Models;

use Carbon\Carbon;
use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель меню
 * Class Menu
 * @package STALKER_CMS\Core\Content\Models
 */
class Menu extends BaseModel implements ModelInterface {

    use ModelTrait;

    /**
     * @var string
     */
    protected $table = 'content_menu';
    /**
     * @var array
     */
    protected $fillable = ['slug', 'locale', 'template_id', 'title', 'user_id'];
    /**
     * @var array
     */
    protected $hidden = [];
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @param $request
     * @return Menu
     */
    public function insert($request) {

        $model = new Menu();
        $model->slug = $request::input('slug');
        $model->locale = \App::getLocale();
        $model->template_id = $request::input('template_id');
        $model->title = $request::input('title');
        $model->user_id = \Auth::user()->id;
        $model->save();
        return $model;
    }

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->title = $request::input('title');
        $model->template_id = $request::input('template_id');
        $model->user_id = \Auth::user()->id;
        $model->save();
        $model->touch();
        return $model;
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /**
     * Шаблон меню
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function template() {

        return $this->hasOne('\STALKER_CMS\Core\Content\Models\PageTemplate', 'id', 'template_id');
    }

    /**
     * Автор меню
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    /**
     * Элементы меню
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items() {

        return $this->hasMany('\STALKER_CMS\Core\Content\Models\MenuItem', 'menu_id', 'id');
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['slug' => 'required', 'template_id' => 'required'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return ['template_id' => 'required'];
    }
}