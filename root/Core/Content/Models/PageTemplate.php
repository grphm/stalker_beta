<?php
namespace STALKER_CMS\Core\Content\Models;

use Carbon\Carbon;
use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель Шаблон страницы
 * Class PageTemplate
 * @package STALKER_CMS\Core\Content\Models
 */
class PageTemplate extends BaseModel implements ModelInterface {

    use ModelTrait;

    /**
     * @var string
     */
    protected $table = 'content_pages_templates';
    /**
     * @var array
     */
    protected $fillable = ['menu_type', 'locale', 'title', 'path', 'user_id'];
    /**
     * @var array
     */
    protected $hidden = [];
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @param $request
     * @return $this
     */
    public function insert($request) {

        $this->menu_type = $request::input('template_type');
        $this->locale = \App::getLocale();
        $this->title = $request::input('title');
        $this->path = $request::input('path');
        $this->required = $request::input('required');
        $this->user_id = \Auth::user()->id;
        $this->save();
        return $this;
    }

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->title = $request::input('title');
        $model->user_id = \Auth::user()->id;
        $model->save();
        return $model;
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        Page::whereTemplateId($id)->update(['template_id' => NULL]);
        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /**
     * Автор
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    /**
     * Страница
     */
    public function pages() {

    }

    /**
     * Меню
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menus() {

        return $this->hasMany('\STALKER_CMS\Core\Content\Models\Menu', 'template_id', 'id');
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['title' => 'required', 'path' => 'required'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return ['title' => 'required'];
    }
}