<?php

return [
    'embed' => 'Embed code',
    'edit' => 'Edit',
    'delete' => [
        'question' => 'Delete block',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'slug' => 'Symbolic code',
    'empty' => 'List is empty',
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding block',
        'form' => [
            'title' => 'Title',
            'template' => 'Page block',
            'content' => 'Content block',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: index',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit block',
        'form' => [
            'title' => 'Title',
            'template' => 'Block template',
            'content' => 'Content block',
            'submit' => 'Save'
        ]
    ]
];