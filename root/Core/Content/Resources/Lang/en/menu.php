<?php

return [
    'edit' => 'Edit',
    'embed' => 'Embed code',
    'delete' => [
        'question' => 'Delete menu',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'items' => 'Menu items',
    'slug' => 'Symbolic code',
    'symbolic_code' => 'Symbolic code',
    'empty' => 'List is empty',
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding menu',
        'form' => [
            'title' => 'Title',
            'template' => 'Menu template',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: about',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit menu',
        'form' => [
            'title' => 'Title',
            'template' => 'Menu template',
            'submit' => 'Save'
        ]
    ]
];