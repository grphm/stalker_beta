<?php

return [
    'cache_clear' => 'Clear cache',
    'search' => 'Enter the title or symbolic code page',
    'sort_title' => 'Title',
    'sort_published' => 'Published date',
    'sort_updated' => 'Update date',
    'edit' => 'Edit',
    'blocks' => 'Content blocks',
    'blank' => 'Open in new window',
    'embed' => 'Embed code',
    'delete' => [
        'question' => 'Delete page',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'not_published' => 'Not published',
    'published' => 'Published',
    'update' => 'Update date',
    'slug' => 'Symbolic code',
    'empty' => 'List is empty',
    'blocks_list_empty' => 'List of empty blocks',
    'blocks_list' => 'List of blocks',
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding page',
        'form' => [
            'template' => 'Page template',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: index',
            'title' => 'Title',
            'title_help_description' => 'For example: Main page',
            'publish' => 'Publish',
            'main_page' => 'Main page',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit page',
        'form' => [
            'template' => 'Page template',
            'title' => 'Title',
            'title_help_description' => 'For example: Main page',
            'publish' => 'Publish',
            'main_page' => 'Main page',
            'submit' => 'Save'
        ]
    ]
];