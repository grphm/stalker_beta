<?php

return [
    'all_types' => 'All types',
    'edit' => 'Edit',
    'delete' => [
        'question' => 'Delete template',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'empty' => 'List is empty',
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding template',
        'form' => [
            'content' => 'Content template',
            'template_type' => 'Type of template',
            'title' => 'Title',
            'title_help_description' => 'For example: Main page',
            'path' => 'Filename template',
            'path_help_description' => 'Expanding .blade.php is not needed!<br> Example: index',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit template',
        'form' => [
            'content' => 'Content template',
            'title' => 'Title',
            'title_help_description' => 'For example: Main page',
            'submit' => 'Save'
        ]
    ]
];