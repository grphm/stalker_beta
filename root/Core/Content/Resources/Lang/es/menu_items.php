<?php

return [
    'breadcrumb' => 'Elementos de menú',
    'top_item' => 'El nivel superior',
    'edit' => 'Editar',
    'delete' => [
        'question' => 'Eliminar elemento de menú',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'empty' => 'Lista está vacía',
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de un elemento de menú',
        'form' => [
            'parent' => 'El elemento de menú principal',
            'title' => 'El texto del elemento de menú',
            'pages' => 'Página',
            'files' => 'Archivos cargados',
            'blank' => 'Abrir en una nueva ventana',
            'anchor' => 'Ancla',
            'properties' => 'Otras propiedades',
            'title_link' => 'el atributo title',
            'class_link' => 'CSS-clases para los enlaces',
            'get_link' => 'GET-parámetros',
            'hide' => 'Ocultar',
            'link_type' => 'Tipo de elemento de menú',
            'page_link_type' => 'Enlace a la página',
            'external_link' => 'Referencia externa',
            'anchor_link' => 'La referencia a "anclar"',
            'file_link' => 'Enlace a archivo',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Elemento de menú Editar',
        'form' => [
            'parent' => 'El elemento de menú principal',
            'title' => 'El texto del elemento de menú',
            'pages' => 'Página',
            'files' => 'Archivos cargados',
            'blank' => 'Abrir en una nueva ventana',
            'anchor' => 'Ancla',
            'properties' => 'Otras propiedades',
            'title_link' => 'el atributo title',
            'class_link' => 'CSS-clases para los enlaces',
            'get_link' => 'GET-parámetros',
            'hide' => 'Ocultar',
            'link_type' => 'Tipo de elemento de menú',
            'page_link_type' => 'Enlace a la página',
            'external_link' => 'Referencia externa',
            'anchor_link' => 'La referencia a "anclar"',
            'file_link' => 'Enlace a archivo',
            'submit' => 'Save'
        ]
    ]
];