<?php

return [
    'index' => [
        'title' => 'Hello!',
        'description' => 'Welcome to ' . config('app.application_name')
    ],
    'contacts' => [
        'description' => 'For any questions and suggestions please contact:',
        'phone' => 'Phone: +7(918)891-97-21',
        'email' => 'Email: vk@grapheme.ru'
    ]
];