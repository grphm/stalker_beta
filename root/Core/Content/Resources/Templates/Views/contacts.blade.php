@extends('site_views::layout')
@metaTitle()
@metaDescription()
@section('head')
	{!! Html::style('core/css/vendor.css') !!}
    {!! Html::style('core/css/main.css') !!}
@stop
@section('body-class'){{ 'four-zero-content' }}@stop
@section('header')
@stop
@section('content')
    <div class="four-zero">
        <small class="m-t-20">@lang('site_lang::main.contacts.description')</small>
        <small class="m-t-20">@lang('site_lang::main.contacts.phone')</small>
        <small class="m-t-10">@lang('site_lang::main.contacts.email')</small>
        <footer>
            @anchor('index', '<i class="zmdi zmdi-home"></i>')
            @anchor('contacts', '<i class="zmdi zmdi-local-post-office"></i>', 'bgm-lightblue')
            @dashboard
        </footer>
    </div>
@stop
@section('footer')
@stop
@section('scripts')
    {!! Html::script('core/js/vendor.js') !!}
    {!! Html::script('core/js/main.js') !!}
@stop