@set($menu_items, \PublicMenu::make($menu_slug))
@if(count($menu_items))
    <ul>
        @foreach($menu_items as $item)
            @if($item['hide'])
                @continue
            @endif
            <li class="{!! !empty($item['css']) ? ' '.$item['css'] : '' !!}">
                @if($item['item_type'] == 'page_link')
                    <a href="{!! route('public.page.'.$item['page']['slug']) !!}"{!! $item['blank'] ? ' target="_blank"' : '' !!}>{{ $item['title'] }}</a>
                @elseif($item['item_type'] == 'external_link')
                    <a href="{!! $item['link'] !!}"{!! $item['blank'] ? ' target="_blank"' : '' !!}>{{ $item['title'] }}</a>
                @elseif($item['item_type'] == 'anchor_link')
                    <a href="{!! route('public.page.'.$item['page']['slug']) !!}{!! $item['anchor'] !!}"{!! $item['blank'] ? ' target="_blank"' : '' !!}>{{ $item['title'] }}</a>
                @endif
                @if(!empty($item['sub_menu']))
                    <ul>
                        @foreach($item['sub_menu'] as $sub_menu_item)
                            @if($sub_menu_item['hide'])
                                @continue
                            @endif
                            <li class="{!! !empty($sub_menu_item['css']) ? ' '.$sub_menu_item['css'] : '' !!}">
                                @if($sub_menu_item['item_type'] == 'page_link')
                                    <a href="{!! route('public.page.'.$sub_menu_item['page']['slug']) !!}"{!! $sub_menu_item['blank'] ? ' target="_blank"' : '' !!}>{!! $sub_menu_item['title'] !!}</a>
                                @elseif($item['item_type'] == 'external_link')
                                    <a href="{!! $sub_menu_item['link'] !!}"{!! $sub_menu_item['blank'] ? ' target="_blank"' : '' !!}>{!! $sub_menu_item['title'] !!}</a>
                                @elseif($item['item_type'] == 'anchor_link')
                                    <a href="{!! route('apublic.page.'.$sub_menu_item['page']['slug']) !!}{!! $sub_menu_item['anchor'] !!}"{!! $sub_menu_item['blank'] ? ' target="_blank"' : '' !!}>{!! $item['title'] !!}</a>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
    </ul>
@endif