@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.title')) !!}
        </li>
        <li>
            <a href="{{ route('core.content.pages.index') }}">
                <i class="{{ config('core_content::menu.menu_child.pages.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.pages.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-view-subtitles"></i> @lang('core_content_lang::pages.blocks')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-view-subtitles"></i> @lang('core_content_lang::pages.blocks')
        </h2>
    </div>
    <a href="{{ route('core.content.pages.blocks_create', $page->id) }}"
       class="btn bgm-deeppurple m-btn btn-float waves-effect waves-circle waves-float">
        <i class="zmdi zmdi-plus"></i>
    </a>
    <div class="card">
        <div class="lv-header-alt clearfix">
            <h2 class="lvh-label hidden-xs">{{ $page->title }}</h2>
        </div>
        <div class="card-body card-padding m-h-250">
            @if($blocks->count())
                <div class="listview lv-bordered lv-lg">
                    <div class="lv-body">
                        @foreach($blocks as $block)
                            <div class="js-item-container lv-item media">
                                <div class="media-body">
                                    <div class="lv-title">{{ $block->title }}</div>
                                    <small class="lv-small">
                                        {!! \Illuminate\Support\Str::limit(strip_tags($block->content), 500) !!}
                                    </small>
                                    <ul class="lv-attrs">
                                        <li>
                                            @lang('core_content_lang::blocks.insert.form.slug'): {{ $block->slug }}
                                        </li>
                                    </ul>
                                    <div class="lv-actions actions dropdown">
                                        <a aria-expanded="true" data-toggle="dropdown" href="">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="{{ route('core.content.pages.blocks_edit', [$page->id, $block->id]) }}">@lang('core_content_lang::blocks.edit')</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);"
                                                   class="js-copy-link"
                                                   data-clipboard-text="{{ '@'.'PageBlock(\'' . $block->slug . '\')' }}">
                                                    @lang('core_content_lang::blocks.embed')
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                {!! Form::open(['route' => ['core.content.pages.blocks_destroy', $page->id, $block->id], 'method' => 'DELETE']) !!}
                                                <button type="submit"
                                                        class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                        autocomplete="off"
                                                        data-question="@lang('core_content_lang::blocks.delete.question') &laquo;{{ $block->slug }}&raquo;?"
                                                        data-confirmbuttontext="@lang('core_content_lang::blocks.delete.confirmbuttontext')"
                                                        data-cancelbuttontext="@lang('core_content_lang::blocks.delete.cancelbuttontext')">
                                                    @lang('core_content_lang::blocks.delete.submit')
                                                </button>
                                                {!! Form::close() !!}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else
                <h2 class="f-16 c-gray">@lang('core_content_lang::blocks.empty')</h2>
            @endif
        </div>
    </div>
@stop