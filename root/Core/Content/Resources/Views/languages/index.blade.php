@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_content::menu.menu_child.languages.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.languages.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_content::menu.menu_child.languages.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.languages.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div class="lv-header-alt clearfix">
            <h2 class="lvh-label hidden-xs">
                {{ \App::getLocale() }}: {{ ucfirst(basename($language_file, '.php')) }}
            </h2>
            @if($languages_files->count() > 1)
                <ul class="lv-actions actions">
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="true">
                            <i class="zmdi zmdi-filter-list"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            @foreach($languages_files as $file)
                                <li>
                                    <a href="{{ route('core.content.languages.index') . '?file=' . basename($file, '.php') }}">
                                        {{ ucfirst(basename($file, '.php')) }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            @endif
        </div>
        <div class="card-body card-padding m-h-250">
            <div class="row">
                {!! Form::open(['route' => 'core.content.languages.store', 'class' => 'form-validate', 'id' => 'edit-content-languages-form']) !!}
                {!! Form::hidden('file', basename($language_file)) !!}
                <div class="col-sm-9">
                    <div class="form-group">
                        <pre id="template_content">{{ $languages_content }}</pre>
                        {!! Form::textarea('content', NULL, ['class' => 'hidden', 'data-autosize-on' => 'true']) !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('core_content_lang::languages.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('modal')
@stop
@section('scripts_before')
    {!! Html::script('packages/content/js/ace.js') !!}
    <script>
        var editor = ace.edit("template_content");
        editor.setTheme("ace/theme/monokai");
        editor.getSession().setMode("ace/mode/javascript");
        editor.getSession().setUseSoftTabs(true);
        document.getElementById('template_content').style.fontSize = '14px';
        editor.getSession().setUseWrapMode(true);
        editor.setShowPrintMargin(false);
        editor.setOptions({
            maxLines: Infinity
        });
        $("form button[type='submit']").click(function () {
            $("form textarea[name='content']").val(editor.getValue());
        });
    </script>
@stop
@section('scripts_after')
@stop