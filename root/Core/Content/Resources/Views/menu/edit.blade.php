@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.title')) !!}
        </li>
        <li>
            <a href="{{ route('core.content.menu.index') }}">
                <i class="{{ config('core_content::menu.menu_child.menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.menu.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('core_content_lang::menu.replace.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-edit"></i> @lang('core_content_lang::menu.replace.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::model($menu, ['route' => ['core.content.menu.update', $menu->id], 'class' => 'form-validate', 'id' => 'edit-content-menu-form', 'method' => 'PUT']) !!}
                <div class="col-sm-4">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_content_lang::menu.insert.form.title')</label>
                    </div>
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('core_content_lang::menu.insert.form.template')</p>
                        {!! Form::select('template_id', $menu_templates, NULL, ['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('core_content_lang::menu.replace.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop