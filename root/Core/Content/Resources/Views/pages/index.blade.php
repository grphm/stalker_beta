@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_content::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_content::menu.menu_child.pages.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.pages.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_content::menu.menu_child.pages.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_content::menu.menu_child.pages.title')) !!}
        </h2>
    </div>
    @if(\PermissionsController::allowPermission('core_content', 'create', FALSE))
        @BtnAdd('core.content.pages.create')
    @endif
    <div class="card">
        <div class="lv-header-alt clearfix">
            <div class="lvh-search">
                {!! Form::open(['route' => 'core.content.pages.index', 'method' => 'get']) !!}
                <input type="text" name="search" placeholder="@lang('core_content_lang::pages.search')"
                       class="lvhs-input">
                <i class="lvh-search-close">&times;</i>
                {!! Form::close() !!}
            </div>
            <ul class="lv-actions actions">
                <li>
                    <a href="" class="lvh-search-trigger">
                        <i class="zmdi zmdi-search"></i>
                    </a>
                </li>
                @if($pages->count())
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                            <i class="zmdi zmdi-sort-amount-asc"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="{{ route('core.content.pages.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'asc'])) }}">
                                    @lang('core_content_lang::pages.sort_title')
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('core.content.pages.index', array_merge(Request::all(), ['sort_field' => 'published_at', 'sort_direction' => 'asc'])) }}">
                                    @lang('core_content_lang::pages.sort_published')
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('core.content.pages.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'asc'])) }}">
                                    @lang('core_content_lang::pages.sort_updated')
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                            <i class="zmdi zmdi-sort-amount-desc"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="{{ route('core.content.pages.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'desc'])) }}">
                                    @lang('core_content_lang::pages.sort_title')
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('core.content.pages.index', array_merge(Request::all(), ['sort_field' => 'published_at', 'sort_direction' => 'desc'])) }}">
                                    @lang('core_content_lang::pages.sort_published')
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('core.content.pages.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'desc'])) }}">
                                    @lang('core_content_lang::pages.sort_updated')
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                <li class="dropdown">
                    <a data-toggle="dropdown" href="" aria-expanded="false">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="{{ URL::route('core.content.cache.clear') }}">
                                @lang('core_content_lang::pages.cache_clear')
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="card-body card-padding m-h-250">
            @if($pages->count())
                <div class="listview lv-bordered lv-lg">
                    <div class="lv-body">
                        @foreach($pages as $page)
                            <div class="js-item-container lv-item media">
                                <div class="media-body">
                                    <div class="lv-title">{{ $page->title }}</div>
                                    @if($page->template)
                                        <small class="lv-small">{{ \STALKER_CMS\Vendor\Helpers\double_slash('/home/Resources/Views/' . \STALKER_CMS\Vendor\Helpers\settings(['core_content', 'pages', 'templates_dir']). '/' . $page->template->path) }}</small>
                                    @endif
                                    <ul class="lv-attrs">
                                        <li>ID: @numDimensions($page->id)</li>
                                        <li>
                                            @lang('core_content_lang::pages.published'):
                                            {!! $page->PublishedDate !!}
                                        </li>
                                        <li>
                                            @lang('core_content_lang::pages.update'):
                                            {!! $page->UpdatedDate !!}
                                        </li>
                                        <li>
                                            @lang('core_content_lang::pages.slug'):
                                            {{ $page->slug }}
                                        </li>
                                    </ul>
                                    <div class="lv-actions actions dropdown">
                                        <a aria-expanded="true" data-toggle="dropdown" href="">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            @if(\PermissionsController::allowPermission('core_content', 'edit', FALSE))
                                                <li>
                                                    <a href="{{ route('core.content.pages.edit', $page->id) }}">
                                                        @lang('core_content_lang::pages.edit')
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('core.content.pages.blocks_index', $page->id) }}">
                                                        @lang('core_content_lang::pages.blocks')
                                                    </a>
                                                </li>
                                            @endif
                                            @if($page->publication)
                                                <li>
                                                    <a href="{{ \PublicPage::changeLocale(\App::getLocale(), $page) }}"
                                                       target="_blank">
                                                        @lang('core_content_lang::pages.blank')
                                                    </a>
                                                </li>
                                            @endif
                                            <li>
                                                <a href="javascript:void(0);"
                                                   class="js-copy-link"
                                                   data-clipboard-text="{{ '@'.'anchor(\'' . $page->slug . '\', \'' . $page->title . '\')' }}">
                                                    @lang('core_content_lang::pages.embed')
                                                </a>
                                            </li>
                                            @if(\PermissionsController::allowPermission('core_content', 'delete', FALSE))
                                                <li class="divider"></li>
                                                <li>
                                                    {!! Form::open(['route' => ['core.content.pages.destroy', $page->id], 'method' => 'DELETE']) !!}
                                                    <button type="submit"
                                                            class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                            autocomplete="off"
                                                            data-question="@lang('core_content_lang::pages.delete.question') &laquo;{{ $page->title }}&raquo;?"
                                                            data-confirmbuttontext="@lang('core_content_lang::pages.delete.confirmbuttontext')"
                                                            data-cancelbuttontext="@lang('core_content_lang::pages.delete.cancelbuttontext')">
                                                        @lang('core_content_lang::pages.delete.submit')
                                                    </button>
                                                    {!! Form::close() !!}
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else
                <h2 class="f-16 c-gray">@lang('core_content_lang::pages.empty')</h2>
            @endif
        </div>
    </div>
@stop