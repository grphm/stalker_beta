<?php
namespace STALKER_CMS\Core\Content\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ModuleTableSeeder extends Seeder {

    public function run() {

        \DB::table('content_pages_templates')->insert([
            'menu_type' => 'page', 'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Основной слой', 'en' => 'Base layout', 'es' => 'Diseño base']),
            'path' => 'layout.blade.php', 'required' => TRUE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        \DB::table('content_pages_templates')->insert([
            'menu_type' => 'page', 'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Главная страница', 'en' => 'Main page', 'es' => 'Pagina principal']),
            'path' => 'index.blade.php', 'required' => FALSE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        \DB::table('content_pages_templates')->insert([
            'menu_type' => 'page', 'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Страница контактов', 'en' => 'Contacts page', 'es' => 'Página de contactos']),
            'path' => 'contacts.blade.php', 'required' => FALSE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        \DB::table('content_pages_templates')->insert([
            'menu_type' => 'menu', 'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Верхнее меню', 'en' => 'Top menu', 'es' => 'Menú de arriba']),
            'path' => 'top-menu.blade.php', 'required' => FALSE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        \DB::table('content_pages_templates')->insert([
            'menu_type' => 'menu', 'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Нижнее меню', 'en' => 'Bottom menu', 'es' => 'Menú de inferior']),
            'path' => 'bottom-menu.blade.php', 'required' => FALSE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        \DB::table('content_pages')->insert([
            'slug' => 'index', 'locale' => env('APP_LOCALE', 'ru'), 'template_id' => 2,
            'publication' => TRUE, 'start_page' => TRUE, 'title' => $this->translate(['ru' => 'Главная', 'en' => 'Main', 'es' => 'Los principales']),
            'seo_title' => NULL, 'seo_description' => NULL, 'seo_h1' => NULL, 'seo_url' => NULL,
            'open_graph' => NULL, 'published_at' => Carbon::now(), 'user_id' => NULL,
            'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);

        \DB::table('content_pages')->insert([
            'slug' => 'contacts', 'locale' => env('APP_LOCALE', 'ru'), 'template_id' => 3,
            'publication' => TRUE, 'start_page' => FALSE, 'title' => $this->translate(['ru' => 'Контакты', 'en' => 'Contacts', 'es' => 'Contactos']),
            'seo_title' => NULL, 'seo_description' => NULL, 'seo_h1' => NULL, 'seo_url' => NULL,
            'open_graph' => NULL, 'published_at' => Carbon::now(), 'user_id' => NULL,
            'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
    }

    private function translate(array $trans) {

        return array_first($trans, function ($key, $value) {
            return $key == \App::getLocale();
        });
    }
}