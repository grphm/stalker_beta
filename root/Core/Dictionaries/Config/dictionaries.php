<?php
return [
    'package_name' => 'core_dictionaries',
    'package_title' => ['ru' => 'Модуль словарей', 'en' => 'Module dictionaries', 'es' => 'Módulo diccionarios'],
    'package_icon' => 'zmdi zmdi-collection-bookmark',
    'version' => [
        'ver' => 0.8,
        'date' => '23.09.2016'
    ],
    'form_elements' => [
        'text' => ['ru' => 'Текстовое поле', 'en' => 'Text field', 'es' => 'Сampo de texto'],
        'textarea' => ['ru' => 'Текстовая область', 'en' => 'Text area', 'es' => 'Area de texto'],
        'select' => ['ru' => 'Выпадающий список', 'en' => 'Drop-down list', 'es' => 'La lista desplegable'],
        'checkbox' => ['ru' => 'Флажок', 'en' => 'Checkbox', 'es' => 'Bandera'],
        'file' => ['ru' => 'Выбора файла', 'en' => 'Select file', 'es' => 'Seleccionar archivo'],
        'page' => ['ru' => 'Выбор страницы', 'en' => 'Select page', 'es' => 'Seleccionar página'],
        'image' => ['ru' => 'Загрузка изображения', 'en' => 'Loading image', 'es' => 'La carga de imágenes'],
        'datapicker' => ['ru' => 'Поле для ввода даты', 'en' => 'Field to enter the date', 'es' => 'Campo para la introducción de la fecha'],
    ]
];