<?php
namespace STALKER_CMS\Core\Dictionaries\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Core\Content\Models\Page;
use STALKER_CMS\Core\Dictionaries\Models\Dictionary;
use STALKER_CMS\Core\Dictionaries\Models\DictionaryLists;
use STALKER_CMS\Core\Dictionaries\Models\DictionaryListsFields;
use STALKER_CMS\Core\Uploads\Models\Upload;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Helpers as Helpers;

/**
 * Контроллер элемента словаря
 * Class DictionaryListsController
 * @package STALKER_CMS\Core\Dictionaries\Http\Controllers
 */
class DictionaryListsController extends ModuleController implements CrudInterface {

    /**
     * @var
     */
    protected $dictionary;
    /**
     * @var DictionaryLists
     */
    protected $model;
    /**
     * @var DictionaryListsFields
     */
    protected $fields;

    /**
     * DictionaryListsController constructor.
     * @param Dictionary $dictionary
     * @param DictionaryLists $lists
     * @param DictionaryListsFields $fields
     */
    public function __construct(Dictionary $dictionary, DictionaryLists $lists, DictionaryListsFields $fields) {

        $this->dictionary = $dictionary->whereLocale(\App::getLocale())->findOrFail(\Request::segment(3));
        $this->model = $lists;
        $this->fields = $fields;
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_dictionaries', 'dictionaries');
        return view('core_dictionaries_views::lists.index', [
            'dictionary' => $this->dictionary,
            'lists' => $this->model->whereDictionaryId($this->dictionary->id)->orderBy('order')->get()
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        \PermissionsController::allowPermission('core_dictionaries', 'create');
        $this->dictionary->structure = json_decode($this->dictionary->structure, TRUE);
        $pages = $files = [];
        if (\PermissionsController::isPackageEnabled('core_content')):
            $pages[''] = Helpers\array_translate(['ru' => 'Не указано', 'en' => '', 'es' => '']);
            foreach (Page::whereLocale(\App::getLocale())->wherePublication(TRUE)->lists('title', 'slug') as $slug => $title):
                $pages[$slug] = $title;
            endforeach;
        endif;
        if (\PermissionsController::isPackageEnabled('core_uploads')):
            $files[''] = Helpers\array_translate(['ru' => 'Не указано', 'en' => '', 'es' => '']);
            foreach (Upload::lists('original_name', 'path') as $id => $original_name):
                $files[$id] = $original_name;
            endforeach;
        endif;
        return view('core_dictionaries_views::lists.create', ['dictionary' => $this->dictionary, 'pages' => $pages, 'files' => $files]);
    }

    /**
     * @param null $dictionary_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($dictionary_id = NULL) {

        \PermissionsController::allowPermission('core_dictionaries', 'create');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            $list = $this->model->insert($request);
            foreach ($request::input('fields') as $field_name => $field_value):
                if (is_array($field_value) && !empty($field_value)):
                    $field_value = implode(',', $field_value);
                endif;
                $this->fields->insert(['dictionary_list_id' => $list->id, 'field' => $field_name, 'value' => $field_value]);
            endforeach;
            if ($image = $this->uploadImage($request, 'photo')):
                $this->fields->insert(['dictionary_list_id' => $list->id, 'field' => 'photo', 'value' => $image]);
            endif;
            if ($image = $this->uploadImage($request, 'background')):
                $this->fields->insert(['dictionary_list_id' => $list->id, 'field' => 'background', 'value' => $image]);
            endif;
            return \ResponseController::success(201)->redirect(route('core.dictionaries.lists_index', $dictionary_id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param null $dictionary_id
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($dictionary_id = NULL, $id = NULL) {

        \PermissionsController::allowPermission('core_dictionaries', 'edit');
        $item = $this->model->findOrFail($id);
        $fields = $this->fields->whereDictionaryListId($item->id)->lists('value', 'field');
        $structure = [];
        foreach (json_decode($this->dictionary->structure, TRUE) as $index => $element):
            $structure[$index] = $element;
            $structure[$index]['value'] = isset($fields[$element['name']]) ? $fields[$element['name']] : NULL;
        endforeach;
        $this->dictionary->structure = $structure;
        $pages = $files = [];
        if (\PermissionsController::isPackageEnabled('core_content')):
            $pages[''] = Helpers\array_translate(['ru' => 'Не указано', 'en' => '', 'es' => '']);
            foreach (Page::whereLocale(\App::getLocale())->wherePublication(TRUE)->lists('title', 'slug') as $slug => $title):
                $pages[$slug] = $title;
            endforeach;
        endif;
        if (\PermissionsController::isPackageEnabled('core_uploads')):
            $files[''] = Helpers\array_translate(['ru' => 'Не указано', 'en' => '', 'es' => '']);
            foreach (Upload::lists('original_name', 'path') as $id => $original_name):
                $files[$id] = $original_name;
            endforeach;
        endif;
        return view('core_dictionaries_views::lists.edit', ['dictionary' => $this->dictionary, 'item' => $item, 'pages' => $pages, 'files' => $files]);
    }

    /**
     * @param null $dictionary_id
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($dictionary_id = NULL, $id = NULL) {

        \PermissionsController::allowPermission('core_dictionaries', 'edit');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->replace($id, $request);
            foreach ($request::input('fields') as $field_name => $field_value):
                if (is_array($field_value) && !empty($field_value)):
                    $field_value = implode(',', $field_value);
                endif;
                $this->fields->replace(['dictionary_list_id' => $id, 'field' => $field_name], $field_value);
            endforeach;
            if ($image = $this->uploadImage($request, 'photo')):
                $this->fields->replace(['dictionary_list_id' => $id, 'field' => 'photo'], $image);
            endif;
            if ($image = $this->uploadImage($request, 'background')):
                $this->fields->replace(['dictionary_list_id' => $id, 'field' => 'background'], $image);
            endif;
            return \ResponseController::success(202)->redirect(route('core.dictionaries.lists_index', $dictionary_id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param null $dictionary_id
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($dictionary_id = NULL, $id = NULL) {

        \PermissionsController::allowPermission('core_dictionaries', 'delete');
        $request = \RequestController::isAJAX()->init();
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('core.dictionaries.lists_index', $dictionary_id))->json();
    }

    /**************************************************************************************************************/

    /**
     * Загрузка изображения
     * @param \Request $request
     * @param $field
     * @return mixed
     */
    private function uploadImage(\Request $request, $field) {

        if ($request::hasFile($field)):
            $upload_directory = 'dictionaries';
            $fileName = time() . "_" . rand(1000, 1999) . '.' . $request::file($field)->getClientOriginalExtension();
            $request::file($field)->move('uploads/' . $upload_directory, $fileName);
            return Helpers\add_first_slash($upload_directory . '/' . $fileName);
        endif;
    }

    /*
        if ($photo = Helpers\trim_base64_image($request::input($field))):
            $upload_directory = 'dictionaries';
            $fileName = time() . "_" . rand(1000, 1999) . '.jpg';
            $photoPath = Helpers\double_slash($upload_directory . '/' . $fileName);
            \Storage::put($photoPath, base64_decode($photo));
            return Helpers\add_first_slash($photoPath);
        else:
            return NULL;
        endif;
    */

    /**
     * Удаление изображения
     * @param $dictionary_list_id
     * @param $field
     */
    private function deleteImage($dictionary_list_id, $field) {

        if ($ListsField = $this->fields->whereDictionaryListId($dictionary_list_id)->whereField($field)->first()):
            if (!empty($ListsField->value) && \Storage::exists($ListsField->value)):
                \Storage::delete($ListsField->value);
                $ListsField->value = NULL;
            endif;
            $ListsField->save();
        endif;
    }

    /**
     * Сортировка элементов словаря
     * @return \Illuminate\Http\JsonResponse
     */
    public function sortable() {

        \PermissionsController::allowPermission('core_dictionaries', 'dictionaries');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, ['elements' => 'required'])):
            foreach (explode(',', $request::input('elements')) as $index => $slide_id):
                $this->model->where('id', $slide_id)->update(['order' => $index + 1]);
            endforeach;
            return \ResponseController::success(202)->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }
}