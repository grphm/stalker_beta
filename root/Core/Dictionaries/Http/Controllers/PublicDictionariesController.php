<?php
namespace STALKER_CMS\Core\Dictionaries\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Core\Dictionaries\Models\Dictionary;
use STALKER_CMS\Core\Dictionaries\Models\DictionaryLists;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Helpers as Helpers;

/**
 * Контроллер для получения словарей в гостевом интерфейсе
 * Class PublicDictionariesController
 * @package STALKER_CMS\Core\Dictionaries\Http\Controllers
 */
class PublicDictionariesController extends ModuleController {

    /**
     * Получить список элементов словаря по символьному коду
     * @param $slug
     * @return Collection
     */
    public function lists($slug) {

        $lists = new Collection();
        $dictionary = Dictionary::whereSlug($slug)->whereLocale(\App::getLocale())->with(['lists' => function ($query) {
            $query->orderBy('order');
            $query->with('fields');
        }])->first();
        if ($dictionary):
            foreach ($dictionary->lists as $index => $list):
                $items[$index] = [];
                foreach ($list->fields as $field):
                    $items[$index]['id'] = $list['id'];
                    $items[$index][$field['field']] = $field['value'];
                endforeach;
                $lists->add($items[$index]);
            endforeach;
        endif;
        return $lists;
    }

    /**
     * Получить значения элемента словаря по символьному коду по ID элемента
     * @param $slug
     * @param $id
     * @return mixed|null
     */
    public function fields($slug, $id) {

        $items = $this->lists($slug);
        if ($items->count()):
            foreach ($items as $item):
                if ($item['id'] == $id):
                    return $item;
                endif;
            endforeach;
        endif;
        return NULL;
    }
}