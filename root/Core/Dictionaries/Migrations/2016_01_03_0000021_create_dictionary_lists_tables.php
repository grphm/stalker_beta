<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDictionaryListsTables extends Migration {

    public function up() {

        Schema::create('dictionary_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dictionary_id', FALSE, TRUE)->nullable()->index();
            $table->string('title', 100)->nullable();
            $table->integer('order', FALSE, TRUE)->nullable()->index();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('dictionary_lists');
    }
}

