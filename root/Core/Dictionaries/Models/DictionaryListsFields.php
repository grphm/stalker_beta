<?php
namespace STALKER_CMS\Core\Dictionaries\Models;

use Carbon\Carbon;
use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель Поля элемента словаря
 * Class DictionaryListsFields
 * @package STALKER_CMS\Core\Dictionaries\Models
 */
class DictionaryListsFields extends BaseModel implements ModelInterface {

    use ModelTrait;

    /**
     * @var string
     */
    protected $table = 'dictionary_lists_fields';
    /**
     * @var array
     */
    protected $fillable = [];
    /**
     * @var array
     */
    protected $hidden = [];
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @param $request
     * @return DictionaryListsFields
     */
    public function insert($request) {

        $model = new DictionaryListsFields();
        $model->dictionary_list_id = $request['dictionary_list_id'];
        $model->field = $request['field'];
        $model->value = $request['value'];
        $model->save();
        return $model;
    }

    /**
     * @param $attributes
     * @param $value
     * @return DictionaryListsFields
     */
    public function replace($attributes, $value) {

        if (!$model = $this::where($attributes)->first()):
            $model = new DictionaryListsFields();
            $model->dictionary_list_id = $attributes['dictionary_list_id'];
            $model->field = $attributes['field'];
        endif;
        $model->value = $value;
        $model->save();
        $model->touch();
        return $model;
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        return static::where('dictionary_list_id', $id)->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['dictionary_list_id' => 'required'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return [];
    }
}