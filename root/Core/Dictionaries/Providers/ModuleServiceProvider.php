<?php

namespace  STALKER_CMS\Core\Dictionaries\Providers;

use STALKER_CMS\Core\Dictionaries\Http\Controllers\PublicDictionariesController;
use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

/**
 * Class ModuleServiceProvider
 * @package STALKER_CMS\Core\Dictionaries\Providers
 */
class ModuleServiceProvider extends BaseServiceProvider {

    /**
     * Метод загрузки
     */
    public function boot() {

        $this->setPath(__DIR__ . '/../');
        $this->registerViews('core_dictionaries_views');
        $this->registerLocalization('core_dictionaries_lang');
        $this->registerConfig('core_dictionaries::config', 'Config/dictionaries.php');
        $this->registerSettings('core_dictionaries::settings', 'Config/settings.php');
        $this->registerActions('core_dictionaries::actions', 'Config/actions.php');
        $this->registerSystemMenu('core_dictionaries::menu', 'Config/menu.php');
    }

    /**
     * Метода регистрации
     */
    public function register() {

        \App::bind('PublicDictionariesController', function () {
            return new PublicDictionariesController();
        });
    }
}
