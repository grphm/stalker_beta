<?php

return [
    'lists' => 'Elements of the dictionary',
    'edit' => 'Edit',
    'delete' => [
        'question' => 'Delete dictionary',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete'
    ],
    'slug' => 'Symbolic code',
    'elements' => 'Count of elements',
    'empty' => 'List is empty',
    'choice_element' => 'Form element',
    'form_element_add' => 'Add',
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding dictionary',
        'form' => [
            'form_constructor' => 'Designer dictionary fields',
            'remove-element' => 'Delete element',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters',
            'title' => 'Title',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit page',
        'form' => [
            'form_constructor' => 'Designer dictionary fields',
            'remove-element' => 'Delete element',
            'title' => 'Title',
            'submit' => 'Save'
        ]
    ],
    'select_data_items' => [
        'countries' => 'Countries',
        'cities' => 'Cities',
        'gallery' => 'Galleries',
        'upload' => 'Uploaded files',
    ]
];