<?php

return [
    'text' => [
        'title' => 'Text field',
        'name' => 'The name of the text field [attribute name]',
        'placeholder' => 'Description form field'
    ],
    'textarea' => [
        'title' => 'Text area',
        'name' => 'The name of the text field [attribute name]',
        'placeholder' => 'Description form field',
        'redactor' => 'Use the visual editor'
    ],
    'select' => [
        'title' => 'Drop-down list',
        'name' => 'Name drop-down list [attribute name]',
        'placeholder' => 'Description drop-down list',
        'data_item' => 'Information list',
        'multiple' => 'Multiple selection'
    ],
    'checkbox' => [
        'title' => 'Checkbox',
        'name' => 'Checkbox name [attribute name]',
        'placeholder' => 'Description of the checkbox'
    ],
    'file' => [
        'title' => 'Select file',
        'name' => 'Field name [attribute name]',
        'placeholder' => 'Description form field'
    ],
    'page' => [
        'title' => 'Select page',
        'name' => 'Field name [attribute name]',
        'placeholder' => 'Description form field'
    ],
    'image' => [
        'title' => 'Loading image',
        'name' => 'Field name [attribute name]',
        'placeholder' => 'Description form field',
        'image_help_description' => 'Supported formats: png, jpg, gif',
        'image_select' => 'Select',
        'image_change' => 'Change',
        'image_delete' => 'Delete'
    ],
    'datapicker' => [
        'title' => 'Field to enter the date',
        'name' => 'Field name [attribute name]',
        'placeholder' => 'Description form field'
    ]
];