<?php

return [
    'edit' => 'Edit',
    'delete' => [
        'question' => 'Delete dictionary item',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete'
    ],
    'empty' => 'List is empty',
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding a dictionary item',
        'form' => [
            'title' => 'Title',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Editing the dictionary item',
        'form' => [
            'title' => 'Title',
            'submit' => 'Save'
        ]
    ]
];