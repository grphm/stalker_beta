<?php

return [
    'lists' => 'Elementos del diccionario',
    'edit' => 'Editar',
    'delete' => [
        'question' => 'Eliminar diccionario',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar'
    ],
    'slug' => 'Simbólico código',
    'elements' => 'Número de elementos',
    'empty' => 'Lista está vacía',
    'choice_element' => 'Elemento conformado',
    'form_element_add' => 'Añadir',
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de la página',
        'form' => [
            'form_constructor' => 'Campos de diccionario diseñador',
            'remove-element' => 'Eliminar elemento',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres',
            'title' => 'Título',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar de la página',
        'form' => [
            'form_constructor' => 'Campos de diccionario diseñador',
            'remove-element' => 'Eliminar elemento',
            'title' => 'Título',
            'submit' => 'Guardar'
        ]
    ],
    'select_data_items' => [
        'countries' => 'Países',
        'cities' => 'Ciudades',
        'gallery' => 'Galerías',
        'upload' => 'Archivos cargados',
    ]
];