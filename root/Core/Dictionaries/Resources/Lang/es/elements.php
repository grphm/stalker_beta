<?php

return [
    'text' => [
        'title' => 'Сampo de texto',
        'name' => 'El nombre del campo de texto [nombre de atributo]',
        'placeholder' => 'Descripción del campo de formulario'
    ],
    'textarea' => [
        'title' => 'Area de texto',
        'name' => 'El nombre del campo de texto [nombre de atributo]',
        'placeholder' => 'Descripción del campo de formulario',
        'redactor' => 'Utilice el editor visual'
    ],
    'select' => [
        'title' => 'la lista desplegable',
        'name' => 'Nombre de la lista desplegable [nombre de atributo]',
        'placeholder' => 'Descripción lista desplegable',
        'data_item' => 'lista de información',
        'multiple' => 'De opción múltiple'
    ],
    'checkbox' => [
        'title' => 'Bandera',
        'name' => 'Nombre casilla [nombre de atributo]',
        'placeholder' => 'Descripción de la bandera'
    ],
    'file' => [
        'title' => 'Seleccionar archivo',
        'name' => 'Nombre del campo [nombre de atributo]',
        'placeholder' => 'Descripción del campo'
    ],
    'page' => [
        'title' => 'Seleccionar página',
        'name' => 'Nombre del campo [nombre de atributo]',
        'placeholder' => 'Descripción del campo'
    ],
    'image' => [
        'title' => 'La carga de imágenes',
        'name' => 'Nombre del campo [nombre de atributo]',
        'placeholder' => 'Descripción del campo',
        'image_help_description' => 'Formatos soportados: png, jpg, gif',
        'image_select' => 'Selecto',
        'image_change' => 'Enmendar',
        'image_delete' => 'Eliminar'
    ],
    'datapicker' => [
        'title' => 'Campo para la introducción de la fecha',
        'name' => 'Nombre del campo [nombre de atributo]',
        'placeholder' => 'Descripción del campo'
    ]
];