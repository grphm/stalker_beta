<?php

return [
    'lists' => 'Элементы словаря',
    'edit' => 'Редактировать',
    'delete' => [
        'question' => 'Удалить словарь',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'slug' => 'Символьный код',
    'elements' => 'Количество элементов',
    'empty' => 'Список пустой',
    'choice_element' => 'Элемент формы',
    'form_element_add' => 'Добавить',
    'insert' => [
        'breadcrumb' => 'Добавить',
        'title' => 'Добавление словаря',
        'form' => [
            'form_constructor' => 'Конструктор полей словаря',
            'remove-element' => 'Удалить элемент',
            'slug' => 'Символьный код',
            'slug_help_description' => 'Только латинские символы, символы подчеркивания, тире, не менее 5 символов',
            'title' => 'Название',
            'submit' => 'Сохранить'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Редактировать',
        'title' => 'Редактирование словаря',
        'form' => [
            'form_constructor' => 'Конструктор полей словаря',
            'remove-element' => 'Удалить елемент',
            'title' => 'Название',
            'submit' => 'Сохранить'
        ]
    ],
    'select_data_items' => [
        'countries' => 'Страны',
        'cities' => 'Населенные пункты',
        'gallery' => 'Галереи',
        'upload' => 'Загруженные файлы',
    ]
];