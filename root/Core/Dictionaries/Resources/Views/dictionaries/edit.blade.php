@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_dictionaries::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_dictionaries::menu.title')) !!}
        </li>
        <li>
            <a href="{{ route('core.dictionaries.index') }}">
                <i class="{{ $dictionaries_icon }}"></i> {!! $dictionaries_title !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-edit"></i> @lang('core_dictionaries_lang::dictionaries.replace.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-edit"></i> @lang('core_dictionaries_lang::dictionaries.replace.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('core_dictionaries_lang::dictionaries.choice_element')</p>
                        {!! Form::select(NULL, $categories, NULL, ['id' => 'selectFormElement', 'class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <button type="submit" id="addFormElement" autocomplete="off"
                            class="btn btn-primary btn-sm m-t-30 waves-effect">
                        <i class="fa fa-plus"></i>
                        <span class="btn-text">@lang('core_dictionaries_lang::dictionaries.form_element_add')</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::model($dictionary, ['route' => ['core.dictionaries.update', $dictionary->id], 'class' => 'form-validate', 'id' => 'edit-dictionary-form', 'method' => 'PUT']) !!}
                <div class="col-sm-9">
                    <div id="form-structure">
                        @foreach(json_decode($dictionary->structure, TRUE) as $element)
                            <div class="clone-element">
                                @include('core_dictionaries_views::dictionaries.elements.' . $element['type'], ['element' => $element, 'delete' => FALSE])
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_dictionaries_lang::dictionaries.insert.form.title')</label>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                                <i class="fa fa-save"></i>
                                <span class="btn-text">@lang('core_dictionaries_lang::dictionaries.insert.form.submit')</span>
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="hidden">
        <div class="form-element-text clone-element">
            @include('core_dictionaries_views::dictionaries.elements.text', ['element' => ['name' => NULL, 'placeholder' => NULL], 'delete' => TRUE])
        </div>
        <div class="form-element-textarea clone-element">
            @include('core_dictionaries_views::dictionaries.elements.textarea', ['element' => ['name' => NULL, 'placeholder' => NULL, 'redactor' => NULL], 'delete' => TRUE])
        </div>
        <div class="form-element-select clone-element">
            @include('core_dictionaries_views::dictionaries.elements.select', ['element' => ['name' => NULL, 'placeholder' => NULL, 'data_item' => NULL], 'delete' => TRUE])
        </div>
        <div class="form-element-checkbox clone-element">
            @include('core_dictionaries_views::dictionaries.elements.checkbox', ['element' => ['name' => NULL, 'placeholder' => NULL], 'delete' => TRUE])
        </div>
        <div class="form-element-file clone-element">
            @include('core_dictionaries_views::dictionaries.elements.file', ['element' => ['name' => NULL, 'placeholder' => NULL], 'delete' => TRUE])
        </div>
        <div class="form-element-page clone-element">
            @include('core_dictionaries_views::dictionaries.elements.page', ['element' => ['name' => NULL, 'placeholder' => NULL], 'delete' => TRUE])
        </div>
        <div class="form-element-image clone-element">
            @include('core_dictionaries_views::dictionaries.elements.image', ['element' => ['name' => NULL, 'placeholder' => NULL], 'delete' => TRUE])
        </div>
        <div class="form-element-datapicker clone-element">
            @include('core_dictionaries_views::dictionaries.elements.datapicker', ['element' => ['name' => NULL, 'placeholder' => NULL], 'delete' => TRUE])
        </div>
    </div>
@stop
@section('scripts_before')
    <script>
        var number_element = 0;
        $("#form-structure .clone-element").each(function (index_clone_element, clone_element) {
            number_element++;
            $(clone_element).find(".element-input-name").each(function (index, element) {
                var name = $(element).attr('name');
                $(element).attr('name', 'elements[' + number_element + '][' + name + ']');
            });
        });
        $("#addFormElement").click(function () {
            if ($(".form-element-" + $("#selectFormElement").val()).length) {
                var clone_elements_count = $('#form-structure .clone-element').length;
                if (clone_elements_count > 0) {
                    $(".form-element-" + $("#selectFormElement").val()).clone(true).insertAfter($('#form-structure .clone-element:last'));
                } else {
                    $(".form-element-" + $("#selectFormElement").val()).clone(true).prependTo('#form-structure');
                }
                var clone_element = $("#form-structure .clone-element:last");
                $(clone_element).removeAttr('class').addClass('clone-element');
                $(clone_element).find(".element-input-name").each(function (index, element) {
                    var name = $(element).attr('name');
                    var next_element = clone_elements_count + 1;
                    $(element).attr('name', 'elements[' + next_element + '][' + name + ']');
                });
                $(this).after('<div class="status-inserted m-t-40 c-green pull-right" style="width: 60px;">Ok</div>');
                $(".status-inserted").fadeOut(1000, function () {
                    $(this).remove();
                });
                $("#edit-dictionary-form select").SelectizeInput();
            }
        });
        $(document).on('click', '.remove-clone-element', function (event) {
            event.preventDefault();
            $(this).parents('.clone-element').remove();
        });
    </script>
@stop
@section('scripts_after')
    <script>
        $("#edit-dictionary-form select").SelectizeInput();
    </script>
@stop