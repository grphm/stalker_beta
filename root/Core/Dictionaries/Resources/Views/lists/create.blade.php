@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li>
            <a href="{{ route('core.dictionaries.index') }}">
                <i class="{{ config('core_dictionaries::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_dictionaries::menu.title')) !!}
            </a>
        </li>
        <li>
            <a href="{{ route('core.dictionaries.lists_index', $dictionary->id) }}">
                <i class="{{ config('core_dictionaries::menu.menu_child.dictionaries.icon') }}"></i> {{ $dictionary->title }}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-plus"></i> @lang('core_dictionaries_lang::lists.insert.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-plus"></i> @lang('core_dictionaries_lang::lists.insert.title')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::open(['route' => ['core.dictionaries.lists_store', $dictionary->id], 'class' => 'form-validate', 'id' => 'add-dictionary-list-form', 'files' => TRUE]) !!}
                {!! Form::hidden('dictionary_id', $dictionary->id) !!}
                <div class="col-sm-9">
                    @foreach($dictionary->structure as $element)
                        @include('core_dictionaries_views::lists.elements.' . $element['type'], compact('element'))
                    @endforeach
                </div>
                <div class="col-sm-3">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                        </div>
                        <label class="fg-label">@lang('core_dictionaries_lang::lists.insert.form.title')</label>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                                <i class="fa fa-save"></i>
                                <span class="btn-text">@lang('core_dictionaries_lang::dictionaries.insert.form.submit')</span>
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
    @set($summernote_locale, \App::getLocale() . '-' . strtoupper(\App::getLocale()))
    {!! Html::script('core/js/summernote/summernote-' . $summernote_locale . '.js') !!}
    <script>
        $(".redactor").summernote({
            height: 250,
            tabsize: 2,
            lang: '{{ $summernote_locale }}',
            toolbar: [
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture']],
                ['view', ['fullscreen', 'codeview']],
            ]
        });
        $('.redactor').on('summernote.change', function (we, contents, $editable) {
            $(this).html(contents);
            $(this).change();
        });
        $(".checkbox input").click(function () {
            if ($(this).prop("checked")) {
                $(this).parents('.form-group').find('input[type="text"]').attr('value', 1);
            } else {
                $(this).parents('.form-group').find('input[type="text"]').attr('value', 0);
            }
        });
        $("#add-dictionary-list-form .tag-select-multiple").SelectizeInput();
    </script>
@stop