<?php

return [
    'galleries' => [
        'title' => ['ru' => 'Просмотр', 'en' => 'View', 'es' => 'Ver'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-eye'
    ],
    'create' => [
        'title' => ['ru' => 'Создание', 'en' => 'Add', 'es' => 'Añadir'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-collection-plus'
    ],
    'edit' => [
        'title' => ['ru' => 'Редактирование', 'en' => 'Edit', 'es' => 'Edición'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-edit'
    ],
    'delete' => [
        'title' => ['ru' => 'Удаление', 'en' => 'Delete', 'es' => 'Eliminar'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-delete'
    ],
    'images' => [
        'title' => [
            'ru' => 'Работа с изображениями',
            'en' => 'Working with images',
            'es' => 'Trabajar con imágenes'
        ],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-collection-image'
    ],
    'templates' => [
        'title' => ['ru' => 'Работа с шаблонами', 'en' => 'Working with templates', 'es' => 'Trabajar con plantillas'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-layers'
    ]
];