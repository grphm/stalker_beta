<?php
namespace STALKER_CMS\Core\Galleries\Http\Controllers;

use STALKER_CMS\Core\Galleries\Facades\PublicGalleries;
use STALKER_CMS\Core\Galleries\Models\Gallery;
use STALKER_CMS\Core\Galleries\Models\GalleryTemplate;
use STALKER_CMS\Core\Galleries\Models\Photo;
use \STALKER_CMS\Vendor\Helpers as Helpers;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

/**
 * Контроллер Галерия
 * Class GalleriesController
 * @package STALKER_CMS\Core\Galleries\Http\Controllers
 */
class GalleriesController extends ModuleController implements CrudInterface {

    /**
     * Модель
     * @var Gallery
     */
    protected $model;
    /**
     * @var Photo
     */
    protected $photo;

    /**
     * GalleriesController constructor.
     * @param Gallery $gallery
     * @param Photo $photo
     */
    public function __construct(Gallery $gallery, Photo $photo) {

        $this->model = $gallery;
        $this->photo = $photo;
        $this->middleware('auth');
    }

    /**
     * Список созданных галерей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_galleries', 'galleries');
        $galleries = Gallery::with(['photos' => function ($query) {
            $query->orderBy('order');
        }])->with('template')->orderBy('created_at', 'DESC')->get();
        return view('core_galleries_views::galleries.index', compact('galleries'));
    }

    /**
     * Создание галереи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        \PermissionsController::allowPermission('core_galleries', 'create');
        $templates = GalleryTemplate::whereLocale(\App::getLocale())->lists('title', 'id');
        return view('core_galleries_views::galleries.create', compact('templates'));
    }

    /**
     * Сохраняем новую галерею
     * @return \Illuminate\Http\JsonResponse
     */
    public function store() {

        \PermissionsController::allowPermission('core_galleries', 'create');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getStoreRules())):
            $this->model->uniqueness($request::only('slug'));
            $request::merge(['aspect_ratio' => Helpers\trim_delimiter($request::input('aspect_ratio'), '/')]);
            $gallery = $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('core.galleries.photos_index', $gallery->id))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id) {

        \PermissionsController::allowPermission('core_galleries', 'edit');
        $gallery = Gallery::findOrFail($id);
        $templates = GalleryTemplate::whereLocale(\App::getLocale())->lists('title', 'id');
        return view('core_galleries_views::galleries.edit', compact('gallery', 'templates'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update($id) {

        \PermissionsController::allowPermission('core_galleries', 'edit');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $request::merge(['aspect_ratio' => Helpers\trim_delimiter($request::input('aspect_ratio'), '/')]);
            $this->model->uniqueness($request::only('slug'), $id);
            $gallery = $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('core.galleries.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \PermissionsController::allowPermission('core_galleries', 'delete');
        $request = \RequestController::isAJAX()->init();
        if ($model = $this->model->whereFind(['id' => $id])):
            $this->deletePhotos($model);
            $model->delete();
        endif;
        return \ResponseController::success(1203)->redirect(route('core.galleries.index'))->json();
    }

    /**
     * @param Gallery $model
     */
    private function deletePhotos(Gallery $model) {

        foreach ($model->photos as $photo):
            $this->photo->remove($photo->id);
        endforeach;
    }
}