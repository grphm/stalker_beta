<?php
namespace STALKER_CMS\Core\Galleries\Http\Controllers;

use STALKER_CMS\Vendor\Http\Controllers\Controller;

/**
 * Основной контроллер пакета
 * Class ModuleController
 * @package STALKER_CMS\Core\Galleries\Http\Controllers
 */
class ModuleController extends Controller {

    /**
     * Возвращает список доступных типов файлов для загрузки в галерею
     * @return array
     */
    public static function getAllowedFileTypes() {

        $allowed_types = [];
        $maxFileSize = \STALKER_CMS\Vendor\Helpers\settings(['core_galleries', 'galleries', 'upload_max_file_size']);
        $allowed_extensions = \STALKER_CMS\Vendor\Helpers\settings(['core_galleries', 'galleries', 'allowed_file_extensions']);
        $allowed_extensions = !empty($allowed_extensions) ? explode(',', $allowed_extensions) : [];
        $mimes = config('core_system::mimes');
        foreach ($allowed_extensions as $extension):
            if (isset($mimes[$extension])):
                if (is_array($mimes[$extension])):
                    foreach ($mimes[$extension] as $mime):
                        $allowed_types[$mime]['mime'] = $mime;
                        $allowed_types[$mime]['extensions'] = $extension;
                        $allowed_types[$mime]['max_size'] = $maxFileSize;
                    endforeach;
                else:
                    $allowed_types[$mimes[$extension]]['mime'] = $mimes[$extension];
                    $allowed_types[$mimes[$extension]]['extensions'] = $extension;
                    $allowed_types[$mimes[$extension]]['max_size'] = $maxFileSize;
                endif;
            endif;
        endforeach;
        sort($allowed_types);
        return $allowed_types;
    }
}