<?php

namespace STALKER_CMS\Core\Galleries\Http\Controllers;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Helpers;
use STALKER_CMS\Core\Galleries\Models\Gallery;
use Illuminate\Database\Eloquent\Collection;

/**
 * Контроллер для получения галереи в гостевом интерфейсе
 * Class PublicGalleriesController
 * @package STALKER_CMS\Core\Galleries\Http\Controllers
 */
class PublicGalleriesController extends ModuleController {

    /**
     * @var Gallery
     */
    protected $model;

    /**
     * PublicGalleriesController constructor.
     */
    public function __construct() {

        $this->model = new Gallery();
    }

    /**
     * Возвращает список изображений в галереи по символьному коду
     * @param $symbolicCode
     * @return Collection
     */
    public function images($symbolicCode) {

        if ($gallery = $this->model->whereSlug($symbolicCode)->first()):
            return $gallery->photos()->orderBy('order')->get();
        else:
            return new Collection();
        endif;
    }
}