<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalleriesTables extends Migration {

    public function up() {

        Schema::create('galleries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id', FALSE, TRUE)->default(0)->nullable()->index();
            $table->smallInteger('template_id', FALSE, TRUE)->nullable()->index();
            $table->integer('unit_id', FALSE, TRUE)->default(0)->nullable()->index();
            $table->string('slug', 50)->nullable()->unique();
            $table->string('title', 250)->nullable();
            $table->text('description')->nullable();
            $table->boolean('crop_use', FALSE, TRUE)->default(1)->nullable();
            $table->boolean('resizable', FALSE, TRUE)->default(1)->nullable();
            $table->string('aspect_ratio', 10)->nullable();
            $table->integer('crop_wight', FALSE, TRUE)->nullable();
            $table->integer('crop_height', FALSE, TRUE)->nullable();
            $table->integer('crop_wight_thumbnail', FALSE, TRUE)->default(200)->nullable();
            $table->integer('crop_height_thumbnail', FALSE, TRUE)->default(200)->nullable();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('galleries');
    }
}

