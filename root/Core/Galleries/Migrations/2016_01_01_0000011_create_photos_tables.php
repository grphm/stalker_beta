<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePhotosTables extends Migration {

    public function up() {

        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gallery_id', FALSE, TRUE)->default(0)->nullable()->index();
            $table->string('title', 250)->nullable();
            $table->string('alt', 250)->nullable();
            $table->string('path', 200)->nullable();
            $table->string('thumbnail_path', 200)->nullable();
            $table->string('original_name', 100)->nullable();
            $table->double('file_size', FALSE, TRUE)->default(0)->nullable();
            $table->string('mime_type', 50)->nullable();
            $table->integer('order', FALSE, TRUE)->default(0)->index();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('photos');
    }
}

