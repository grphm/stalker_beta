<?php
namespace STALKER_CMS\Core\Galleries\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class Photo extends BaseModel implements ModelInterface {

    use ModelTrait;

    protected $table = 'photos';
    protected $fillable = ['gallery_id', 'title', 'alt', 'path', 'thumbnail_path', 'original_name', 'file_size', 'mime_type', 'order'];
    protected $guarded = [];

    public function insert($request) {

        $this->gallery_id = $request::input('gallery_id');
        $this->title = $request::input('title');
        $this->alt = $request::input('alt');
        $this->path = $request::input('path');
        $this->thumbnail_path = $request::input('thumbnail_path');
        $this->original_name = $request::input('original_name');
        $this->file_size = $request::input('file_size');
        $this->mime_type = $request::input('mime_type');
        $this->order = $request::input('order');
        $this->save();
        return $this;
    }

    public function replace($id, $request) {
        // TODO: Implement replace() method.
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        if ($instance->ExistOnDisk):
            \Storage::delete($instance->path);
        endif;
        if ($instance->ExistThumbnailOnDisk):
            \Storage::delete($instance->thumbnail_path);
        endif;
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function getUploadSizeAttribute() {

        if ($this->attributes['file_size']):
            if ($this->attributes['file_size'] > 1048576):
                return round($this->attributes['file_size'] / 1048576, 2) . ' ' . \Lang::get('core_galleries_lang::photos.sizes.mb');
            elseif ($this->attributes['file_size'] > 1024):
                return round($this->attributes['file_size'] / 1024, 2) . ' ' . \Lang::get('core_galleries_lang::photos.sizes.kb');
            else:
                return $this->attributes['file_size'] . ' ' . \Lang::get('core_galleries_lang::photos.sizes.b');
            endif;
        else:
            return '<span class="c-red">' . \Lang::get('core_galleries_lang::photos.sizes.undefined') . '</span>';
        endif;
    }

    public function getUploadMimeTypeAttribute() {

        if ($this->attributes['mime_type']):
            return $this->attributes['mime_type'];
        else:
            return '<span class="c-red">' . \Lang::get('core_galleries_lang::photos.mime_undefined') . '</span>';
        endif;
    }

    public function getExistOnDiskAttribute() {

        if (!isset($this->ExistFile)):
            if (\Storage::exists($this->attributes['path'])):
                $this->ExistFile = TRUE;
            else:
                $this->ExistFile = FALSE;
            endif;
            return $this->ExistFile;
        else:
            return $this->ExistFile;
        endif;
    }

    public function getExistThumbnailOnDiskAttribute() {

        if (!isset($this->ExistThumbnailFile)):
            if (\Storage::exists($this->attributes['thumbnail_path'])):
                $this->ExistThumbnailFile = TRUE;
            else:
                $this->ExistThumbnailFile = FALSE;
            endif;
            return $this->ExistThumbnailFile;
        else:
            return $this->ExistThumbnailFile;
        endif;
    }

    public static function nextOrder($gallery_id) {

        return self::whereGalleryId($gallery_id)->orderBy('order', 'DESC')->value('order') + 1;
    }

    public function getAssetPathAttribute() {

        return asset('uploads' . $this->attributes['path']);
    }

    public function getAssetThumbnailPathAttribute() {

        return asset('uploads' . $this->attributes['thumbnail_path']);
    }

    public function gallery() {

        return $this->belongsTo('STALKER_CMS\Core\Galleries\Models\Gallery', 'gallery_id', 'id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['name' => 'required'];
    }

    public static function getBase64Rules() {

        return ['image' => 'required', 'thumbnail' => 'required', 'image_original_name' => 'required'];
    }

    public static function getUpdateRules() {

        return [];
    }
}