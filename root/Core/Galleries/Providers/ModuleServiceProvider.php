<?php

namespace STALKER_CMS\Core\Galleries\Providers;

use Illuminate\Support\Str;
use STALKER_CMS\Core\Galleries\Http\Controllers\GalleryValidatorController;
use STALKER_CMS\Core\Galleries\Http\Controllers\PublicGalleriesController;
use STALKER_CMS\Vendor\Providers\ServiceProvider;

/**
 * Class ModuleServiceProvider
 * @package STALKER_CMS\Core\Galleries\Providers
 */
class ModuleServiceProvider extends ServiceProvider {

    /**
     * Метод загрузки
     */
    public function boot() {

        $this->setPath(__DIR__ . '/../');
        $this->registerViews('core_galleries_views');
        $this->registerLocalization('core_galleries_lang');
        $this->registerConfig('core_galleries::config', 'Config/galleries.php');
        $this->registerSettings('core_galleries::settings', 'Config/settings.php');
        $this->registerActions('core_galleries::actions', 'Config/actions.php');
        $this->registerSystemMenu('core_galleries::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
        $this->publishesTemplates();
        $this->publishesAssets();
    }

    /**
     * Метод регистрации
     */
    public function register() {

        \App::bind('GalleryValidatorController', function () {
            return new GalleryValidatorController();
        });

        \App::bind('PublicGalleriesController', function () {
            return new PublicGalleriesController();
        });
    }

    /********************************************************************************************************************/
    /**
     * Регистрация blade директив
     */
    public function registerBladeDirectives() {

        \Blade::directive('galleryCropImage', function ($expression) {
            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (!empty($expression)):
                return "<?php echo \$__env->make('core_galleries_views::photos.assets.cropping', ['image' => " . $expression . "])->render(); ?>";
            else:
                return "<?php echo \$__env->make('core_galleries_views::photos.assets.cropping')->render(); ?>";
            endif;
        });
        \Blade::directive('galleryPreviewImage', function ($expression) {
            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (!empty($expression)):
                return "<?php echo \$__env->make('core_galleries_views::photos.assets.preview', ['image' => " . $expression . "])->render(); ?>";
            else:
                return "<?php echo \$__env->make('core_galleries_views::photos.assets.preview')->render(); ?>";
            endif;
        });
        \Blade::directive('Gallery', function ($expression) {
            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (!empty($expression)):
                $expressions = [];
                foreach (explode(',', $expression, 2) as $parameter):
                    $expressions[] = trim($parameter);
                endforeach;
                switch (count($expressions)):
                    case 1:
                        if (view()->exists("site_views::gallery-single")):
                            return "<?php echo \$__env->make('site_views::gallery-single', ['gallery_slug' => $expressions[0]])->render(); ?>";
                        endif;
                        break;
                    case 2:
                        $expressions[1] = preg_replace("/[\']/i", '', $expressions[1]);
                        if (view()->exists("site_views::$expressions[1]")):
                            return "<?php echo \$__env->make('site_views::$expressions[1]', ['gallery_slug' => $expressions[0]])->render(); ?>";
                        endif;
                        break;
                endswitch;
            endif;
            return NULL;
        });

    }

    /**
     * Публикация шаблонов
     */
    public function publishesTemplates() {

        $this->publishes([
            __DIR__ . '/../Resources/Templates' => base_path('home/Resources')
        ]);
    }

    /**
     * Метод публикации ресурсов
     */
    public function publishesAssets() {

        $this->publishes([
            __DIR__ . '/../Resources/Assets' => public_path('packages/galleries'),
        ]);
    }
}