<?php

return [
    'pictures' => 'Pictures',
    'edit' => 'Edit',
    'embed' => 'Embed code',
    'delete' => [
        'question' => 'Delete gallery',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'missing' => 'Images are missing',
    'empty' => 'List is empty',
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding gallery',
        'form' => [
            'title' => 'Title',
            'title_help_description' => 'For example: Main Gallery',
            'template' => 'Gallery Template',
            'description' => 'Description',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: main_gallery',
            'crop_use' => 'Use crop',
            'aspect_ratio' => 'Aspect ratio',
            'aspect_ratio_help_description' => 'Leave blank if the image should have an arbitrary aspect ratio',
            'crop_wight' => 'Fixed width, in pixels',
            'crop_wight_help_description' => 'Leave blank if the width of the area to trim, should have an arbitrary value',
            'crop_height' => 'Fixed height in pixels',
            'crop_height_help_description' => 'Leave blank if the height of the area to trim, should have an arbitrary value',
            'info' => 'When specifying values &laquo; Aspect Ratio &raquo; and &laquo; Fixed width (height) &raquo; make the relevant calculations of height and width',
            'resizable' => 'Change the size of the cropping area',
            'resizable_help_description' => 'It allows to change the size of the selection to crop the image',
            'crop_wight_thumbnail' => 'Fixed width thumbnail in pixels',
            'crop_height_thumbnail' => 'Fixed height of the thumbnail in pixels',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit gallery',
        'form' => [
            'title' => 'Title',
            'title_help_description' => 'For example: Main Gallery',
            'template' => 'Gallery Template',
            'description' => 'Description',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: main_gallery',
            'crop_use' => 'Use crop',
            'aspect_ratio' => 'Aspect ratio',
            'aspect_ratio_help_description' => 'Leave blank if the image should have an arbitrary aspect ratio',
            'crop_wight' => 'Fixed width, in pixels',
            'crop_wight_help_description' => 'Leave blank if the width of the area to trim, should have an arbitrary value',
            'crop_height' => 'Fixed height in pixels',
            'crop_height_help_description' => 'Leave blank if the height of the area to trim, should have an arbitrary value',
            'info' => 'When specifying values &laquo; Aspect Ratio &raquo; and &laquo; Fixed width (height) &raquo; make the relevant calculations of height and width',
            'resizable' => 'Change the size of the cropping area',
            'resizable_help_description' => 'It allows to change the size of the selection to crop the image',
            'crop_wight_thumbnail' => 'Fixed width thumbnail in pixels',
            'crop_height_thumbnail' => 'Fixed height of the thumbnail in pixels',
            'submit' => 'Save'
        ]
    ]
];