<?php

return [
    'breadcrumb' => 'Pictures',
    'title' => 'Pictures',
    'select_all' => 'Select all',
    'delete_selected' => [
        'question' => 'Remove selected images?',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete selected',
    ],
    'delete_all' => [
        'question' => 'Delete all images?',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete all',
    ],
    'edit' => 'Edit',
    'select' => 'Select',
    'cancel_selection' => 'Cancel selection',
    'delete' => [
        'question' => 'Delete images',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'empty' => 'List is empty',
    'crop' => [
        'title' => 'Title',
        'alt_text' => 'Alt text',
        'select' => 'Select',
        'cancel' => 'Cancel'
    ],
    'form' => [
        'size' => 'Size',
        'type' => 'Type',
        'upload' => 'Upload',
        'uploading' => 'Uploading file',
        'done' => 'done',
    ],
    'modal_edit' => [
        'submit' => 'Save',
        'cancel' => 'Cancel'
    ],
    'sizes' => ['mb' => 'Mb', 'kb' => 'kb', 'b' => 'b', 'undefined' => 'Size is not defined'],
    'mime_undefined' => 'Mime-type is not defined'
];