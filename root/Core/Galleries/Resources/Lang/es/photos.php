<?php

return [
    'breadcrumb' => 'Fotos',
    'title' => 'Fotos',
    'select_all' => 'Seleccionar todo',
    'delete_selected' => [
        'question' => 'Eliminar itages selekted?',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar selección',
    ],
    'delete_all' => [
        'question' => 'Eliminar todas las imágenes?',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar todos',
    ],
    'edit' => 'Editar',
    'select' => 'Seleccionar',
    'cancel_selection' => 'Cancelar la selección',
    'delete' => [
        'question' => 'Eliminar imagen',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'empty' => 'Lista está vacía',
    'crop' => [
        'title' => 'Título',
        'alt_text' => 'Alt texto',
        'select' => 'Seleccionar',
        'cancel' => 'Cancelar'
    ],
    'form' => [
        'size' => 'El Tamaño',
        'type' => 'Tipo',
        'upload' => 'Subir',
        'uploading' => 'Cargar archivos',
        'done' => 'hecho'
    ],
    'modal_edit' => [
        'submit' => 'Guardar',
        'cancel' => 'Cancelar'
    ],
    'sizes' => ['mb' => 'Mb', 'kb' => 'kb', 'b' => 'b', 'undefined' => 'El tamaño no se define'],
    'mime_undefined' => 'Mime-tipo no está definido'
];