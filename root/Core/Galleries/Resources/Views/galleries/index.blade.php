@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="active">
            <i class="{{ config('core_galleries::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_galleries::menu.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_galleries::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_galleries::menu.title')) !!}
        </h2>
    </div>
    @if(\PermissionsController::allowPermission('core_galleries', 'create', FALSE))
        @BtnAdd('core.galleries.create')
    @endif
    <div class="card">
        <div class="card-body card-padding m-h-250">
            @forelse($galleries as $gallery)
                <div class="js-item-container p-timeline">
                    <div class="pt-line c-gray text-right">
                        <span class="d-block">{{ $gallery->updated_at->format("d.m.y") }}</span>
                    </div>
                    <div class="pt-body">
                        <h2 class="ptb-title m-l-10">
                            {{ $gallery->title }}
                            <ul class="actions dropdown pull-right p-t-0">
                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{{ route('core.galleries.photos_index', $gallery->id) }}">
                                            @lang('core_galleries_lang::galleries.pictures')
                                        </a>
                                    </li>
                                    @if(\PermissionsController::allowPermission('core_galleries', 'edit', FALSE))
                                        <li>
                                            <a href="{{ route('core.galleries.edit', $gallery->id) }}">@lang('core_galleries_lang::galleries.edit')</a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="javascript:void(0);"
                                           class="js-copy-link"
                                           data-clipboard-text="{{ '@'.'Gallery(\'' . $gallery->slug . '\''.$gallery->TemplateSlug.')' }}">
                                            @lang('core_galleries_lang::galleries.embed')
                                        </a>
                                    </li>
                                    @if(\PermissionsController::allowPermission('core_galleries', 'delete', FALSE))
                                        <li class="divider"></li>
                                        <li>
                                            {!! Form::open(['route' => ['core.galleries.destroy', $gallery->id], 'method' => 'DELETE']) !!}
                                            <button type="submit"
                                                    class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                    autocomplete="off"
                                                    data-question="@lang('core_galleries_lang::galleries.delete.question') &laquo;{{ $gallery->title }}&raquo;?"
                                                    data-confirmbuttontext="@lang('core_galleries_lang::galleries.delete.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('core_galleries_lang::galleries.delete.cancelbuttontext')">
                                                @lang('core_galleries_lang::galleries.delete.submit')
                                            </button>
                                            {!! Form::close() !!}
                                        </li>
                                    @endif
                                </ul>
                            </ul>
                        </h2>
                        @if(count($gallery->photos))
                            <div class="lightbox clearfix">
                                @foreach($gallery->photos as $photo)
                                    <div data-src="{{ $photo->asset_path }}">
                                        <div class="lightbox-item">
                                            <img src="{{ $photo->asset_thumbnail_path }}" title="{{ $photo->title }}"
                                                 alt="{{ $photo->alt }}"/>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <h2 class="f-12 m-l-10 c-gray">@lang('core_galleries_lang::galleries.missing')</h2>
                        @endif
                    </div>
                </div>
            @empty
                <h2 class="f-16 c-gray">@lang('core_galleries_lang::galleries.empty')</h2>
            @endforelse
        </div>
    </div>
@stop