<div class="cmsCropOverlay js-cmsCropOverlay">
    <div class="popup">
        <div class="popup__pi">
            <div class="pi__image js-crop-container"></div>
            <div class="pi__btns">
                <div class="no-padding no-margin pull-left">
                    <span class="js-cron-image-wight"></span>x<span class="js-cron-image-height"></span>
                    <input id="js-cron-image-title" placeholder="@lang('core_galleries_lang::photos.crop.title')" class="p-5 l-h-n wp-40">
                    <input id="js-cron-image-alt" placeholder="@lang('core_galleries_lang::photos.crop.alt_text')" class="m-l-5 p-5 l-h-n wp-40">
                </div>
                <div class="no-padding no-margin pull-right">
                    <a href="javascript:void(0);" class="js-crop-save cbtn-save"><span>@lang('core_galleries_lang::photos.crop.select')</span></a>
                    <a href="javascript:void(0);" class="js-crop-cancel cbtn-cancel"><span>@lang('core_galleries_lang::photos.crop.cancel')</span></a>
                </div>
            </div>
        </div>
    </div>
</div>