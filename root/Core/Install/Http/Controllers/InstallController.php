<?php
namespace STALKER_CMS\Core\Install\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Vendor\Helpers;
use STALKER_CMS\Vendor\Models\Packages;

/**
 * Контроллера установки CMS
 * Class InstallController
 * @package STALKER_CMS\Core\Install\Http\Controllers
 */
class InstallController extends ModuleController {

    /**
     * @var Packages
     */
    protected $model;

    /**
     * InstallController constructor.
     * @param Packages $packages
     */
    public function __construct(Packages $packages) {

        $this->model = $packages;
    }

    /**
     * Отображает начальную страницу установки.
     * Прерывает установку если существует файл конфигурации
     */
    public function index() {

        if (file_exists(base_path('.env'))):
            return view('core_install_views::break');
        else:
            return view('core_install_views::index');
        endif;
    }

    /**
     * Отображает список доступных пакетов CMS
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function packages() {

        if (!file_exists(base_path('.env'))):
            return redirect()->route('install.index');
        else:
            $packages = new Collection();
            foreach ($this->model->whereEnabled(FALSE)->whereRequired(FALSE)->get() as $package):
                $packages[$package->name] = $package;
            endforeach;
            return view('core_install_views::packages', compact('packages'));
        endif;
    }

    /**
     * Завершение установки
     * @return \Illuminate\Http\RedirectResponse
     */
    public function success() {

        if (!file_exists(base_path('.env'))):
            return redirect()->route('install.index');
        else:
            $this->model->insert([
                'slug' => 'core_install', 'title' => json_encode(['ru' => 'Модуль установки системы', 'en' => 'Installation module']), 'description' => '',
                'composer_config' => NULL, 'install_path' => 'Core/Install', 'enabled' => FALSE, 'required' => TRUE, 'relations' => NULL, 'order' => 9999
            ]);
            foreach (Packages::whereEnabled(TRUE)->get() as $package):
                \PackagesController::publishesPackageAssets($package);
            endforeach;
            return view('core_install_views::success');
        endif;
    }

    /**
     * Создает файл конфигурации
     * Формирует список доступных пакетов для становки
     */
    public function configDB() {

        $request = \RequestController::isAJAX()->init();
        if (file_exists(base_path('.env.example'))):
            if (\ValidatorController::passes($request, ['DB_DATABASE' => 'required', 'DB_USERNAME' => 'required', 'APP_LOCALE' => 'required'])):
                try {
                    \App::setLocale($request::input('APP_LOCALE'));
                    if (Helpers\connectMySQL($request::input('DB_USERNAME'), $request::input('DB_PASSWORD'), $request::input('DB_DATABASE'))):
                        copy(base_path('.env.example'), base_path('.env'));
                        Helpers\update_config_file('DB_DATABASE=__DBNAME__', 'DB_DATABASE=' . $request::input('DB_DATABASE'));
                        Helpers\update_config_file('DB_USERNAME=__UDBSER__', 'DB_USERNAME=' . $request::input('DB_USERNAME'));
                        Helpers\update_config_file('DB_PASSWORD=__DBPASS__', 'DB_PASSWORD=' . $request::input('DB_PASSWORD'));
                        Helpers\update_config_file('APP_KEY=', 'APP_KEY=' . str_random(32));
                        Helpers\update_config_file('APP_LOCALE=', 'APP_LOCALE=' . $request::input('APP_LOCALE'));
                        config(['database.connections.mysql.database' => env('DB_DATABASE', $request::input('DB_DATABASE'))]);
                        config(['database.connections.mysql.username' => env('DB_USERNAME', $request::input('DB_USERNAME'))]);
                        config(['database.connections.mysql.password' => env('DB_PASSWORD', $request::input('DB_PASSWORD'))]);
                        \Artisan::call('migrate', ['--force' => TRUE, '--no-ansi' => TRUE, '--quiet' => TRUE, '--no-interaction' => TRUE, '--path' => "root/Core/Install/Migrations"]);
                        \Artisan::call('db:seed', ['--force' => TRUE, '--no-ansi' => TRUE, '--quiet' => TRUE, '--no-interaction' => TRUE, '--class' => 'STALKER_CMS\Core\Install\Seeds\PackagesTableSeeder']);
                        \Artisan::call('db:seed', ['--force' => TRUE, '--no-ansi' => TRUE, '--quiet' => TRUE, '--no-interaction' => TRUE, '--class' => 'STALKER_CMS\Core\Install\Seeds\LanguagesTableSeeder']);
                        return \ResponseController::success(1001)->redirect(route('install.packages'))->json();
                    else:
                        return \ResponseController::error(2001)->json();
                    endif;
                } catch (\Exception $e) {
                    return \ResponseController::error(2002)->json();
                }
            else:
                return \ResponseController::error(2100)->json();
            endif;
        else:
            return \ResponseController::error(2003)->json();
        endif;
    }

    /**
     * Установка модулей ядра
     * Установка модулей выбранных пользователем
     * @return \Illuminate\Http\JsonResponse
     */
    public function configPackages() {

        $request = \RequestController::isAJAX()->init();
        if (file_exists(base_path('.env'))):
            try {
                foreach ($package = $this->model->whereEnabled(FALSE)->whereRequired(TRUE)->get() as $package):
                    \PackagesController::installPackage($package);
                endforeach;
                if ($request::has('MODULE')):
                    foreach ($request::input('MODULE') as $package_slug => $enabled):
                        if ($package = $this->model->whereSlug($package_slug)->whereEnabled(FALSE)->first()):
                            \PackagesController::installPackage($package);
                        endif;
                    endforeach;
                endif;
                return \ResponseController::success(1002)->redirect(route('install.success'))->json();
            } catch (Exception $e) {
                return \ResponseController::error(2004)->json();
            }
        else:
            return \ResponseController::error(2005)->json();
        endif;
    }
}