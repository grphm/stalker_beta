<?php
\Route::group(['middleware' => 'secure', 'prefix' => 'install'], function () {
    \Route::get('/', ['as' => 'install.index', 'uses' => 'InstallController@index']);
    \Route::get('packages', ['as' => 'install.packages', 'uses' => 'InstallController@packages']);
    \Route::get('success', ['as' => 'install.success', 'uses' => 'InstallController@success']);
    \Route::post('config/db', ['as' => 'install.config.db', 'uses' => 'InstallController@configDB']);
    \Route::post('config/packages', ['as' => 'install.config.packages', 'uses' => 'InstallController@configPackages']);
});