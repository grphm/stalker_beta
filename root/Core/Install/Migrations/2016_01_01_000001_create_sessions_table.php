<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionsTable extends Migration {

    public function up() {
        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id')->unique()->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable()->index();
            $table->string('ip_address', 45)->nullable();
            $table->text('user_agent')->nullable();
            $table->longText('payload')->nullable();
            $table->integer('last_activity', FALSE, TRUE)->nullable()->index();
        });
    }

    public function down() {
        Schema::drop('sessions');
    }
}
