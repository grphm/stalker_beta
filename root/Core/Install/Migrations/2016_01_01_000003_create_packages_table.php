<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration {

    public function up() {

        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 100)->nullable()->index();
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->text('composer_config', 200)->nullable();
            $table->string('install_path', 200)->nullable();
            $table->boolean('enabled', FALSE, TRUE)->default(0)->nullable();
            $table->boolean('required', FALSE, TRUE)->default(0)->nullable();
            $table->string('relations', 150)->nullable();
            $table->integer('order', FALSE, TRUE)->default(0)->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('packages');
    }
}
