<?php

return [
    'page_title' => 'Can not be established',
    'page_description' => '',
    'title' => '<i class="zmdi zmdi-alert-triangle"></i>',
    'message' => 'Detected configuration file<br>Remove it and start again',
];
