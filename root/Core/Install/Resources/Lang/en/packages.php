<?php

return [
    'page_title' => 'Installing Modules',
    'page_description' => '',
    'form' => [
        'title' => 'Step 2: Install the modules',
        'check_all' => 'Full installation'
    ]
];