<?php

return [
    'page_title' => 'No puede ser establecida',
    'page_description' => '',
    'title' => '<i class="zmdi zmdi-alert-triangle"></i>',
    'message' => 'Archivo de configuración Detected <br> quitarlo y volver a empezar',
];
