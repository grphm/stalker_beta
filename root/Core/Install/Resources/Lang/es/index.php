<?php

return [
    'page_title' => 'Configuración de la conexión a la base de datos',
    'page_description' => '',
    'form' => [
        'title' => 'Paso 1. Configuración de una conexión con la base de datos',
        'APP_LOCALE' => 'Idioma de interfaz',
        'DB_DATABASE' => 'Base de datos',
        'DB_USERNAME' => 'Nombre de usuario',
        'DB_PASSWORD' => 'Contraseña',
    ]
];
