<?php

return [
    'page_title' => 'Instalación de módulos',
    'page_description' => '',
    'form' => [
        'title' => 'Paso 2: Instale los módulos',
        'check_all' => 'Instalación completa'
    ]
];