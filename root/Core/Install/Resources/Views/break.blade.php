@extends('root_views::layouts.errors')
@section('title', trans('core_install_lang::break.page_title'))
@section('description', trans('core_install_lang::break.page_description'))
@section('body-class'){{ 'four-zero-content' }}@stop
@section('content')
    <div class="four-zero">
        <h2>@lang('core_install_lang::break.title')</h2>
        <small class="f-20">@lang('core_install_lang::break.message')</small>
        <footer>
            <a href="{!! url('/') !!}"><i class="zmdi zmdi-home"></i></a>
        </footer>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
@stop