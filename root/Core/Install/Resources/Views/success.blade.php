@extends('root_views::layouts.install')
@section('title', trans('core_install_lang::success.page_title'))
@section('description', trans('core_install_lang::success.page_description'))
@section('body-class'){{ 'four-zero-content' }}@stop
@section('content')
    <div class="four-zero">
        <h2>@lang('core_install_lang::success.title')</h2>
        <small>@lang('core_install_lang::success.description')</small>
        <footer>
            <a href="{{ Route::getRoutes()->hasNamedRoute('auth.login.index') ? URL::route('auth.login.index') : '/login' }}"><i class="zmdi zmdi-key"></i></a>
            <a href="{{ url('/') }}"><i class="zmdi zmdi-home"></i></a>
        </footer>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
@stop