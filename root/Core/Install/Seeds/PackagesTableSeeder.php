<?php
namespace STALKER_CMS\Core\Install\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Facade;
use PhpSpec\Exception\Exception;

class PackagesTableSeeder extends Seeder {

    public function run() {

        try {
            \DB::table('packages')->insert([
                'slug' => 'core_auth',
                'title' => json_encode([
                    'ru' => 'Авторизация и регистрация',
                    'en' => 'Authorization and registration',
                    'es' => 'Autorización y registro',
                ]),
                'description' => json_encode([
                    'ru' => 'Позволяет авторизовывать и регистрировать пользователей',
                    'en' => 'It allows you to authenticate and register users',
                    'es' => 'Te permite autenticar y registrar los usuarios',
                ]),
                'install_path' => NULL,
                'enabled' => FALSE,
                'required' => TRUE,
                'relations' => NULL,
                'order' => 9000
            ]);
            \DB::table('packages')->insert([
                'slug' => 'core_system',
                'title' => json_encode([
                    'ru' => 'Системный модуль',
                    'en' => 'System Module',
                    'es' => 'Módulo del Sistema',
                ]),
                'description' => json_encode([
                    'ru' => 'Основной модуль CMS',
                    'en' => 'The main CMS module',
                    'es' => 'El módulo principal CMS',
                ]),
                'install_path' => NULL,
                'enabled' => FALSE,
                'required' => TRUE,
                'relations' => NULL,
                'order' => 9999
            ]);
            \DB::table('packages')->insert([
                'slug' => 'core_mailer',
                'title' => json_encode([
                    'ru' => 'Почтовый модуль',
                    'en' => 'Mailing module',
                    'es' => 'Módulo de correo',
                ]),
                'description' => json_encode([
                    'ru' => 'Позволяет управлять рассылкой писем',
                    'en' => 'It allows you to manage the distribution of letters',
                    'es' => 'Se le permite administrar la distribución de cartas',
                ]),
                'install_path' => NULL,
                'enabled' => FALSE,
                'required' => FALSE,
                'relations' => NULL,
                'order' => 9001
            ]);
            \DB::table('packages')->insert([
                'slug' => 'core_content',
                'title' => json_encode([
                    'ru' => 'Модуль контента',
                    'en' => 'Content module',
                    'es' => 'Módulo de contenido',
                ]),
                'description' => json_encode([
                    'ru' => 'Позволяет управлять web-контентом',
                    'en' => 'It allows you to manage web-content',
                    'es' => 'Se le permite gestionar contenidos web',
                ]),
                'install_path' => NULL,
                'enabled' => FALSE,
                'required' => FALSE,
                'relations' => NULL,
                'order' => 1
            ]);
            \DB::table('packages')->insert([
                'slug' => 'core_dictionaries',
                'title' => json_encode([
                    'ru' => 'Модуль словарей',
                    'en' => 'Module dictionaries',
                    'es' => 'Diccionarios Módulo',
                ]),
                'description' => json_encode([
                    'ru' => 'Позволяет управлять информационными списками',
                    'en' => 'It allows you to manage lists of information',
                    'es' => 'Te permite administrar las listas de la información',
                ]),
                'install_path' => NULL,
                'enabled' => FALSE,
                'required' => FALSE,
                'relations' => NULL,
                'order' => 2
            ]);
            \DB::table('packages')->insert([
                'slug' => 'core_seo',
                'title' => json_encode([
                    'ru' => 'Модуль SEO',
                    'en' => 'SEO module',
                    'es' => 'Módulo de SEO',
                ]),
                'description' => json_encode([
                    'ru' => 'Позволяет управлять поисковой оптимизацией',
                    'en' => 'It allows you to manage the search engine optimization',
                    'es' => 'Permite gestionar la optimización de motores de búsqueda',
                ]),
                'install_path' => NULL,
                'enabled' => FALSE,
                'required' => FALSE,
                'relations' => NULL,
                'order' => 3
            ]);
            \DB::table('packages')->insert([
                'slug' => 'core_open_graph',
                'title' => json_encode([
                    'ru' => 'Модуль Open Graph',
                    'en' => 'Open Graph module',
                    'es' => 'Módulo de Open Graph',
                ]),
                'description' => json_encode([
                    'ru' => 'Позволяет связывать контент с социальными сетями',
                    'en' => 'It allows you to associate content with social networks',
                    'es' => 'Se permite asociar contenidos con las redes sociales',
                ]),
                'install_path' => NULL,
                'enabled' => FALSE,
                'required' => FALSE,
                'relations' => NULL,
                'order' => 4
            ]);
            \DB::table('packages')->insert([
                'slug' => 'core_uploads',
                'title' => json_encode([
                    'ru' => 'Модуль загрузок',
                    'en' => 'Module uploads',
                    'es' => 'Subidos Módulo',
                ]),
                'description' => json_encode([
                    'ru' => 'Позволяет загружать файлы на сервер',
                    'en' => 'It allows you to upload files to the server',
                    'es' => 'Se le permite subir archivos al servidor',
                ]),
                'install_path' => NULL,
                'enabled' => FALSE,
                'required' => FALSE,
                'relations' => NULL,
                'order' => 5
            ]);
            \DB::table('packages')->insert([
                'slug' => 'core_galleries',
                'title' => json_encode([
                    'ru' => 'Модуль галерей',
                    'en' => 'Module galleries',
                    'es' => 'Galerías del módulo',
                ]),
                'description' => json_encode([
                    'ru' => 'Позволяет управлять галереями и фотографиями',
                    'en' => 'It allows you to manage the galleries and photos',
                    'es' => 'Permite gestionar las galerías y fotos',
                ]),
                'install_path' => NULL,
                'enabled' => FALSE,
                'required' => FALSE,
                'relations' => NULL,
                'order' => 6
            ]);

            \DB::table('packages')->insert([
                'slug' => 'application',
                'title' => json_encode([
                    'ru' => 'Модуль расширения функционала',
                    'en' => 'Expansion Module functionality',
                    'es' => 'Funcionalidad del módulo de expansión',
                ]),
                'description' => json_encode([
                    'ru' => 'Позволяет расширить функционал CMS',
                    'en' => 'It allows you to extend the functionality CMS',
                    'es' => 'Se le permite ampliar las funcionalidades CMS',
                ]),
                'install_path' => NULL,
                'enabled' => FALSE,
                'required' => FALSE,
                'relations' => NULL,
                'order' => 7
            ]);
        } catch (Exception $e) {

        }

    }
}