<?php

return [
    'mailer' => [
        'title' => ['ru' => 'Просмотр', 'en' => 'View', 'es' => 'Ver'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-eye'
    ],
    'templates' => [
        'title' => ['ru' => 'Работа с шаблонами', 'en' => 'Working with templates', 'es' => 'Trabajar con plantillas'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-layers'
    ]
];