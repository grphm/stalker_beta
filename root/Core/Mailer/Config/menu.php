<?php

return [
    'package' => 'core_mailer',
    'title' => ['ru' => 'Почта', 'en' => 'Mail', 'es' => 'Correo'],
    'route' => '__#',
    'icon' => 'zmdi zmdi-mail-send',
    'menu_child' => [
        'mailer' => [
            'title' => ['ru' => 'Входящие', 'en' => 'Inbox', 'es' => 'Bandeja de entrada'],
            'route' => 'core.mailer.index',
            'icon' => 'zmdi zmdi-email'
        ],
        'templates' => [
            'title' => ['ru' => 'Шаблоны', 'en' => 'Templates', 'es' => 'Plantillas'],
            'route' => 'core.mailer.templates.index',
            'icon' => 'zmdi zmdi-layers'
        ]
    ]
];