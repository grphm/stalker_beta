<?php
namespace STALKER_CMS\Core\Mailer\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Фасад контроллера Почтовый модуль
 * Class PublicMailer
 * @package STALKER_CMS\Core\Mailer\Facades
 */
class PublicMailer extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicMailerController';
    }
}