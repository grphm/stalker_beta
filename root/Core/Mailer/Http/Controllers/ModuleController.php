<?php
namespace STALKER_CMS\Core\Mailer\Http\Controllers;

use STALKER_CMS\Vendor\Http\Controllers\Controller;

/**
 * Основной контроллер пакета
 * Class ModuleController
 * @package STALKER_CMS\Core\Mailer\Http\Controllers
 */
abstract class ModuleController extends Controller {

}