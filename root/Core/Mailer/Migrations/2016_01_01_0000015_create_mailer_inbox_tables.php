<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMailerInboxTables extends Migration {

    public function up() {

        Schema::create('mailer_inbox', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content')->nullable();
            $table->boolean('views', FALSE, TRUE)->default(0)->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable()->index();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('mailer_inbox');
    }
}

