<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMailerTemplatesTables extends Migration {

    public function up() {

        Schema::create('mailer_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale', 10)->nullable()->index();
            $table->string('title', 100)->nullable();
            $table->string('path', 255)->nullable();
            $table->boolean('required', FALSE, TRUE)->default(0)->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable()->index();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('mailer_templates');
    }
}

