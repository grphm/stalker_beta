<?php

return [
    'edit' => 'Edit',
    'delete' => [
        'question' => 'Delete template',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'empty' => 'List is empty',
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding template',
        'form' => [
            'content' => 'Content template',
            'title' => 'Title',
            'title_help_description' => 'For example: Feedback',
            'path' => 'Filename template',
            'path_help_description' => 'Expanding .blade.php is not needed!<br> Example: feedback',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit template',
        'form' => [
            'content' => 'Content template',
            'title' => 'Title',
            'title_help_description' => 'For example: Feedback',
            'submit' => 'Save'
        ]
    ]
];