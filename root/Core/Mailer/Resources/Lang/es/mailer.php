<?php

return [
    'templates' => 'Plantillas de letras',
    'root_directory' => 'Directorio raíz: /home/Resources/Mails',
    'sort_date_created' => 'Fecha de recepción',
    'empty' => 'Lista está vacía',
];