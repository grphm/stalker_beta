@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('head')
@stop
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_mailer::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_mailer::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_mailer::menu.menu_child.mailer.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_mailer::menu.menu_child.mailer.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_mailer::menu.menu_child.mailer.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_mailer::menu.menu_child.mailer.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div class="lv-header-alt clearfix m-b-5">
            <ul class="lv-actions actions">
                <li class="dropdown">
                    <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                        <i class="zmdi zmdi-sort-amount-asc"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="{{ route('core.mailer.index', array_merge(Request::all(), ['sort_field' => 'created_at', 'sort_direction' => 'asc'])) }}">
                                @lang('core_mailer_lang::mailer.sort_date_created')
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                        <i class="zmdi zmdi-sort-amount-desc"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="{{ route('core.mailer.index', array_merge(Request::all(), ['sort_field' => 'created_at', 'sort_direction' => 'desc'])) }}">
                                @lang('core_mailer_lang::mailer.sort_date_created')
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="card-body card-padding m-h-250">
            @if($mails->count())
                <div class="row">
                    <div class="col-sm-12 m-b-25">
                        <div class="listview lv-bordered lv-lg">
                            @forelse($mails as $mail)
                                <div class="js-item-container lv-item media">
                                    <div class="profile-pic small-image pull-left">
                                        @ProfileAvatar($mail->name, $mail->avatar)
                                    </div>
                                    <div class="media-body">
                                        <div class="lv-title">{{ $mail->name }}</div>
                                        <small class="lv-small">{{ $mail->message }}</small>
                                        @if(!empty($mail->link))
                                            <small class="lv-small">{!! $mail->link !!}</small>
                                        @endif
                                        <ul class="lv-attrs">
                                            <ul class="lv-attrs">
                                                @if(!empty($mail->phone))
                                                    <li>{{ $mail->phone }}</li>
                                                @endif
                                                @if(!empty($mail->email))
                                                    <li>{{ $mail->email }}</li>
                                                @endif
                                                <li>{{ $mail->CreatedDate }}</li>
                                            </ul>
                                        </ul>
                                    </div>
                                </div>
                            @empty
                                <h2 class="f-16 c-gray">@lang('core_mailer_lang::mailer.empty')</h2>
                            @endforelse
                        </div>
                    </div>
                </div>
                {!! $mails->appends(['sort_field' => \Request::input('sort_field'), 'sort_direction' => \Request::input('sort_direction')])->render() !!}
            @else
                <h2 class="f-16 c-gray">@lang('core_mailer_lang::mailer.empty')</h2>
            @endif
        </div>
    </div>
@stop