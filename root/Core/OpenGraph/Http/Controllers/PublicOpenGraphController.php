<?php
namespace STALKER_CMS\Core\OpenGraph\Http\Controllers;

use League\Flysystem\Exception;
use \STALKER_CMS\Vendor\Helpers as Helpers;

/**
 * Контроллер для управления данными OpenGraph
 * Class PublicOpenGraphController
 * @package STALKER_CMS\Core\OpenGraph\Http\Controllers
 */
class PublicOpenGraphController extends ModuleController {

    /**
     * Сохранение данных
     * @param \Request $request
     * @return string
     * @throws \Exception
     */
    public function makeOpenGraphData(\Request $request) {

        $allowed_mime_types = array_merge([config('core_system::mimes.gif')], config('core_system::mimes.jpeg'), config('core_system::mimes.png'));
        $open_graph = ['og:title' => $request::input('og.og:title'), 'og:description' => $request::input('og.og:description'), 'og:image' => []];
        foreach ($request::input('og.og:image') as $image):
            if (is_object($image) && $image->isValid() && in_array($image->getClientMimeType(), $allowed_mime_types)):
                $upload_directory = Helpers\setDirectory('pages');
                $fileName = time() . "_" . $image->getClientOriginalName();
                $image->move(public_path('uploads/pages'), $fileName);
                $open_graph['og:image'][] = $fileName;
            endif;
        endforeach;
        return json_encode($open_graph);
    }

    /**
     * Пересохранение данных
     * @param $model
     * @param \Request $request
     * @return string
     * @throws \Exception
     */
    public function remakeOpenGraphData($model, \Request $request) {

        $allowed_mime_types = array_merge([config('core_system::mimes.gif')], config('core_system::mimes.jpeg'), config('core_system::mimes.png'));
        $open_graph = ['og:title' => $request::input('og.og:title'), 'og:description' => $request::input('og.og:description'), 'og:image' => []];
        $op_images = [];
        if ($page_open_graph = $model->open_graph):
            $op_images = json_decode($page_open_graph, TRUE)['og:image'];
            if ($request::has('og.og:image_save') && count($op_images)):
                foreach ($op_images as $index => $op_image):
                    if (in_array($index, $request::input('og.og:image_save'))):
                        $open_graph['og:image'][] = $op_image;
                    else:
                        if (\Storage::exists('pages/' . $op_image)):
                            \Storage::delete('pages/' . $op_image);
                        endif;
                    endif;
                endforeach;
            elseif (count($op_images)):
                foreach ($op_images as $index => $op_image):
                    if (\Storage::exists('pages/' . $op_image)):
                        \Storage::delete('pages/' . $op_image);
                    endif;
                endforeach;
            endif;
        endif;
        foreach ($request::input('og.og:image') as $image):
            if (is_object($image) && $image->isValid() && in_array($image->getClientMimeType(), $allowed_mime_types)):
                $upload_directory = Helpers\setDirectory('pages');
                $fileName = time() . "_" . $image->getClientOriginalName();
                $image->move(public_path('uploads/pages'), $fileName);
                $open_graph['og:image'][] = $fileName;
            endif;
        endforeach;
        return json_encode($open_graph);
    }

    /**
     * Удаление данных
     * @param $model
     */
    public function destroyOpenGraphData($model) {

        if (isset($model->page_open_graph) && !empty($model->page_open_graph)):
            foreach (json_decode($model->page_open_graph, TRUE)['og:image'] as $op_image):
                if (\Storage::exists('content/' . $op_image)):
                    \Storage::delete('content/' . $op_image);
                endif;
            endforeach;
        endif;
    }
}