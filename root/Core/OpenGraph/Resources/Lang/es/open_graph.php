<?php

return [
    'open_graph' => 'Etiquetas meta Open Graph',
    'paste' => 'Fylla',
    'open_graph_title' => 'Título',
    'open_graph_description' => 'Descripción',
    'open_graph_images' => 'Imágenes',
    'open_graph_images_help_description' => 'Formatos soportados: png, jpg, gif',
    'open_graph_select' => 'Seleccionar',
    'open_graph_change' => 'Cambio',
    'open_graph_delete' => 'Eliminar'
];