<div id="open-graph-container">
    <div class="c-gray m-b-20">
        <i class="{{ config('core_open_graph::config.package_icon') }}"></i>
        @lang('core_open_graph_lang::open_graph.open_graph')
        <div class="pull-right">
            <button type="button" class="btn btn-primary btn-xs btn-icon-text waves-effect auto-paste">
                <i class="fa fa-paste"></i> @lang('core_open_graph_lang::open_graph.paste')
            </button>
        </div>
    </div>

    <div class="form-group fg-float">
        <div class="fg-line">
            {!! Form::text('og[og:title]', isset($open_graph->og_title) ? $open_graph->og_title : NULL, ['class'=>'input-sm form-control fg-input']) !!}
        </div>
        <label class="fg-label">@lang('core_open_graph_lang::open_graph.open_graph_title')</label>
    </div>
    <div class="form-group fg-float">
        <div class="fg-line">
            {!! Form::textarea('og[og:description]', isset($open_graph->og_description) ? $open_graph->og_description : NULL, ['class' => 'form-control auto-size fg-input', 'data-autosize-on' => 'true', 'rows' => 1]) !!}
        </div>
        <label class="fg-label">@lang('core_open_graph_lang::open_graph.open_graph_description')</label>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <p class="c-gray m-b-5">@lang('core_open_graph_lang::open_graph.open_graph_images')</p>
            <small class="help-description">@lang('core_open_graph_lang::open_graph.open_graph_images_help_description')</small>
        </div>
        <div class="col-sm-4">
            <div class="fileinput fileinput-{{ (isset($open_graph->og_image_first) && !is_null($open_graph->og_image_first)) ? 'exists' : 'new' }}"
                 data-provides="fileinput">
                <div class="fileinput-preview thumbnail l-h-150" data-trigger="fileinput" style="line-height: 150px;">
                    @if(isset($open_graph->og_image_first) && !is_null($open_graph->og_image_first))
                        <div class="ci-avatar">
                            <img style="width: 100%" alt="" src="{{ $open_graph->og_image_first }}">
                        </div>
                        {!! Form::hidden('og[og:image_save][]', 0) !!}
                    @endif
                </div>
                <div>
                <span class="btn btn-info btn-file">
                    <span class="fileinput-new">@lang('core_open_graph_lang::open_graph.open_graph_select')</span>
                    <span class="fileinput-exists">@lang('core_open_graph_lang::open_graph.open_graph_change')</span>
                    {!! Form::file('og[og:image][]') !!}
                </span>
                    <a href="#" class="btn btn-danger fileinput-exists"
                       data-dismiss="fileinput">@lang('core_open_graph_lang::open_graph.open_graph_delete')</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="fileinput fileinput-{{ (isset($open_graph->og_image_second) && !is_null($open_graph->og_image_second)) ? 'exists' : 'new' }}"
                 data-provides="fileinput">
                <div class="fileinput-preview thumbnail l-h-150" data-trigger="fileinput" style="line-height: 150px;">
                    @if(isset($open_graph->og_image_second) && !is_null($open_graph->og_image_second))
                        <div class="ci-avatar">
                            <img style="width: 100%" alt="" src="{{ $open_graph->og_image_second }}">
                        </div>
                        {!! Form::hidden('og[og:image_save][]', 1) !!}
                    @endif
                </div>
                <div>
                <span class="btn btn-info btn-file">
                    <span class="fileinput-new">@lang('core_open_graph_lang::open_graph.open_graph_select')</span>
                    <span class="fileinput-exists">@lang('core_open_graph_lang::open_graph.open_graph_change')</span>
                    {!! Form::file('og[og:image][]') !!}
                </span>
                    <a href="#" class="btn btn-danger fileinput-exists"
                       data-dismiss="fileinput">@lang('core_open_graph_lang::open_graph.open_graph_delete')</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="fileinput fileinput-{{ (isset($open_graph->og_image_third) && !is_null($open_graph->og_image_third)) ? 'exists' : 'new' }}"
                 data-provides="fileinput">
                <div class="fileinput-preview thumbnail l-h-150" data-trigger="fileinput" style="line-height: 150px;">
                    @if(isset($open_graph->og_image_third) && !is_null($open_graph->og_image_third))
                        <div class="ci-avatar">
                            <img style="width: 100%" alt="" src="{{ $open_graph->og_image_third }}">
                        </div>
                        {!! Form::hidden('og[og:image_save][]', 2) !!}
                    @endif
                </div>
                <div>
                <span class="btn btn-info btn-file">
                    <span class="fileinput-new">@lang('core_open_graph_lang::open_graph.open_graph_select')</span>
                    <span class="fileinput-exists">@lang('core_open_graph_lang::open_graph.open_graph_change')</span>
                    {!! Form::file('og[og:image][]') !!}
                </span>
                    <a href="#" class="btn btn-danger fileinput-exists"
                       data-dismiss="fileinput">@lang('core_open_graph_lang::open_graph.open_graph_delete')</a>
                </div>
            </div>
        </div>
    </div>
</div>