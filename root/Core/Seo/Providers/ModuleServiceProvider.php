<?php

namespace STALKER_CMS\Core\Seo\Providers;

use Illuminate\Support\Str;
use STALKER_CMS\Vendor\Providers\ServiceProvider;

/**
 * Class ModuleServiceProvider
 * @package STALKER_CMS\Core\Seo\Providers
 */
class ModuleServiceProvider extends ServiceProvider {

    /**
     * Метод загрузки
     */
    public function boot() {

        $this->setPath(__DIR__ . '/../');
        $this->registerViews('core_seo_views');
        $this->registerLocalization('core_seo_lang');
        $this->registerConfig('core_seo::config', 'Config/seo.php');
        $this->registerSettings('core_seo::settings', 'Config/settings.php');
        $this->registerActions('core_seo::actions', 'Config/actions.php');
        $this->registerSystemMenu('core_seo::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
    }

    /**
     * Метод регистрации
     */
    public function register() {

    }

    /********************************************************************************************************************/
    /**
     * Регистрация blade директив
     */
    public function registerBladeDirectives() {

        \Blade::directive('metaTitle', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (empty($expression)):
                return "<?php \$__env->startSection('title', @\$page->page_title); ?>";
            else:
                return "<?php \$__env->startSection('title', $expression); ?>";
            endif;
        });

        \Blade::directive('metaDescription', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (empty($expression)):
                return "<?php \$__env->startSection('description', @\$page->page_description); ?>";
            else:
                return "<?php \$__env->startSection('description', $expression); ?>";
            endif;
        });

        \Blade::directive('PageTitle', function ($expression) {

            if (Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if (empty($expression)):
                return "<?php echo @\$page->page_h1; ?>";
            else:
                return "<?php echo $expression; ?>";
            endif;
        });
    }
}