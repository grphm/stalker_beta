<?php

return [
    'seo' => 'Search Engine Optimization',
    'paste' => 'Fylla',
    'seo_title' => 'Título',
    'seo_description' => 'Descripción',
    'seo_h1' => 'Título del primer nivel',
    'seo_url' => 'Identificador Universal de Recursos',
    'seo_url_help_description' => 'Dirección de página. Sólo latino personajes, guiones, guiones. Especificar nombre de dominio sin <br> Por ejemplo: product-catalog'
];