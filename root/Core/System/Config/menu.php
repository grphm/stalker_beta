<?php

return [
    'package' => 'core_system',
    'title' => ['ru' => 'Система', 'en' => 'System', 'es' => 'Sistema'],
    'route' => '__#',
    'icon' => 'zmdi zmdi-settings-square',
    'menu_child' => [
        'settings' => [
            'title' => ['ru' => 'Настройки модулей', 'en' => 'Settings modules', 'es' => 'Configuración del módulo'],
            'route' => 'core.system.settings.index',
            'icon' => 'zmdi zmdi-settings'
        ],
        'modules' => [
            'title' => ['ru' => 'Доступные модули', 'en' => 'Available modules', 'es' => 'Los módulos disponibles'],
            'route' => 'core.system.modules.index',
            'icon' => 'zmdi zmdi-view-module'
        ],
        'modules_solutions' => [
            'title' => ['ru' => 'Доступные решения', 'en' => 'Available solutions', 'es' => 'Las soluciones disponibles'],
            'route' => 'core.system.modules.solutions.index',
            'icon' => 'zmdi zmdi-widgets'
        ],
        'groups' => [
            'title' => ['ru' => 'Группы пользователей', 'en' => 'Group of users', 'es' => 'Grupos de usuarios'],
            'route' => 'core.system.groups.index',
            'icon' => 'zmdi zmdi-accounts-list'
        ],
        'users' => [
            'title' => ['ru' => 'Пользователи', 'en' => 'Users', 'es' => 'Usuarios'],
            'route' => 'core.system.users.index',
            'icon' => 'zmdi zmdi-accounts-alt'
        ]
    ]
];