<?php

return [
    'package_name' => 'core_system',
    'package_title' => [
        'ru' => 'Системный модуль',
        'en' => 'System module',
        'es' => 'Módulo de sistema'
    ],
    'package_icon' => 'zmdi zmdi-settings-square',
    'version' => [
        'ver' => 1.0,
        'date' => '15.04.2016'
    ]
];
