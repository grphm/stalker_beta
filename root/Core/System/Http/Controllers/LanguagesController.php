<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use STALKER_CMS\Vendor\Interfaces\CrudInterface;
use STALKER_CMS\Vendor\Models\Languages;

/**
 * Контроллер языков
 * Class LanguagesController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class LanguagesController extends ModuleController implements CrudInterface {

    /**
     *
     */
    public function index() {
        // TODO: Implement index() method.
    }

    /**
     *
     */
    public function create() {
        // TODO: Implement create() method.
    }

    /**
     *
     */
    public function store() {
        // TODO: Implement store() method.
    }

    /**
     * @param $id
     */
    public function edit($id) {
        // TODO: Implement edit() method.
    }

    /**
     * @param $id
     */
    public function update($id) {
        // TODO: Implement update() method.
    }

    /**
     * @param $id
     */
    public function destroy($id) {
        // TODO: Implement destroy() method.
    }

    /**
     * Вернуть список активных языков
     * @return mixed
     */
    public function getLanguages() {

        return Languages::whereActive(TRUE)->lists('title', 'slug');
    }
}