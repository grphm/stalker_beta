<?php
namespace STALKER_CMS\Core\System\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use STALKER_CMS\Vendor\Helpers;
use STALKER_CMS\Vendor\Models\Packages;

/**
 * Class ModulesController
 * @package STALKER_CMS\Core\System\Http\Controllers
 */
class ModulesController extends ModuleController {

    /**
     * ModulesController constructor.
     */
    public function __construct() {

        $this->middleware('auth');
    }

    /**
     * Список доступных модулей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        \PermissionsController::allowPermission('core_system', 'modules');
        $packages = new Collection();
        foreach (Packages::orderBy('required', 'DESC')->orderBy('order')->get() as $package):
            $packages[$package->slug] = $package;
        endforeach;
        return view('core_system_views::modules.index', compact('packages'));
    }

    /**
     * Повторная публикация пакета
     * @param \Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rebuild(\Request $request) {

        $redirect_route = 'core.system.modules.index';
        if ($request::has('redirect_route')):
            $redirect_route = $request::input('redirect_route');
        endif;
        \PermissionsController::allowPermission('core_system', 'modules');
        foreach (Packages::whereEnabled(TRUE)->get() as $package):
            \PackagesController::publishesPackageAssets($package);
        endforeach;
        \Artisan::call('config:cache', ['--no-ansi' => TRUE, '--quiet' => TRUE]);
        return redirect()->route($redirect_route);
    }

    /**
     * Последовательность загрузки модулей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function boot() {

        \PermissionsController::allowPermission('core_system', 'modules');
        $packages = Packages::getNestedPackages();
        return view('core_system_views::modules.boot', compact('packages'));
    }

    /**
     * Отключение модулей
     * Установка и включение модулей
     * @return \Illuminate\Http\JsonResponse
     */
    public function update() {

        \PermissionsController::allowPermission('core_system', 'modules');
        $request = \RequestController::isAJAX()->init();
        foreach (Packages::whereEnabled(TRUE)->whereRequired(FALSE)->get() as $package):
            if ($request::has($package->slug) === FALSE):
                \PackagesController::disabledPackage($package);
            endif;
        endforeach;
        foreach (Packages::whereEnabled(FALSE)->whereRequired(FALSE)->get() as $package):
            if ($request::has($package->slug)):
                if (empty($package->install_path)):
                    \PackagesController::installPackage($package);
                else:
                    \PackagesController::enabledPackage($package);
                endif;
            endif;
        endforeach;
        return \ResponseController::success(200)->redirect(route('core.system.modules.rebuild'))->json();
    }

    /**
     * Выключение модуля
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy() {

        \PermissionsController::allowPermission('core_system', 'modules');
        $request = \RequestController::isAJAX()->init();
        if ($package = Packages::whereId($request::get('package'))->whereEnabled(TRUE)->whereRequired(FALSE)->first()):
            \PackagesController::disabledPackage($package);
            return \ResponseController::success(200)->redirect(route('core.system.modules.index'))->json();
        endif;
        return \ResponseController::error(403)->json();
    }

    /**
     * Сохранение последовательности загрузки модулей
     * @return \Illuminate\Http\JsonResponse
     */
    public function bootUpdate() {

        \PermissionsController::allowPermission('core_system', 'modules');
        $request = \RequestController::isAJAX()->init();
        if (\ValidatorController::passes($request, ['elements' => 'required'])):
            foreach (explode(',', $request::input('elements')) as $index => $package_slug):
                Packages::whereSlug($package_slug)->update(['order' => $index + 1]);
            endforeach;
            return \ResponseController::success(202)->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Обновление файлов ядра системы
     * Обновление установленных решений
     */
    public function getUpdates() {

        $composer = Helpers\settings(['core_system', 'settings', 'composer']);
        $php = Helpers\settings(['core_system', 'settings', 'php']);
        if (\File::exists($composer)):
            try {
                if (!\File::exists(storage_path('app/updates'))):
                    \File::makeDirectory(storage_path('app/updates'), 0754);
                else:
                    \File::deleteDirectory(storage_path('app/updates'));
                    \File::makeDirectory(storage_path('app/updates'), 0754);
                endif;
                chdir(storage_path('app/updates'));
                if (substr(php_uname(), 0, 7) == "Windows"):
                    shell_exec(trim($php . ' ' . $composer . ' self-update'));
                    shell_exec(trim($php . ' ' . $composer . ' --prefer-dist --no-progress --no-ansi require "grphm/stalker_updates":"@dev"'));
                else:
                    shell_exec('COMPOSER_HOME="' . storage_path('app/updates') . '" ' . trim($php . ' ' . $composer) . ' self-update');
                    shell_exec('COMPOSER_HOME="' . storage_path('app/updates') . '" ' . trim($php . ' ' . $composer) . ' --prefer-dist --no-progress --no-ansi require "grphm/stalker_updates":"@dev"');
                endif;
                if (\File::exists(storage_path('app/updates/vendor/grphm/stalker_updates/public'))):
                    \File::copyDirectory(storage_path('app/updates/vendor/grphm/stalker_updates/public'), public_path());
                endif;
                if (\File::exists(storage_path('app/updates/vendor/grphm/stalker_updates/root'))):
                    \File::copyDirectory(storage_path('app/updates/vendor/grphm/stalker_updates/root'), base_path('root'));
                endif;
                \File::deleteDirectory(storage_path('app/updates'));
                return redirect()->to(route('core.system.modules.index') . '?status=200');
            } catch (\Exception $e) {
                return redirect()->to(route('core.system.modules.index') . '?status=500');
            }
        else:
            return redirect()->to(route('core.system.modules.index') . '?status=2404');
        endif;
    }
}