<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    public function up() {

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('group_id', FALSE, TRUE)->default(0)->nullable()->index();
            $table->string('locale', 10)->nullable()->index();
            $table->string('name', 100)->nullable()->index();
            $table->string('login', 100)->nullable()->unique();
            $table->string('email', 100)->nullable()->unique();
            $table->string('country', 100)->nullable();
            $table->string('city', 100)->nullable();
            $table->string('address', 250)->nullable();
            $table->dateTime('birth_date')->nullable();
            $table->tinyInteger('age', FALSE, TRUE)->default(0)->nullable();
            $table->tinyInteger('sex', FALSE, TRUE)->default(0)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('skype', 20)->nullable();
            $table->text('social')->nullable();
            $table->string('photo', 100)->nullable();
            $table->string('thumbnail', 100)->nullable();
            $table->text('comment')->nullable();
            $table->integer('likes', FALSE, TRUE)->default(0)->nullable();
            $table->integer('approve', FALSE, TRUE)->default(1)->nullable()->index();
            $table->smallInteger('active', FALSE, TRUE)->default(0)->nullable()->index();
            $table->string('password', 60)->nullable();
            $table->string('remember_token', 255)->nullable();
            $table->timestamp('last_login')->nullable();
            $table->timestamp('disabled_at')->nullable();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::drop('users');
    }
}
