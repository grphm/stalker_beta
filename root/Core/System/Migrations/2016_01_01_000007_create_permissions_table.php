<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration {

    public function up() {

        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id', FALSE, TRUE)->nullable()->index();
            $table->string('module', 32)->nullable()->index();
            $table->string('action', 32)->nullable()->index();
            $table->integer('status', FALSE, TRUE)->default(0)->nullable()->index();
        });
    }

    public function down() {

        Schema::dropIfExists('permissions');
    }
}
