<?php

namespace STALKER_CMS\Core\System\Models;

use STALKER_CMS\Vendor\Models\BaseModel;

/**
 * Модель Сессии
 * Class Sessions
 * @package STALKER_CMS\Core\System\Models
 */
class Sessions extends BaseModel {

    /**
     * @var string
     */
    protected $table = 'sessions';
    /**
     * @var array
     */
    protected $fillable = [];
    /**
     * @var array
     */
    protected $hidden = [];
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var array
     */
    protected $dates = [];

}
