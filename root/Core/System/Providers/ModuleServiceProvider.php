<?php

namespace STALKER_CMS\Core\System\Providers;

use STALKER_CMS\Core\System\Http\Controllers\LanguagesController;
use STALKER_CMS\Core\System\Http\Controllers\PermissionsController;
use STALKER_CMS\Core\System\Http\Controllers\SettingsController;
use STALKER_CMS\Vendor\Providers\ServiceProvider;

/**
 * Class ModuleServiceProvider
 * @package STALKER_CMS\Core\System\Providers
 */
class ModuleServiceProvider extends ServiceProvider {

    /**
     * Метод загрузка
     */
    public function boot() {

        $this->setPath(__DIR__ . '/../');
        $this->registerViews('core_system_views');
        $this->registerLocalization('core_system_lang');
        $this->registerConfig('core_system::config', 'Config/system.php');
        $this->registerSettings('core_system::settings', 'Config/settings.php');
        $this->registerActions('core_system::actions', 'Config/actions.php');
        $this->registerSystemMenu('core_system::menu', 'Config/menu.php');
        $this->registerMimes();
        $this->registerLayout();
    }

    /**
     * Метод регистрация
     */
    public function register() {

        \App::bind('PermissionsController', function () {
            return new PermissionsController();
        });
        \App::bind('LanguagesController', function () {
            return new LanguagesController();
        });
        $this->app->singleton('Permissions', function ($app) {
            return \Auth::check() ? \PermissionsController::getPermissions() : NULL;
        });
        $this->app->singleton('PackagesEnabled', function ($app) {
            return \PermissionsController::packagesEnabled();
        });
        $this->app->singleton('PackagesInstalled', function ($app) {
            return \PermissionsController::packagesInstalled();
        });
        $this->app->singleton('Settings', function ($app) {
            return SettingsController::getSettings();
        });
    }

    /********************************************************************************************************************/

    /**
     * Метод mime-типов
     */
    private function registerMimes() {

        $this->setConfigurationFile('core_system::mimes', 'Config/mimes.php');
    }

    /**
     * Текущий слой
     */
    private function registerLayout() {

        view()->share('current_layout', 'admin');
    }
}
