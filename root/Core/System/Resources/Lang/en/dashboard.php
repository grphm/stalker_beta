<?php

return [
    'control_panel' => 'Control Panel',
    'edit_profile' => 'Edit profile',
    'edit_profile_permissions' => 'Access Settings',
    'header' => [
        'notification' => 'Notifications',
        'notification_view_all' => 'Read all',
        'settings' => [
            'full_screen' => 'Full Screen',
            'edit' => 'Settings',
            'logout' => 'Log out',
        ],
        'debug_mode' => 'Debug mode',
        'services_mode' => 'Maintenance mode',
    ],
    'title' => 'Dashboard',
    'count_users' => 'Users',
    'uploaded_files' => 'Uploaded files',
    'installed_modules' => 'Module is installed',
    'register_statistic' => [
        'title' => 'Registration for the current month',
        'views' => 'Views',
        'visitors' => 'Visitors',
    ],
    'feedback' => [
        'title' => 'Feedback',
        'inbox' => 'Received',
        'views' => 'Views',
        'sends' => 'Answer',
    ],
    'feedback_list' => [
        'title' => 'Feedback',
        'show_all' => 'Read all'
    ],
    'edit_template' => 'Edit template'
];