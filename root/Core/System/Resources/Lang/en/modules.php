<?php

return [
    'select_all' => 'Select all',
    'boot' => 'Boot Sequence',
    'information' => 'Information',
    'check_updates' => 'Update to the latest version',
    'required' => 'Required for installation',
    'clear_cache_notification_part1' => 'Before installing the modules, clear the cache',
    'clear_cache_notification_part2' => 'settings',
    'disabled' => [
        'question' => 'Disable module',
        'confirmbuttontext' => 'Yes, disable',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Disable',
    ]
];