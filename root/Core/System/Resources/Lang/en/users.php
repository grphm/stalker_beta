<?php

return [
    'total' => 'Total',
    'users_count' => 'user|users|users',
    'search' => 'Enter the name or email',
    'sort_name' => 'Name and Surname',
    'sort_email' => 'E-mail address',
    'sort_created_at' => 'Date of registration',
    'sort_last_login' => 'Last signed',
    'all_groups' => 'All groups',
    'edit' => 'Edit',
    'group' => 'Group',
    'register' => 'Register',
    'login' => 'Login',
    'status' => 'Status',
    'offline' => 'Offline',
    'online' => 'Online',
    'not_logged' => 'Not logged',
    'active' => 'Active',
    'blocked_by' => 'Blocked by',
    'delete' => [
        'question' => 'Delete user',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding user',
        'form' => [
            'name' => 'Name and Surname',
            'email' => 'Email',
            'login' => 'Login',
            'password' => 'Password',
            'access' => 'Access granted',
            'send_notification' => 'Notify user',
            'send_notification_help_description' => 'Send a letter with the notification of registration',
            'address' => 'Address',
            'birth_date' => 'Birthdate',
            'age' => 'Age',
            'phone' => 'Phone',
            'skype' => 'Skype',
            'vk' => 'VKontakte',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'comment' => 'Comments',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit user',
        'form' => [
            'name' => 'Name and Surname',
            'email' => 'Email',
            'login' => 'Login',
            'password' => 'Password',
            'password_help_description' => 'Leave blank if you do not need to change your password',
            'access' => 'Access granted',
            'address' => 'Address',
            'birth_date' => 'Birthdate',
            'age' => 'Age',
            'phone' => 'Phone',
            'skype' => 'Skype',
            'vk' => 'VKontakte',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'comment' => 'Comments',
            'submit' => 'Save'
        ]
    ]
];