<?php

return [
    'select_all' => 'Seleccionar todo',
    'boot' => 'Secuencia de inicio',
    'information' => 'Información',
    'check_updates' => 'Actualizar a la última versión',
    'required' => 'Necesario para la instalación',
    'clear_cache_notification_part1' => 'Antes de instalar los módulos, borrar la memoria caché',
    'clear_cache_notification_part2' => 'ajustes',
    'disabled' => [
        'question' => 'Módulo desactivar',
        'confirmbuttontext' => 'Sí, deshabilitar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Inhabilitar',
    ]
];