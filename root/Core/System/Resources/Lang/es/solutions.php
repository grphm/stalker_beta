<?php

return [
    'settings' => 'Ajustes',
    'check_updates' => 'Buscar actualizaciones',
    'install' => 'Instalar',
    'empty' => 'No hay soluciones',
    'disable' => [
        'question' => 'Retire la solución',
        'confirmbuttontext' => 'Sí, retire',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Retire',
    ],
    'enable' => [
        'question' => 'Instalar la solución',
        'confirmbuttontext' => 'Sí, instalar',
        'cancelbuttontext' => 'He cambiado de idea'
    ]
];