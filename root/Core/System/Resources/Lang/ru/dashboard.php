<?php

return [
    'control_panel' => 'Панель управления',
    'edit_profile' => 'Редактировать профиль',
    'edit_profile_permissions' => 'Настройка доступов',
    'header' => [
        'notification' => 'Уведомления',
        'notification_view_all' => 'Читать все',
        'settings' => [
            'full_screen' => 'Полный экран',
            'edit' => 'Настройка',
            'logout' => 'Завершить сеанс',
        ],
        'debug_mode' => 'Режим отладки',
        'services_mode' => 'Режим обслуживания',
    ],
    'title' => 'Дашборд',
    'count_users' => 'Пользователи',
    'uploaded_files' => 'Загружено файлов',
    'installed_modules' => 'Установлено модулей',
    'register_statistic' => [
        'title' => 'Регистрации за текущий месяц',
        'views' => 'Просмотров',
        'visitors' => 'Посетителей',
    ],
    'feedback' => [
        'title' => 'Обратная связь',
        'inbox' => 'Получено',
        'views' => 'Просмотрено',
        'sends' => 'Отвечено',
    ],
    'feedback_list' => [
        'title' => 'Обратная связь',
        'show_all' => 'Читать все'
    ],
    'edit_template' => 'Изменить шаблон'
];