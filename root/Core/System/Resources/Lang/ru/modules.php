<?php

return [
    'select_all' => 'Выбрать все',
    'boot' => 'Последовательность загрузки',
    'information' => 'Информация',
    'check_updates' => 'Обновить до последней версии',
    'required' => 'Для установки требуются',
    'clear_cache_notification_part1' => 'Перед установкой модулей очистите кэш',
    'clear_cache_notification_part2' => 'настроек',
    'disabled' => [
        'question' => 'Отключить модуль',
        'confirmbuttontext' => 'Да, отключить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Отключить',
    ]
];