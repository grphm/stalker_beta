<?php

return [
    'total' => 'Всего',
    'users_count' => 'пользователь|пользователя|пользователей',
    'search' => 'Введите имя или электронный адрес',
    'sort_name' => 'Имя и Фамилия',
    'sort_email' => 'Электронный адрес',
    'sort_created_at' => 'Дата регистрации',
    'sort_last_login' => 'Последний вход',
    'all_groups' => 'Все группы',
    'edit' => 'Редактировать',
    'group' => 'Группа',
    'register' => 'Регистрация',
    'login' => 'Вход',
    'status' => 'Статус',
    'offline' => 'Не в сети',
    'online' => 'В сети',
    'not_logged' => 'Не входил',
    'active' => 'Активен',
    'blocked_by' => 'Заблокирован от',
    'delete' => [
        'question' => 'Удалить пользователя',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'insert' => [
        'breadcrumb' => 'Добавить',
        'title' => 'Добавление пользователя',
        'form' => [
            'name' => 'Имя пользователя',
            'email' => 'Электронная почта',
            'login' => 'Логин',
            'password' => 'Пароль',
            'access' => 'Доступ разрешен',
            'send_notification' => 'Уведомить пользователя',
            'send_notification_help_description' => 'Отправить письмо с уведомлением о регистрации',
            'address' => 'Адрес',
            'birth_date' => 'Дата рождения',
            'age' => 'Возраст',
            'phone' => 'Телефон',
            'skype' => 'Skype',
            'vk' => 'Вконтакте',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'comment' => 'Комментарий',
            'submit' => 'Сохранить'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Редактировать',
        'title' => 'Редактирование пользователя',
        'form' => [
            'name' => 'Имя пользователя',
            'email' => 'Электронная почта',
            'login' => 'Логин',
            'password' => 'Пароль',
            'password_help_description' => 'Оставьте пустым если пароль менять не нужно',
            'access' => 'Доступ разрешен',
            'address' => 'Адрес',
            'birth_date' => 'Дата рождения',
            'age' => 'Возраст',
            'phone' => 'Телефон',
            'skype' => 'Skype',
            'vk' => 'Вконтакте',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'comment' => 'Комментарий',
            'submit' => 'Сохранить'
        ]
    ]
];