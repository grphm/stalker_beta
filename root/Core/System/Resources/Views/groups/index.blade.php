@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_system::menu.menu_child.groups.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.groups.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_system::menu.menu_child.groups.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.groups.title')) !!}
        </h2>
    </div>
    <div class="card">
        @BtnAdd('core.system.groups.create')
        <div class="card-body card-padding">
            <div class="row">
                @if($groups->count())
                    @foreach($groups as $index => $group)
                        <div class="js-item-container col-sm-3">
                            <div class="card">
                                <div class="card-header bgm-bluegray">
                                    <h2>
                                        {{ $group->title }}
                                        <small>
                                            {{ $group->users_count() }}
                                            @choice(Lang::get('core_system_lang::groups.users_count'), $group->users_count())
                                        </small>
                                    </h2>
                                    <ul class="actions actions-alt">
                                        <li class="dropdown">
                                            <a aria-expanded="false" data-toggle="dropdown" href="">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="{{ route('core.system.groups.accesses-index', $group->id) }}">
                                                        @lang('core_system_lang::groups.permissions')
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('core.system.groups.edit', $group->id) }}">
                                                        @lang('core_system_lang::groups.edit')
                                                    </a>
                                                </li>
                                                @if($group->id == Auth::user()->group_id || $group->required)
                                                @else
                                                    <li class="divider"></li>
                                                    <li>
                                                        {!! Form::open(['route' => ['core.system.groups.destroy', $group->id], 'method' => 'DELETE']) !!}
                                                        <button type="submit"
                                                                class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                                autocomplete="off"
                                                                data-question="@lang('core_system_lang::groups.delete.question') &laquo;{{ $group->title }}&raquo;?"
                                                                data-confirmbuttontext="@lang('core_system_lang::groups.delete.confirmbuttontext')"
                                                                data-cancelbuttontext="@lang('core_system_lang::groups.delete.cancelbuttontext')">
                                                            @lang('core_system_lang::groups.delete.submit')
                                                        </button>
                                                        {!! Form::close() !!}
                                                    </li>
                                                @endif
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body card-padding p-10">
                                    <ul class="clist clist-angle">
                                        <li>ID: @numDimensions($group->id)</li>
                                        <li>
                                            @lang('core_system_lang::groups.symbolic_code'):
                                            {{ $group->slug }}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
@stop