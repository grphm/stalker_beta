@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.title')) !!}
        </li>
        <li>
            <a href="{{ route('core.system.modules.index') }}">
                <i class="{{ config('core_system::menu.menu_child.modules.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.modules.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-time-countdown zmdi-hc-fw"></i> @lang('core_system_lang::modules.boot')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="zmdi zmdi-time-countdown zmdi-hc-fw"></i> @lang('core_system_lang::modules.boot')
        </h2>
    </div>
    <div class="card">
        <div class="card-body card-padding m-h-250">
            <div class="listview lv-bordered lv-lg">
                <div class="lv-body">
                    <ul class="nestable-list" data-action-url="{!! route('core.system.modules.boot.update') !!}">
                        @foreach($packages as $index => $package)
                            <li class="lv-item media" data-element="{!! $package->slug !!}">
                                <div class="pull-left">
                                    <i class="zmdi zmdi-swap-vertical f-20 p-t-10 cursor-move"></i>
                                </div>
                                <div class="media-body">
                                    <div class="lv-title">
                                        <i class="{{ config($package->slug . '::config.package_icon' ) }}"></i> {{ $package->title }}
                                    </div>
                                    <small class="lv-small">
                                        {{ $package->description }}
                                    </small>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
@stop