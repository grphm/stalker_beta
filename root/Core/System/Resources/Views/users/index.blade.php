@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_system::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_system::menu.menu_child.users.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.users.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_system::menu.menu_child.users.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_system::menu.menu_child.users.title')) !!}
        </h2>
    </div>
    @BtnAdd('core.system.users.create')
    <div class="card">
        <div class="lv-header-alt clearfix m-b-5">
            <h2 class="lvh-label hidden-xs">
                @if(isset($groups[\Request::input('group')]))
                    {!! $groups[\Request::input('group')] !!}:
                @else
                    @lang('core_system_lang::users.total'):
                @endif
                {!! $total_users !!} @choice(Lang::get('core_system_lang::users.users_count'), $total_users)
            </h2>

            <div class="lvh-search">
                {!! Form::open(['route' => 'core.system.users.index', 'method' => 'get']) !!}
                <input type="text" name="search" placeholder="@lang('core_system_lang::users.search')"
                       class="lvhs-input">
                <i class="lvh-search-close">&times;</i>
                {!! Form::close() !!}
            </div>

            <ul class="lv-actions actions">
                <li>
                    <a href="" class="lvh-search-trigger"><i class="zmdi zmdi-search"></i></a>
                </li>
                @if($users->count())
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                            <i class="zmdi zmdi-sort-amount-asc"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="{!! route('core.system.users.index', array_merge(Request::all(), ['sort_field' => 'name', 'sort_direction' => 'asc'])) !!}">
                                    @lang('core_system_lang::users.sort_name')
                                </a>
                            </li>
                            <li>
                                <a href="{!! route('core.system.users.index', array_merge(Request::all(), ['sort_field' => 'email', 'sort_direction' => 'asc'])) !!}">
                                    @lang('core_system_lang::users.sort_email')
                                </a>
                            </li>
                            <li>
                                <a href="{!! route('core.system.users.index', array_merge(Request::all(), ['sort_field' => 'created_at', 'sort_direction' => 'asc'])) !!}">
                                    @lang('core_system_lang::users.sort_created_at')
                                </a>
                            </li>
                            <li>
                                <a href="{!! route('core.system.users.index', array_merge(Request::all(), ['sort_field' => 'last_login', 'sort_direction' => 'asc'])) !!}">
                                    @lang('core_system_lang::users.sort_last_login')
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                            <i class="zmdi zmdi-sort-amount-desc"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="{!! route('core.system.users.index', array_merge(Request::all(), ['sort_field' => 'name', 'sort_direction' => 'desc'])) !!}">
                                    @lang('core_system_lang::users.sort_name')
                                </a>
                            </li>
                            <li>
                                <a href="{!! route('core.system.users.index', array_merge(Request::all(), ['sort_field' => 'email', 'sort_direction' => 'desc'])) !!}">
                                    @lang('core_system_lang::users.sort_email')
                                </a>
                            </li>
                            <li>
                                <a href="{!! route('core.system.users.index', array_merge(Request::all(), ['sort_field' => 'created_at', 'sort_direction' => 'desc'])) !!}">
                                    @lang('core_system_lang::users.sort_created_at')
                                </a>
                            </li>
                            <li>
                                <a href="{!! route('core.system.users.index', array_merge(Request::all(), ['sort_field' => 'last_login', 'sort_direction' => 'desc'])) !!}">
                                    @lang('core_system_lang::users.sort_last_login')
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
                <li class="dropdown">
                    <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                        <i class="zmdi zmdi-filter-list"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="{!! route('core.system.users.index') !!}">@lang('core_system_lang::users.all_groups')</a>
                        </li>
                        <li class="divider"></li>
                        @foreach($groups as $group_id => $group_description)
                            <li>
                                <a href="{!! route('core.system.users.index') . '?group=' . $group_id !!}">{{ $group_description }}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            </ul>
        </div>
        <div class=" card-body card-padding m-h-250">
            @if($users->count())
                <div class="listview lv-bordered lv-lg">
                    <div class="lv-body">
                        @foreach($users as $user)
                            <div class="js-item-container lv-item media">
                                <div class="pull-left {!! !empty($user->avatar_thumbnail) ? ' lightbox' : '' !!} clearfix">
                                    @ProfileAvatar($user->name, $user->avatar_thumbnail)
                                </div>
                                <div class="media-body">
                                    <div class="lv-title">{!! $user->name !!}</div>
                                    <small class="lv-small">
                                        {!! $user->email !!} {!! $user->phone !!}
                                    </small>
                                    <ul class="lv-attrs">
                                        <li>
                                            @lang('core_system_lang::users.group'):
                                            {!! $user->group->title !!}
                                        </li>
                                        <li>
                                            @lang('core_system_lang::users.register'):
                                            {!! $user->CreatedDate !!}
                                        </li>
                                        <li>
                                            @lang('core_system_lang::users.login'):
                                            {!! $user->LastLogin !!}
                                        </li>
                                        <li>
                                            @lang('core_system_lang::users.status'):
                                            {!! $user->activeStatus !!} {!! $user->isOnline !!}
                                        </li>
                                    </ul>
                                    <div class="lv-actions actions dropdown">
                                        <a aria-expanded="true" data-toggle="dropdown" href="">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="{{ route('core.system.users.edit', $user->id) }}">
                                                    @lang('core_system_lang::users.edit')
                                                </a>
                                            </li>
                                            @if(($user->id == Auth::user()->id || $user->group->required))
                                            @else
                                                <li class="divider"></li>
                                                <li>
                                                    {!! Form::open(['route' => ['core.system.users.destroy', $user->id], 'method' => 'DELETE']) !!}
                                                    <button type="submit"
                                                            class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                            autocomplete="off"
                                                            data-question="@lang('core_system_lang::users.delete.question') &laquo;{{ $user->name }}&raquo;?"
                                                            data-confirmbuttontext="@lang('core_system_lang::users.delete.confirmbuttontext')"
                                                            data-cancelbuttontext="@lang('core_system_lang::users.delete.cancelbuttontext')">
                                                        @lang('core_system_lang::users.delete.submit')
                                                    </button>
                                                    {!! Form::close() !!}
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    {!! $users->appends(['group' => \Request::input('group'), 'sort_field' => \Request::input('sort_field'), 'sort_direction' => \Request::input('sort_direction'), 'search' => \Request::input('search')])->render() !!}
                </div>
            @endif
        </div>
    </div>
@stop