<?php

return [
    'package' => 'core_uploads',
    'title' => ['ru' => 'Загрузка', 'en' => 'Upload', 'es' => 'Populares subir'],
    'route' => '__#',
    'icon' => 'zmdi zmdi-upload',
    'menu_child' => [
        'uploads' => [
            'title' => ['ru' => 'Файлы', 'en' => 'Files', 'es' => 'Archivos'],
            'route' => 'core.uploads.index',
            'icon' => 'zmdi zmdi-cloud',
        ],
        'types' => [
            'title' => ['ru' => 'Типы файлов', 'en' => 'File types', 'es' => 'Tipos de archivo'],
            'route' => 'core.uploads.types.index',
            'icon' => 'zmdi zmdi-collection-item',
        ],
    ]
];