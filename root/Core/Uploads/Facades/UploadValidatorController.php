<?php
namespace STALKER_CMS\Core\Uploads\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Фасад контроллера Загрузки
 * Class UploadValidatorController
 * @package STALKER_CMS\Core\Uploads\Facades
 */
class UploadValidatorController extends Facade {

    protected static function getFacadeAccessor() {

        return 'UploadValidatorController';
    }
}