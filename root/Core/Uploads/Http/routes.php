<?php
\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function () {
    \Route::resource('uploads', 'UploadsController',
        [
            'only' => ['index', 'store', 'destroy'],
            'names' => [
                'index' => 'core.uploads.index',
                'store' => 'core.uploads.store',
                'destroy' => 'core.uploads.destroy'
            ]
        ]
    );
    \Route::get('uploads/types', ['as' => 'core.uploads.types.index', 'uses' => 'UploadsTypesController@index']);
    \Route::post('uploads/type/{id}/enabled', ['as' => 'core.uploads.types.update', 'uses' => 'UploadsTypesController@update']);
    \Route::delete('uploads/type/{id}/disabled', ['as' => 'core.uploads.types.destroy', 'uses' => 'UploadsTypesController@destroy']);
});
\Route::group(['middleware' => 'public'], function () {
    \Route::match(['get', 'post'], 'files/download/{crypt_id}', ['as' => 'core.uploads.file.download', 'uses' => 'UploadsController@dynamicDownloadFile']);
});