<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUploadsTables extends Migration {

    public function up() {

        Schema::create('file_types', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title')->nullable();
            $table->string('icon', 50)->nullable();
            $table->string('extensions', 25)->nullable()->index();
            $table->string('mime_type', 250)->nullable();
            $table->integer('max_size', FALSE, TRUE)->default(0)->nullable();
            $table->boolean('enabled', FALSE, TRUE)->default(1)->nullable();
        });

        Schema::create('uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('file_type_id', FALSE, TRUE)->default(0)->nullable();
            $table->integer('package_id', FALSE, TRUE)->default(0)->nullable()->index();
            $table->integer('unit_id', FALSE, TRUE)->default(0)->nullable()->index();
            $table->string('path', 200)->nullable();
            $table->string('original_name', 100)->nullable();
            $table->double('file_size', FALSE, TRUE)->default(0)->nullable();
            $table->string('mime_type', 100)->nullable();
            $table->timestamps();
        });
    }

    public function down() {

        Schema::dropIfExists('file_types');
        Schema::dropIfExists('uploads');
    }
}

