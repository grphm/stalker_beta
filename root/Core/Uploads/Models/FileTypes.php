<?php
namespace STALKER_CMS\Core\Uploads\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;

/**
 * Модель Тип файла
 * Class FileTypes
 * @package STALKER_CMS\Core\Uploads\Models
 */
class FileTypes extends BaseModel implements ModelInterface {

    /**
     * @var string
     */
    protected $table = 'file_types';
    /**
     * @var array
     */
    protected $fillable = ['title', 'icon', 'extensions', 'mime_type', 'enabled'];
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var bool
     */
    public $timestamps = FALSE;

    /**
     * @param $request
     */
    public function insert($request) {
        // TODO: Implement insert() method.
    }

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function replace($id, $request) {

        $model = static::findOrFail($id);
        $model->enabled = $request::input('enabled');
        $model->save();
        $model->touch();
        return $model;
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        return static::findOrFail($id)->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /**
     * Вернуть название
     * @return string
     */
    public function getTitleAttribute() {

        if (!empty($this->attributes['title'])):
            $json = json_decode($this->attributes['title'], TRUE);
            return isset($json[\App::getLocale()]) ? $json[\App::getLocale()] : '';
        endif;
    }

    /**
     * Вернуть загруженные файлы
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files() {

        return $this->hasMany('\STALKER_CMS\Core\Uploads\Models\Upload', 'file_type_id', 'id');
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['title' => 'required', 'icon' => 'required', 'extensions' => 'required', 'mime_type' => 'required'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return ['title' => 'required', 'icon' => 'required', 'extensions' => 'required', 'mime_type' => 'required'];
    }
}