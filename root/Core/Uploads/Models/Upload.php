<?php
namespace STALKER_CMS\Core\Uploads\Models;

use League\Flysystem\Exception;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

/**
 * Модель Загруженный файл
 * Class Upload
 * @package STALKER_CMS\Core\Uploads\Models
 */
class Upload extends BaseModel implements ModelInterface {

    use ModelTrait;

    /**
     * @var string
     */
    protected $table = 'uploads';
    /**
     * @var array
     */
    protected $fillable = ['file_type_id', 'package_id', 'unit_id', 'path', 'original_name', 'file_size', 'mime_type'];
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @param $request
     * @return $this
     */
    public function insert($request) {

        $this->file_type_id = $request::input('file_type_id');
        $this->package_id = $request::has('package_id') ? $request::input('package_id') : NULL;
        $this->unit_id = $request::has('unit_id') ? $request::input('unit_id') : NULL;
        $this->path = $request::input('path');
        $this->original_name = $request::file('file')->getClientOriginalName();
        $this->file_size = $request::file('file')->getClientSize();
        $this->mime_type = $request::file('file')->getClientMimeType();
        $this->save();
        return $this;
    }

    /**
     * @param $id
     * @param $request
     */
    public function replace($id, $request) {
        // TODO: Implement replace() method.
    }

    /**
     * @param array|int $id
     * @return mixed
     */
    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /**
     * Тип файла
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type_file() {

        return $this->belongsTo('\STALKER_CMS\Core\Uploads\Models\FileTypes', 'file_type_id', 'id');
    }

    /**
     * Пакет
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function package() {

        return $this->belongsTo('\STALKER_CMS\Vendor\Models\Packages', 'package_id', 'id');
    }

    /**
     * Иконка пакета
     * @return string
     */
    public function getPackageIcoAttribute() {

        if (!empty($this->package)):
            return '<i class="fa ' . config($this->package->slug . '::config.package_icon') . '"> </i>';
        endif;
    }

    /**
     * Название пакета
     * @return mixed
     */
    public function getPackageTitleAttribute() {

        if (!empty($this->package)):
            return $this->package->title;
        endif;
    }

    /**
     * Размер файла
     * @return string
     */
    public function getUploadSizeAttribute() {

        if ($this->attributes['file_size']):
            if ($this->attributes['file_size'] > 1048576):
                return round($this->attributes['file_size'] / 1048576, 2) . ' ' . \Lang::get('core_uploads_lang::uploads.sizes.mb');
            elseif ($this->attributes['file_size'] > 1024):
                return round($this->attributes['file_size'] / 1024, 2) . ' ' . \Lang::get('core_uploads_lang::uploads.sizes.kb');
            else:
                return $this->attributes['file_size'] . ' ' . \Lang::get('core_uploads_lang::uploads.sizes.b');
            endif;
        else:
            return '<span class="c-red">' . \Lang::get('core_uploads_lang::uploads.sizes.undefined') . '</span>';
        endif;
    }

    /**
     * Mime-тип файла
     * @return string
     */
    public function getUploadMimeTypeAttribute() {

        if ($this->attributes['mime_type']):
            return $this->attributes['mime_type'];
        else:
            return '<span class="c-red">' . \Lang::get('core_uploads_lang::uploads.mime_undefined') . '</span>';
        endif;
    }

    /**
     * Проверка на существование файла на диске
     * @return bool|mixed
     */
    public function getExistOnDiskAttribute() {

        if (!isset($this->ExistFile)):
            if (\Storage::exists($this->attributes['path'])):
                $this->ExistFile = TRUE;
            else:
                $this->ExistFile = FALSE;
            endif;
            return $this->ExistFile;
        else:
            return $this->ExistFile;
        endif;
    }

    /**
     * Проверка файла на изображение
     * @return bool
     */
    public function getIsImageAttribute() {

        try {
            if (!$is = getimagesize(public_path('uploads' . $this->attributes['path']))):
                return FALSE;
            elseif (!in_array($is[2], array(1, 2, 3))):
                return FALSE;
            else:
                return TRUE;
            endif;
        } catch (Exception $e) {
            return FALSE;
        }
    }

    /**
     * Вернуть путь в файлу
     * @return string
     */
    public function getAssetAttribute() {

        return asset('uploads' . $this->attributes['path']);
    }

    /**
     * Вернуть относительный пусть в файлу
     * @return string
     */
    public function getFullPathAttribute() {

        return '/uploads' . $this->attributes['path'];
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return [];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return [];
    }
}