<?php

namespace STALKER_CMS\Core\Uploads\Providers;

use STALKER_CMS\Core\Uploads\Http\Controllers\UploadValidatorController;
use STALKER_CMS\Vendor\Providers\ServiceProvider;

/**
 * Class ModuleServiceProvider
 * @package STALKER_CMS\Core\Uploads\Providers
 */
class ModuleServiceProvider extends ServiceProvider {

    /**
     * Метод загрузки
     */
    public function boot() {

        $this->setPath(__DIR__ . '/../');
        $this->registerViews('core_uploads_views');
        $this->registerLocalization('core_uploads_lang');
        $this->registerConfig('core_uploads::config', 'Config/uploads.php');
        $this->registerSettings('core_uploads::settings', 'Config/settings.php');
        $this->registerActions('core_uploads::actions', 'Config/actions.php');
        $this->registerSystemMenu('core_uploads::menu', 'Config/menu.php');
    }

    /**
     * Метод регистрации
     */
    public function register() {

        \App::bind('UploadValidatorController', function () {
            return new UploadValidatorController();
        });
    }
}