<?php

return [
    'mime_type' => 'Mime-type',
    'disabled' => [
        'question' => 'Disable upload',
        'confirmbuttontext' => 'Yes, disable',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Disabled',
    ],
    'enabled' => [
        'question' => 'Enable upload',
        'confirmbuttontext' => 'Yes, enable',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Enable',
    ]
];