@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb" style="margin-bottom: 5px;">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('core_uploads::menu.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_uploads::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('core_uploads::menu.menu_child.types.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_uploads::menu.menu_child.types.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('core_uploads::menu.menu_child.types.icon') }}"></i> {!! \STALKER_CMS\Vendor\Helpers\array_translate(config('core_uploads::menu.menu_child.types.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div class="listview lv-bordered lv-lg">
            <div class="lv-body">
                @if($types->count())
                    @foreach($types as $index => $type)
                        <div class="lv-item media{{ $type->enabled ? '' : ' bgm-bluegray' }}">
                            <div class="pull-left">
                                <i class="{{ $type->icon }}"></i>
                            </div>
                            <div class="media-body">
                                <div class="lv-title">
                                    {{ $type->title }}
                                </div>
                                @if(!empty($type->mime_type))
                                    <small class="lv-small">
                                        <strong>
                                            @lang('core_uploads_lang::types.mime_type'):
                                        </strong> {{ str_replace('|',', ', $type->mime_type) }}
                                    </small>
                                @endif
                                <div class="lv-actions actions dropdown">
                                    <a aria-expanded="true" data-toggle="dropdown" href="">
                                        <i class="zmdi zmdi-more-vert"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            @if($type->enabled)
                                                {!! Form::open(['route' => ['core.uploads.types.destroy', $type->id], 'method' => 'DELETE']) !!}
                                                {!! Form::hidden('enabled', FALSE) !!}
                                                <button type="submit"
                                                        class="form-confirm-warning btn-link pull-right c-red p-r-15"
                                                        autocomplete="off"
                                                        data-question="@lang('core_uploads_lang::types.disabled.question') &laquo;{{ $type->title }}&raquo;?"
                                                        data-confirmbuttontext="@lang('core_uploads_lang::types.disabled.confirmbuttontext')"
                                                        data-cancelbuttontext="@lang('core_uploads_lang::types.disabled.cancelbuttontext')">
                                                    @lang('core_uploads_lang::types.disabled.submit')
                                                </button>
                                                {!! Form::close() !!}
                                            @else
                                                {!! Form::open(['route' => ['core.uploads.types.update', $type->id], 'method' => 'POST']) !!}
                                                {!! Form::hidden('enabled', TRUE) !!}
                                                <button type="submit"
                                                        class="form-confirm-warning btn-link pull-right c-green p-r-15"
                                                        autocomplete="off"
                                                        data-question="@lang('core_uploads_lang::types.enabled.question') &laquo;{{ $type->title }}&raquo;?"
                                                        data-confirmbuttontext="@lang('core_uploads_lang::types.enabled.confirmbuttontext')"
                                                        data-cancelbuttontext="@lang('core_uploads_lang::types.enabled.submit')">
                                                    @lang('core_uploads_lang::types.enabled.submit')
                                                </button>
                                                {!! Form::close() !!}
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@stop