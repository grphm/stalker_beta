<?php namespace STALKER_CMS\Packages\Html;

use Illuminate\Support\ServiceProvider;

class HtmlServiceProvider extends ServiceProvider {

    protected $defer = true;

    public function register() {

        $this->registerHtmlBuilder();
        $this->registerFormBuilder();
        $this->app->alias('html', 'STALKER_CMS\Packages\Html\HtmlBuilder');
        $this->app->alias('form', 'STALKER_CMS\Packages\Html\FormBuilder');
    }

    protected function registerHtmlBuilder() {

        $this->app->singleton('html', function ($app) {
            return new HtmlBuilder($app['url']);
        });
    }

    protected function registerFormBuilder() {

        $this->app->singleton('form', function ($app) {
            $form = new FormBuilder($app['html'], $app['url'], $app['session.store']->getToken());
            return $form->setSessionStore($app['session.store']);
        });
    }

    public function provides() {

        return array('html', 'form');
    }
}
