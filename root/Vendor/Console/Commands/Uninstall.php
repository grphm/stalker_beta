<?php

namespace STALKER_CMS\Vendor\Console\Commands;

use Illuminate\Console\Command;
use League\Flysystem\Exception;

/**
 * Class Uninstall
 * Команда удаляет отменяет установку CMS
 * Внимание! База Данных полностью очищается
 * @package STALKER_CMS\Vendor\Console\Commands
 */
class Uninstall extends Command {

    protected $signature = 'Uninstall';

    protected $description = 'Отмена установки';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        if ($this->confirm('Uninstall. Continue? [yes|no]')) {
            $database = config('database.connections.'.config('database.default').'.database');
            $table_index_name = 'Tables_in_'.$database;
            $tables = \DB::select('show full tables where Table_Type != "VIEW"');
            $views = \DB::select('show full tables where Table_Type = "VIEW"');
            if (count($tables)):
                foreach ($tables as $table):
                    $table = json_decode(json_encode($table), true);
                    if (is_array($table) && isset($table[$table_index_name])):
                        \Schema::drop($table[$table_index_name]);
                    endif;
                endforeach;
            endif;
            if (count($views)):
                foreach ($views as $view):
                    $view = json_decode(json_encode($view), true);
                    if (is_array($view) && isset($view[$table_index_name])):
                        \DB::statement('DROP VIEW ' . $view[$table_index_name]);
                    endif;
                endforeach;
            endif;
            if (file_exists(base_path('.env'))):
                unlink(base_path('.env'));
            endif;
            if (file_exists(base_path('bootstrap/cache/config.php'))):
                unlink(base_path('bootstrap/cache/config.php'));
            endif;
            if (file_exists(base_path('bootstrap/cache/services.php'))):
                unlink(base_path('bootstrap/cache/services.php'));
            endif;
            if (file_exists(storage_path('logs/laravel.log'))):
                unlink(storage_path('logs/laravel.log'));
            endif;

            $cachedViewsDirectory = storage_path('framework/views');
            array_map("unlink", glob("$cachedViewsDirectory/*.php"));

            setcookie ("stalker_cms", "", time() - 3600);
            $this->info('Uninstall completed successfully');
        }
    }
}
