<?php
namespace STALKER_CMS\Vendor\Console\Commands;

use Illuminate\Console\Command;

/**
 * Class Update
 * Команда обновляет CMS до заданной версии
 * @package STALKER_CMS\Vendor\Console\Commands
 */
class Update extends Command {

    protected $signature = 'Update {--composer=/usr/local/bin/composer} {--release=2.0.5.2-RC1.0} {--migrations}';
    protected $description = 'Обновление ядра CMS';

    public function __construct() {

        parent::__construct();
    }

    public function handle() {

        $errors = FALSE;
        $composer = $this->option('composer');
        $version = $this->option('release');
        $migrations = $this->option('migrations');
        $this->info('Configuration:');
        $this->info("OS ".substr(php_uname(), 0, 7));
        $this->info("Composer: $composer");
        $this->info("Package: grphm/stalker_update_rc");
        $this->info("Version: $version");
        $hasMigrations = $migrations ? 'YES' : 'NO';
        $this->info("Run migrations: $hasMigrations");
        if(!\File::exists($composer)):
            $composer = \STALKER_CMS\Vendor\Helpers\settings(['core_system', 'settings', 'composer']);
            if(!\File::exists($composer)):
                $this->error('Error: Composer not found!');
                $errors = TRUE;
            endif;
        endif;
        if(!$errors && $this->confirm('Execute the update STALKER CMS. Continue? [yes|no]')):
            $this->info('The upgrade process started.');
            $this->info('-----------------------------');
            try {
                if(!\File::exists(storage_path('app/updates'))):
                    \File::makeDirectory(storage_path('app/updates'), 0754);
                else:
                    \File::deleteDirectory(storage_path('app/updates'));
                    \File::makeDirectory(storage_path('app/updates'), 0754);
                endif;
                chdir(storage_path('app/updates'));
                $this->info("Composer require grphm/stalker_updates_rc:$version");
                if(substr(php_uname(), 0, 7) == "Windows"):
                    shell_exec($composer.' self-update');
                    $out = shell_exec($composer.' --prefer-dist --no-progress --no-ansi require "grphm/stalker_updates_rc":"'.$version.'" 2>&1');
                else:
                    shell_exec('COMPOSER_HOME="'.storage_path('app/updates').'" '.$composer.' self-update');
                    $out = shell_exec('COMPOSER_HOME="'.storage_path('app/updates').'" '.$composer.' --prefer-dist --no-progress --no-ansi require "grphm/stalker_updates_rc":"'.$version.'" 2>&1');
                endif;
                $this->info($out);
                if(\File::exists(storage_path('app/updates/vendor/grphm/stalker_updates_rc/public'))):
                    \File::copyDirectory(storage_path('app/updates/vendor/grphm/stalker_updates_rc/public'), public_path());
                endif;
                if(\File::exists(storage_path('app/updates/vendor/grphm/stalker_updates_rc/root'))):
                    \File::copyDirectory(storage_path('app/updates/vendor/grphm/stalker_updates_rc/root'), base_path('root'));
                endif;
                \File::deleteDirectory(storage_path('app/updates'));
                $this->info('-----------------------------');
                $this->info('Update completed successfully');
            } catch(\Exception $exception) {
                $this->info('-----------------------------');
                $this->error($exception->getMessage());
            }
            if($migrations):
                $this->runMigrations();
            endif;
        endif;
    }

    private function runMigrations() {

        $this->info('Run migrations started');
        try {
            $database = config('database.connections.'.config('database.default').'.database');
            $table_index_name = 'Tables_in_'.$database;
            $tables = \DB::select('show full tables where Table_Type != "VIEW"');
            if(count($tables)):
                $tables_cms = [];
                foreach($tables as $table):
                    $table = json_decode(json_encode($table), TRUE);
                    if(is_array($table) && isset($table[$table_index_name])):
                        $tables_cms[] = $table[$table_index_name];
                    endif;
                endforeach;
                $all_tables = $this->getRenameTablesNames();
                foreach($tables_cms as $table):
                    if(isset($all_tables[$table])):
                        \Schema::rename($table, $all_tables[$table]);
                    endif;
                endforeach;
            endif;
            $this->info('-----------------------------');
            $this->info('Run migrations completed successfully');
        } catch(\Exception $exception) {
            $this->info('-----------------------------');
            $this->error($exception->getMessage());
        }
    }

    private function getRenameTablesNames() {

        return [
            'cities' => 'system_cities',
            'countries' => 'system_countries',
            'dictionary' => 'dictionaries_dictionaries',
            'dictionary_lists' => 'dictionaries_lists',
            'dictionary_lists_fields' => 'dictionaries_list_fields',
            'file_types' => 'uploads_file_types',
            'galleries' => 'galleries_galleries',
            'gallery_templates' => 'galleries_templates',
            'groups' => 'system_groups',
            'languages' => 'system_languages',
            'packages' => 'system_packages',
            'settings' => 'system_settings',
            'permissions' => 'system_permissions',
            'photos' => 'galleries_photos',
            'uploads' => 'uploads_files',
        ];
    }
}
