<?php
namespace STALKER_CMS\Vendor\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * Регистрация artisan-команд
 * @package STALKER_CMS\Vendor\Console
 */
class Kernel extends ConsoleKernel {

    protected $commands = [
        Commands\Uninstall::class,
        Commands\Update::class,
        Commands\Install::class,
        Commands\CacheKiller::class
    ];

    protected function schedule(Schedule $schedule) {
    }
}
