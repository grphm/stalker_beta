<?php
namespace STALKER_CMS\Vendor\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class PackagesController
 * Фасад контроллера по управлению пакетами CMS
 * @package STALKER_CMS\Vendor\Facades
 */
class PackagesController extends Facade {

    protected static function getFacadeAccessor() {

        return 'PackagesController';
    }
}