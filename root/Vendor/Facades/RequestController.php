<?php
namespace STALKER_CMS\Vendor\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class RequestController
 * Фасад контроллера обрабатывающего запросы к серверу
 * @package STALKER_CMS\Vendor\Facades
 */
class RequestController extends Facade {

    protected static function getFacadeAccessor() {

        return 'RequestController';
    }
}