<?php
namespace STALKER_CMS\Vendor\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class ValidatorController
 * Фасад контроллера валидации запроса поступающего к серверу
 * @package STALKER_CMS\Vendor\Facades
 */
class ValidatorController extends Facade {

    protected static function getFacadeAccessor() {

        return 'ValidatorController';
    }
}