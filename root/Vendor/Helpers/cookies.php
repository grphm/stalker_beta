<?php
namespace STALKER_CMS\Vendor\Helpers;

/**
 * Проверка на существование куки
 * @param null $name - имя куки
 * @return bool
 */
function hasCookieData($name = NULL) {

    if (!is_null($name)):
        if (isset($_COOKIE[$name]) && !empty($_COOKIE[$name])):
            return TRUE;
        endif;
    endif;
    return FALSE;
}

/**
 * Возвращает данные куки хранящиеся в JSON-строке
 * @param null $name
 * @param string $return
 * @return array
 */
function getJsonCookieData($name = null, $return = 'keys') {
    if (!is_null($name)):
        if (isset($_COOKIE[$name]) && !empty($_COOKIE[$name])):
            if ($return == 'keys'):
                return array_keys(json_decode($_COOKIE[$name], TRUE));
            elseif ($return == 'values'):
                return array_values(json_decode($_COOKIE[$name], TRUE));
            elseif ($return == 'values_unique'):
                $values_unique = array();
                foreach (json_decode($_COOKIE['ordering'], TRUE) as $index => $values):
                    foreach ($values as $value):
                        $values_unique[] = $value;
                    endforeach;
                endforeach;
                return array_unique($values_unique);
            endif;
        endif;
    endif;
    return array();
}

/**
 * Возвращает массив значение хранящихся в куке через запятую
 * @param null $name
 * @return array
 */
function getArrayCookieStringData($name = null) {
    if (!is_null($name)):
        if (isset($_COOKIE[$name]) && !empty($_COOKIE[$name])):
            return explode(',', $_COOKIE[$name]);
        endif;
    endif;
    return array();
}

/**
 * Установка значения в куку
 * @param bool|false $name
 * @param bool|false $value
 * @param int $lifetime
 */
function cookie_set($name = false, $value = false, $lifetime = 86400) {

    if (is_object($value) || is_array($value)):
        $value = json_encode($value);
    endif;
    setcookie($name, $value, time() + $lifetime, "/");
    if ($lifetime > 0):
        $_COOKIE[$name] = $value;
    endif;
}

/**
 * Получения значения из куки
 * @param bool|false $name
 * @param bool|false $value
 * @param int $lifetime
 * @return array|bool|mixed
 */
function cookie_get($name = false, $value = false, $lifetime = 86400) {

    $return = @isset($_COOKIE[$name]) ? $_COOKIE[$name] : false;
    $return2 = @json_decode($return, 1);
    if (is_array($return2)):
        $return = $return2;
    endif;
    return $return;
}

/**
 * Удаление куки
 * @param bool|false $name
 */
function cookie_drop($name = false) {

    cookie_set($name, false, 0);
    $_COOKIE[$name] = false;
}