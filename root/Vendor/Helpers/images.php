<?php
namespace STALKER_CMS\Vendor\Helpers;

use Illuminate\Support\Facades\Storage;

/**
 * Проверяет является ли файл изображением
 * @param string $filename
 * @return bool|int
 */
function is_image($filename = '') {

    if (!empty($filename) && Storage::disk('local')->exists($filename)):
        if ($is = @getimagesize($filename)):
            if (!in_array($is[2], array(1, 2, 3))):
                return false;
            else:
                return true;
            endif;
        else:
            return FALSE;
        endif;
    else:
        return -1;
    endif;
}

/**
 * Удаляет MIME-type кодированных в base64 изображений
 * https://ru.wikipedia.org/wiki/Data:_URL
 * @param null $string
 * @return mixed|null
 */
function trim_base64_image($string = NULL) {

    if (!is_null($string) && !empty($string)):
        return str_replace(['data:image/jpeg;base64,', 'data:image/jpg;base64', 'data:image/png;base64'], '', $string);
    endif;
    return NULL;
}