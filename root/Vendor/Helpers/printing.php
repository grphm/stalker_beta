<?php
namespace STALKER_CMS\Vendor\Helpers;

/**
 * Выводит на экран данные полученные от пользователя
 */
function tadi() {

    dd(\Request::all());
}

/**
 * Выводит на экран содержимое объекта или массива
 * Останавливает дальнейшее выполнения приложения
 * @param $object
 */
function tad($object) {

    if (is_object($object)):
        dd($object->toArray());
    elseif (is_array($object)):
        dd($object);
    elseif (is_string($object) || is_numeric($object)):
        dd((array)$object);
    endif;
}

/**
 * Выводит на экран содержимое объекта или массива
 * Не останавливает дальнейшее выполнения приложения
 * @param $object
 */
function ta($object) {

    $return = $object;
    if (is_object($object)):
        $return = $object->toArray();
    elseif (is_array($object)):
        foreach ($object as $o => $obj):
            $return[$o] = is_object($obj) ? $obj->toArray() : $obj;
        endforeach;
    endif;
    d((array)$return);
}

/**
 * Выводит на экран содержимое массива
 * Не останавливает дальнейшее выполнения приложения
 * @param array $array
 */
function d(array $array) {

    if (count($array)):
        echo "\n<pre style='text-align:left'>\n" . print_r($array, 1) . "\n</pre>\n";
    else:
        echo "\n<pre style='text-align:left'>\nПустое значение\n</pre>\n";
    endif;
}

/**
 * Выводит на экран содержимое массива
 * Останавливает дальнейшее выполнения приложения
 * @param array $array
 */
function dd(array $array) {
    d($array);
    die;
}