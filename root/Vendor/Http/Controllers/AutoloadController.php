<?php
namespace STALKER_CMS\Vendor\Http\Controllers;

use League\Flysystem\Exception;

/**
 * Class AutoloadController
 * @package STALKER_CMS\Vendor\Controllers
 * Предзагружает Ядро CMS
 */
class AutoloadController {

    protected $mysqli;

    public function __construct() {

        try {
            if ($db_config = $this->dbConfig()):
                $this->mysqli = new \mysqli($db_config['DB_HOST'], $db_config['DB_USERNAME'], $db_config['DB_PASSWORD'], $db_config['DB_DATABASE']);
                if (mysqli_connect_errno()):
                    die('DB connect error');
                endif;
            else:
                $this->mysqli = NULL;
            endif;
        } catch (Exception $e) {
            $this->mysqli = NULL;
        }
    }

    public function __destruct() {

        if (!is_null($this->mysqli)):
            $this->mysqli->close();
        else:
            unset($this->mysqli);
        endif;
    }

    /**
     * Инициализация основных провайдеров
     * Инициализация провайдеров активных модулей
     * Возвращает массив строк для регистрации провайдеров в файле конфигурации приложения (см. config/app.php)
     * @return array
     */
    public function init_providers() {

        $providers = [
            'package_html' => 'STALKER_CMS\Packages\Html\HtmlServiceProvider',
            'vendor_primary' => 'STALKER_CMS\Vendor\Providers\PrimaryServiceProvider',
            'core_install' => 'STALKER_CMS\Core\Install\Providers\ModuleServiceProvider',
            'core_install_route' => 'STALKER_CMS\Core\Install\Providers\RouteServiceProvider',
        ];
        if (!is_null($this->mysqli)):
            if ($result = $this->mysqli->query("SELECT * FROM `packages` WHERE `enabled` = 1 ORDER BY `order`")):
                while ($row = $result->fetch_array(MYSQLI_ASSOC)):
                    $module_sections = PackagesController::makeModuleSections($row);
                    if (file_exists(base_path('root/' . implode('/', $module_sections) . '/Providers/ModuleServiceProvider.php'))):
                        $providers[$row['slug']] = 'STALKER_CMS\\' . implode('\\', $module_sections) . '\Providers\ModuleServiceProvider';
                    endif;
                    if (file_exists(base_path('root/' . implode('/', $module_sections) . '/Providers/RouteServiceProvider.php'))):
                        $providers[$row['slug'] . '_route'] = 'STALKER_CMS\\' . implode('\\', $module_sections) . '\Providers\RouteServiceProvider';
                    endif;
                    /*
                     * Подключение модуля
                     * разширения функционала
                     */
                    if (count($module_sections) == 1 && strtolower($module_sections[0]) == 'application'):
                        if (file_exists(base_path('home/Providers/ModuleServiceProvider.php'))):
                            $providers[$row['slug']] = 'APPLICATION_HOME\Providers\ModuleServiceProvider';
                        endif;
                        if (file_exists(base_path('home/Providers/RouteServiceProvider.php'))):
                            $providers[$row['slug'] . '_route'] = 'APPLICATION_HOME\Providers\RouteServiceProvider';
                        endif;
                    endif;
                endwhile;
                $result->close();
            endif;
            if ($result = $this->mysqli->query("SELECT * FROM `packages` WHERE `required` = 1 and `enabled` = 0")):
                while ($row = $result->fetch_array(MYSQLI_ASSOC)):
                    if (isset($providers[$row['slug']])):
                        unset($providers[$row['slug']]);
                    endif;
                    if (isset($providers[$row['slug'] . '_route'])):
                        unset($providers[$row['slug'] . '_route']);
                    endif;
                endwhile;
                $result->close();
            endif;
        endif;
        return $providers;
    }

    /**
     * Инициализация основных псевдонимов
     * Инициализация псевдонимов активных модулей
     * Возвращает массив строк для регистрации псевдонимов в файле конфигурации приложения (см. config/app.php)
     * @return array
     */
    public function init_aliases() {

        $aliases = [
            'Form' => '\STALKER_CMS\Packages\Html\FormFacade',
            'Html' => '\STALKER_CMS\Packages\Html\HtmlFacade',
            'ResponseController' => '\STALKER_CMS\Vendor\Facades\ResponseController',
            'RequestController' => '\STALKER_CMS\Vendor\Facades\RequestController',
            'ValidatorController' => '\STALKER_CMS\Vendor\Facades\ValidatorController',
            'PackagesController' => '\STALKER_CMS\Vendor\Facades\PackagesController',
        ];
        if (!is_null($this->mysqli)):
            if ($result = $this->mysqli->query("SELECT * FROM `packages` WHERE `enabled` = 1 ORDER BY `order`")):
                while ($row = $result->fetch_array(MYSQLI_ASSOC)):
                    $module_sections = PackagesController::makeModuleSections($row);
                    $directory = base_path('root/' . implode('/', $module_sections) . '/Facades');
                    if (file_exists($directory)):
                        if ($scanned_directory = glob($directory . '/*.php')):
                            foreach ($scanned_directory as $file_name):
                                $alias = basename($directory . '/' . $file_name, ".php");
                                $aliases[$alias] = '\STALKER_CMS\\' . implode('\\', $module_sections) . '\\Facades\\' . $alias;
                            endforeach;
                        endif;
                    endif;
                    /*
                     * Подключение модуля
                     * разширения функционала
                     */
                    if (count($module_sections) == 1 && strtolower($module_sections[0]) == 'application'):
                        $directory = base_path('home/Facades');
                        if (file_exists($directory)):
                            if ($scanned_directory = glob($directory . '/*.php')):
                                foreach ($scanned_directory as $file_name):
                                    $alias = basename($directory . '/' . $file_name, ".php");
                                    $aliases[$alias] = '\APPLICATION_HOME\Facades\\' . $alias;
                                endforeach;
                            endif;
                        endif;
                    endif;
                endwhile;
                $result->close();
            endif;
        endif;
        return $aliases;
    }

    /**
     * Возращает данные для подключения к Базе Данных из файла конфигурации
     * @return array|bool
     */
    private function dbConfig() {

        if (file_exists(base_path('.env')) === FALSE):
            return FALSE;
        endif;

        $config_file = file(base_path('.env'));
        $config = [
            'DB_HOST' => 'localhost',
            'DB_DATABASE' => '',
            'DB_USERNAME' => '',
            'DB_PASSWORD' => '',
        ];
        if (is_array($config_file)):
            foreach ($config_file as $key => $value):
                $line = explode('=', $value);
                switch (@$line[0]):
                    case 'DB_HOST':
                        $config['DB_HOST'] = trim(@$line[1]);
                        break;
                    case 'DB_DATABASE':
                        $config['DB_DATABASE'] = trim(@$line[1]);
                        break;
                    case 'DB_USERNAME':
                        $config['DB_USERNAME'] = trim(@$line[1]);
                        break;
                    case 'DB_PASSWORD':
                        $config['DB_PASSWORD'] = trim(@$line[1]);
                        break;
                endswitch;
            endforeach;
        endif;
        return $config;
    }
}