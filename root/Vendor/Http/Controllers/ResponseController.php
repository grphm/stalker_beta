<?php
namespace STALKER_CMS\Vendor\Http\Controllers;

/**
 * Контроллер обрабатывающий ответы от сервера
 * Class ResponseController
 * @package STALKER_CMS\Vendor\Controllers
 *
 * Формирует ответ от сервера клиенту
 * Используется при AJAX запросах
 * Формат ответ: JSON
 */
class ResponseController {

    /**
     * Свойство хранящее массив ответа
     * @var array
     */
    private $ajax_response;

    /**
     * Инициализирует ответ на AJAX запрос клиенту
     * status - рузультат выполнения серверного обработчика. Тип: булевое значение
     * responseText - Сообщение передачаемое клиенту после выполнении серверного обработчика. Тип: string
     * redirectURL - Абсолютная ссылка (URL). Используется для выполнения перехода после выполнении серверного обработчика. Тип: string
     * redirectCode - Код состояния HTTP. Тип: integer
     * errorCode - Код ошибки выполнении серверного обработчика. Тип: integer
     * errorText - Текст ошибки выполнении серверного обработчика. Тип: string
     */
    public function __construct() {

        $this->ajax_response = [
            'status' => FALSE,
            'responseText' => '',
            'redirectURL' => FALSE,
            'redirectCode' => 200,
            'errorCode' => 0,
            'errorText' => ''
        ];
    }

    /*********************************************************************************/

    /**
     * Устанавливает или добавляет значение в тело ответа
     * @param $_index
     * @param $_value
     * @return $this
     */
    public function set($_index, $_value) {

        $this->ajax_response[$_index] = $_value;
        return $this;
    }

    /**
     * Возвращает объект ответа клиенту
     * @return array
     */
    public function get() {

        return $this->ajax_response;
    }

    /**
     * Возвращает объект ответа клиенту в формате JSON
     * @return \Illuminate\Http\JsonResponse
     */
    public function json() {

        return \Response::json($this->ajax_response);
    }

    /**
     * Формирует ответ при успешном выполнении серверного обработчика
     * @param $code - Код успеха выполнении серверного обработчика
     * @return $this
     */
    public function success($code = 0) {

        $this->set('status', TRUE);
        $this->set('responseText', trans('root_lang::codes.' . $code));
        $this->set('errorCode', 0);
        $this->set('errorText', '');
        return $this;
    }

    /**
     * Формирует ответ при ошибочном выполнении серверного обработчика
     * @param $code - Код ошибки выполнении серверного обработчика
     * @return $this
     */
    public function error($code = 0) {

        $this->set('status', FALSE);
        $this->set('responseText', '');
        $this->set('errorCode', $code);
        $this->set('errorText', trans('root_lang::codes.' . $code));
        return $this;
    }

    /**
     * Формирует параметр перенаправления
     * @param $link - Ссылка на страницу перенаправления
     * @param int $code - Код состояния HTTP
     * @return $this
     */
    public function redirect($link, $code = 200) {

        $this->set('redirectURL', $link);
        $this->set('redirectCode', $code);
        return $this;
    }
}