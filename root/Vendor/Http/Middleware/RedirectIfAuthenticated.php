<?php

namespace STALKER_CMS\Vendor\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Фильтр запрещает выполнения процесса если пользователь авторизован
 * Class RedirectIfAuthenticated
 * @package STALKER_CMS\Vendor\Http\Middleware
 */
class RedirectIfAuthenticated {

    public function handle($request, Closure $next, $guard = null) {

        if (Auth::guard($guard)->check()):
            if ($request->wantsJson()):
                return \ResponseController::error(403)->json();
            else:
                return \Route::has('dashboard') ? redirect()->route('dashboard') : redirect('');
            endif;
        endif;
        return $next($request);
    }
}
