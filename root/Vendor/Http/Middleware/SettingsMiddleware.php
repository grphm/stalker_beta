<?php

namespace STALKER_CMS\Vendor\Http\Middleware;

use Closure;

/**
 * Фильтр доступа к настройке
 * Class SettingsMiddleware
 * @package STALKER_CMS\Vendor\Http\Middleware
 */
class SettingsMiddleware {

    public function handle($request, Closure $next, $package = NULL, $module = NULL, $setting = NULL) {

        $attributes = [];
        if(!is_null($package)):
            $attributes[] = $package;
        endif;
        if(!is_null($module)):
            $attributes[] = $module;
        endif;
        if(!is_null($setting)):
            $attributes[] = $setting;
        endif;
        if (count($attributes) && \STALKER_CMS\Vendor\Helpers\settings($attributes) == FALSE):
            throw new \Exception(\Lang::get('root_lang::codes.404'), 404);
        endif;
        return $next($request);
    }
}