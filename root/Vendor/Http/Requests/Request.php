<?php

namespace STALKER_CMS\Vendor\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class Request
 * @package STALKER_CMS\Vendor\Http\Requests
 */
abstract class Request extends FormRequest {

}