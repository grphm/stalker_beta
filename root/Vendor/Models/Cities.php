<?php
namespace STALKER_CMS\Vendor\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;

/**
 * Модель города
 * Class Cities
 * @package STALKER_CMS\Vendor\Models
 */
class Cities extends BaseModel implements ModelInterface {

    /**
     * @var bool
     */
    public $timestamps = FALSE;

    /**
     * @var string
     */
    protected $table = 'cities';
    /**
     * @var array
     */
    protected $fillable = ['slug', 'locale', 'title'];
    /**
     * @var array
     */
    protected $hidden = [];
    /**
     * @var array
     */
    protected $guarded = ['id', '_method', '_token'];

    /**
     * @param $request
     */
    public function insert($request) {
        // TODO: Implement insert() method.
    }

    /**
     * @param $id
     * @param $request
     */
    public function replace($id, $request) {
        // TODO: Implement replace() method.
    }

    /**
     * @param array|int $id
     */
    public function remove($id) {
        // TODO: Implement remove() method.
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /***************************************************************************************************************/
    /**
     * @return array
     */
    public static function getStoreRules() {

        return ['slug' => 'required', 'title' => 'required', 'locale' => 'required'];
    }

    /**
     * @return array
     */
    public static function getUpdateRules() {

        return ['slug' => 'required', 'title' => 'required', 'locale' => 'required'];
    }
}