<?php
namespace STALKER_CMS\Vendor\Models;

use STALKER_CMS\Vendor\Interfaces\ModelInterface;

/**
 * Модель языки
 * Class Languages
 * @package STALKER_CMS\Vendor\Models
 */
class Languages extends BaseModel implements ModelInterface {

    /**
     * @var bool
     */
    public $timestamps = FALSE;

    /**
     * @var string
     */
    protected $table = 'languages';
    /**
     * @var array
     */
    protected $fillable = ['slug', 'title', 'iso_639_1', 'iso_639_2', 'iso_639_3', 'code', ' active', 'default', 'required'];
    /**
     * @var array
     */
    protected $hidden = [];
    /**
     * @var array
     */
    protected $guarded = ['id', '_method', '_token'];

    /**
     * @var array
     */
    public $rules_store = ['slug' => 'required', 'title' => 'required', 'iso_639_1' => 'required', 'iso_639_2' => 'required', 'iso_639_3' => 'required', 'code' => 'required'];
    /**
     * @var array
     */
    public $rules_update = ['slug' => 'required', 'title' => 'required', 'iso_639_1' => 'required', 'iso_639_2' => 'required', 'iso_639_3' => 'required', 'code' => 'required'];

    /**
     * @param $request
     */
    public function insert($request) {
        // TODO: Implement insert() method.
    }

    /**
     * @param $id
     * @param $request
     */
    public function replace($id, $request) {
        // TODO: Implement replace() method.
    }

    /**
     * @param array|int $id
     */
    public function remove($id) {
        // TODO: Implement remove() method.
    }

    /**
     * @param array $attributes
     */
    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    /**
     * @param array $attributes
     */
    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    /**
     * @param array $attributes
     */
    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }
}