<?php

return [
    'select_image' => 'Seleccionar imagen',
    'delete_image' => 'Eliminar',
    'select' => 'Seleccionar',
    'cancel' => 'Cancelar',
];
