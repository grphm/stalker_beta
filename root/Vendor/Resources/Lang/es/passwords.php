<?php

return [
    'password' => 'Las contraseñas deben tener al menos seis caracteres y que coincida con la confirmación.',
    'reset' => 'La contraseña se ha restablecido!',
    'sent' => 'Hemos enviado por correo electrónico el enlace para restablecer contraseña!',
    'token' => 'Este token de restablecimiento de contraseña no es válido.',
    'user' => 'No podemos encontrar un usuario con ese e-mail.',
];