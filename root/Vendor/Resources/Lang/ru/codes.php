<?php

return [
    100 => 'Продолжить',
    101 => 'Переключение протоколов',
    102 => 'Идёт обработка',
    200 => 'Успешно',
    201 => 'Создано',
    202 => 'Сохранено',
    203 => 'Информация не авторитетна',
    204 => 'Нет содержимого',
    205 => 'Сбросить содержимое',
    206 => 'Частичное содержимое',
    207 => 'Многостатусный',
    226 => 'Использовано IM',
    300 => 'Множество выборов',
    301 => 'Перемещено навсегда',
    302 => 'Перемещено временно',
    303 => 'Смотреть другое',
    304 => 'Не изменялось',
    305 => 'Использовать прокси',
    307 => 'Временное перенаправление',
    400 => 'Плохой, неверный запрос',
    401 => 'Не авторизован',
    402 => 'Необходима оплата',
    403 => 'Запрещено',
    404 => 'Не найдено',
    405 => 'Метод не поддерживается',
    406 => 'Неприемлемо',
    407 => 'Необходима аутентификация прокси',
    408 => 'Истекло время ожидания',
    409 => 'Конфликт',
    410 => 'Удалён',
    411 => 'Необходима длина',
    412 => 'Условие ложно',
    413 => 'Размер запроса слишком велик',
    414 => 'Запрашиваемый URI слишком длинный',
    415 => 'Неподдерживаемый тип данных',
    416 => 'Запрашиваемый диапазон не достижим',
    417 => 'Ожидаемое неприемлемо',
    422 => 'Необрабатываемый экземпляр',
    423 => 'Заблокировано',
    424 => 'Невыполненная зависимость',
    425 => 'Неупорядоченный набор',
    426 => 'Необходимо обновление',
    428 => 'Необходимо предусловие',
    429 => 'Слишком много запросов',
    431 => 'Поля заголовка запроса слишком большие',
    434 => 'Запрашиваемый адрес недоступен',
    444 => '',
    449 => 'Повторить с',
    451 => 'Недоступно по юридическим причинам',
    498 => 'Ошибка Google play',
    499 => 'Используется Nginx, когда клиент закрывает соединение до получения ответа',
    500 => 'Внутренняя ошибка сервера',
    501 => 'Не реализовано',
    502 => 'Плохой, ошибочный шлюз',
    503 => 'Сервис недоступен',
    504 => 'Шлюз не отвечает',
    505 => 'Версия HTTP не поддерживается',
    506 => 'Вариант тоже проводит согласование',
    507 => 'Переполнение хранилища',
    508 => 'Обнаружена петля',
    509 => 'Исчерпана пропускная ширина канала',
    510 => 'Не расширено',
    511 => 'Требуется сетевая аутентификация',

    1000 => '',
    1001 => 'Конфигурация создана',
    1002 => 'Модули установлены',
    1200 => 'Не используется',
    1201 => 'Не используется',
    1202 => 'Не используется',
    1203 => 'Удалено',
    1204 => 'Поддержка включена',
    1205 => 'Поддержка выключена',
    1503 => 'Мы обновляемся, зайдите к нам позже',
    1600 => 'Файл шаблона успешно создан',
    1700 => 'Сообщение отправлено',

    2000 => '',
    2001 => 'Ошибка подключения в Базе Данных',
    2002 => 'Не удалось создать конфигурационный файл',
    2003 => 'Отсутствует шаблон конфигурационного файла',
    2004 => 'Не удалось установить модули',
    2005 => 'Отсутствует конфигурационный файл',
    2010 => 'Указанный Email не существует',
    2011 => 'Указанный Email уже зарегистрирован',
    2050 => 'Отсутствует главная страница',
    2100 => 'Неверно заполнены поля',
    2101 => 'Неверное имя пользователя или пароль',
    2400 => 'Обновите страницу',
    2403 => 'Запрещено',
    2404 => 'Не найден файл composer',
    2503 => 'Ошибка удаления',
	2505 => 'Символьный код уже используется',
	2506 => 'Не удалось загрузить файл',
	2507 => 'URL уже используется',
	2610 => 'Шаблон с таким именем уже существует',
	2611 => 'Шаблон не существует',
	2612 => 'Отсутствуют шаблоны страниц',
	2613 => 'Отсутствуют шаблоны меню',
	2614 => 'Отсутствуют опубликованные страницы',
	2615 => 'Отсутствуют языковые файлы',
];