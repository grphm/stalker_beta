@extends('root_views::layouts.errors')
@section('title', $message)
@section('body-class'){{ 'four-zero-content' }}@stop
@section('content')
    <div class="four-zero">
        <h2>{{ $code }}</h2>
        <small>{{ $message }}</small>
        <footer>
            <a href="{!! url('/') !!}"><i class="zmdi zmdi-home"></i></a>
            @dashboard
        </footer>
    </div>
@stop
@section('scripts_before')
@stop
@section('scripts_after')
@stop