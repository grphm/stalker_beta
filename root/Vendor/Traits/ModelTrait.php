<?php

namespace STALKER_CMS\Vendor\Traits;

use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Модель
 * Class ModelTrait
 * @package STALKER_CMS\Vendor\Traits
 */
trait ModelTrait {

    /**
     * Возвращает человеческую дату создание записи в таблице
     * @return string
     */

    public function getCreatedDateAttribute() {

        \Carbon\Carbon::setLocale(\App::getLocale());
        return \Carbon\Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

    /**
     * Возвращает человеческую дату обновления записи в таблице
     * @return string
     */
    public function getUpdatedDateAttribute() {

        \Carbon\Carbon::setLocale(\App::getLocale());
        return \Carbon\Carbon::parse($this->attributes['updated_at'])->diffForHumans();
    }

    /**
     * Возвращает модель или вызывает исключение
     * @param array $attributes
     * @return mixed
     * @throws Exception
     */
    public static function whereFind(array $attributes) {

        if (!is_null($instance = static::where($attributes)->first())):
            return $instance;
        else:
            throw (new ModelNotFoundException)->setModel(get_called_class());
        endif;
    }

    /**
     * Проверяет существуют записи в модели
     * @param array $attributes
     * @return mixed
     */
    public static function whereExist(array $attributes) {

        return static::where($attributes)->exists();
    }

    /**
     * Проверяет уникальность записи в модели
     * @param array $attributes
     * @param null $id
     * @throws \Exception
     */
    public static function uniqueness(array $attributes, $id = NULL) {

        if (is_null($id) && static::where($attributes)->exists()):
            throw new \Exception(\Lang::get('root_lang::codes.2505'), 2505);
        elseif (is_numeric($id) && static::where('id', '!=', $id)->where($attributes)->exists()):
            throw new \Exception(\Lang::get('root_lang::codes.2505'), 2505);
        endif;
    }
}